/*
 * FLEXIO_LIB.h
 *
 *  Created on: 30/10/2017
 *      Author: Ricardo Andrés Velásquez Vélez
 */

#ifndef FLEXIO_LIB_H_
#define FLEXIO_LIB_H_

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t FEATURE		:16;  						// Feature Specification Number
		uint32_t MINOR			:8;	  						// Minor Version Number
		uint32_t MAJOR	 		:8;							// Major Version Number
	} BITS;
} __flexio_verid_t;



typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SHIFTER		:8;  						// Shifter Number
		uint32_t TIMER			:8;	  						// Timer Number
		uint32_t PIN	 		:8;							// Pin Number
		uint32_t TRIGGER		:8;	  						// Trigger Number
	} BITS;
} __flexio_param_t;

enum{
	mFLEXIO_CTRL_FLEXEN = 0x00000001U,						// 0 FlexIO Enable
	mFLEXIO_CTRL_SWRST = 0x00000002U,						// 1 Software Reset
	mFLEXIO_CTRL_FASTACC = 0x00000004U,						// 2 Fast Access
	mFLEXIO_CTRL_DBGE = 0x40000000U,						// 30 Debug Enable
	mFLEXIO_CTRL_DOZEN = 0x80000000U						// 31 Doze Enable
};

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t FLEXEN		:1;  							// FlexIO Enable
		uint32_t SWRST		:1;	  							// Software Reset
		uint32_t FASTACC 	:1;								// Fast Access
		uint32_t 			:27;	  							// Reserved
		uint32_t DBGE		:1;	  							// Debug Enable
		uint32_t DOZEN		:1;	  							// Doze Enable
	} BITS;
} __flexio_ctrl_t;

#define mFLEXIO_SHIFTSTAT_SSF(X) (X & 0x0000000FU)			// Shifter Status Flag

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SSF		:4;  							// Shifter Status Flag
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_shiftstat_t;

#define mFLEXIO_SHIFTERR_SEF(X) (X & 0x0000000FU)			// Shifter Error Flags

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SEF		:4;  							// Shifter Error Flags
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_shifterr_t;

#define mFLEXIO_TIMSTAT_TSF(X) (X & 0x0000000FU)			// Timer Status Flags

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t TSF		:4;  							// Timer Status Flags
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_timstat_t;

#define mFLEXIO_SHIFTSIEN_SSIE(X) (X & 0x0000000FU)			// Shifter Status Interrupt Enable

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SSIE		:4;  							// Shifter Status Interrupt Enable
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_shiftsien_t;

#define mFLEXIO_SHIFTEIEN_SEIE(X) (X & 0x0000000FU)			// Shifter Error Interrupt Enable

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SEIE		:4;  							// Shifter Error Interrupt Enable
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_shifteien_t;

#define mFLEXIO_TIMIEN_TEIE(X) (X & 0x0000000FU)				// Timer Status Interrupt Enable

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t TEIE		:4;  							// Timer Status Interrupt Enable
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_timien_t;

#define mFLEXIO_SHIFTSDEN_SSDE(X) (X & 0x0000000FU)			// Shifter Status DMA Enable

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SSDE		:4;  							// Shifter Status DMA Enable
		uint32_t 			:28;	  						// Reserved
	} BITS;
} __flexio_shiftsden_t;

#define mFLEXIO_SHIFTCTLn_SMOD(X) (X & 0x00000007U)			// Shifter Mode
#define mFLEXIO_SHIFTCTLn_PINSEL(X) ((X & 0x00000007U)<<8)	// Shifter Pin Select
#define mFLEXIO_SHIFTCTLn_PINCFG(X) ((X & 0x00000003U)<<16)	// Shifter Pin Configuration
#define mFLEXIO_SHIFTCTLn_TIMSEL(X) ((X & 0x00000003U)<<24)	// Timer Select

enum{
	mFLEXIO_SHIFTCTLn_PINPOL =	0x00000080U,				// 7 Shifter Pin Polarity
	mFLEXIO_SHIFTCTLn_TIMPOL = 0x00800000U					// 23 Timer Polarity
};

enum{
	kFLEXIO_SMOD_DISABLED,
	kFLEXIO_SMOD_RECEIVE_MODE,
	kFLEXIO_SMOD_TRANSMIT_MODE,
	kFLEXIO_SMOD_MATCH_STORE_MODE=4,
	kFLEXIO_SMOD_MATCH_CONTINUOS_MODE
};

enum{
	kFLEXIO_SHIFTER_PINCFG_OUTPUT_DISABLED,
	kFLEXIO_SHIFTER_PINCFG_OPEN_DRAIN,
	kFLEXIO_SHIFTER_PINCFG_BIDIRECTIONAL,
	kFLEXIO_SHIFTER_PINCFG_OUTPUT
};

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SMOD		:3;  							// Shifter Mode
		uint32_t 			:4;	  							// Reserved
		uint32_t PINPOL	 	:1;								// Shifter Pin Polarity
		uint32_t PINSEL		:3;	  							// Shifter Pin Select
		uint32_t 			:5;	  							// Reserved
		uint32_t PINCFG		:2;	  							// Shifter Pin Configuration
		uint32_t 			:5;	  							// Reserved
		uint32_t TIMPOL		:1;	  							// Timer Polarity
		uint32_t TIMSEL		:2;	  							// Timer Select
		uint32_t 			:6;	  							// Reserved
	} BITS;
} __flexio_shiftctln_t;

#define mFLEXIO_SHIFTCFGn_SSTART(X) (X & 0x00000003U)		// Shifter Start bit
#define mFLEXIO_SHIFTCFGn_SSTOP(X) ((X & 0x00000003U)<<4)	// Shifter Stop bit

enum{
	mFLEXIO_SHIFTCFGn_INSRC = 0x00000100U					// 8 Input Source
};

enum{
	kFLEXIO_SSTART_DISABLED_ON_ENABLE,
	kFLEXIO_SSTART_DISABLED_ON_FIRST_SHIFT,
	kFLEXIO_SSTART_START_BIT_0_BEFORE_FIRST_SHIFT,
	kFLEXIO_SSTART_START_BIT_1_BEFORE_FIRST_SHIFT
};

enum{
	kFLEXIO_SSTOP_DISABLED,
	kFLEXIO_SSTOP_RESERVED,
	kFLEXIO_SSTOP_STOP_BIT_0,
	kFLEXIO_SSTOP_STOP_BIT_1
};

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t SSTART		:2;  							// Shifter Start bit
		uint32_t 			:2;	  							// Reserved
		uint32_t SSTOP	 	:2;								// Shifter Stop bit
		uint32_t 			:2;	  							// Reserved
		uint32_t INSRC		:1;	  							// Input Source
		uint32_t 			:25;	  						// Reserved
	} BITS;
} __flexio_shiftcfgn_t;

typedef uint32_t __flexio_shiftbufn_t;
typedef uint32_t __flexio_shiftbufbisn_t;
typedef uint32_t __flexio_shiftbufbysn_t;
typedef uint32_t __flexio_shiftbufbbsn_t;

#define mFLEXIO_TIMCTLn_TIMOD(X) (X & 0x00000003U)			// Timer Mode
#define mFLEXIO_TIMCTLn_PINSEL(X) ((X & 0x00000007U)<<8)	// Timer Pin Select
#define mFLEXIO_TIMCTLn_PINCFG(X) ((X & 0x00000003U)<<16)	// Timer Pin Configuration
#define mFLEXIO_TIMCTLn_TRGSEL(X) ((X & 0x0000000FU)<<24)	// Trigger Select

enum{
	mFLEXIO_TIMCTLn_PINPOL =	0x00000080U,				// 7 Timer Pin Polarity
	mFLEXIO_TIMCTLn_TRGSRC = 0x00400000U,					// 22 Trigger Source
	mFLEXIO_TIMCTLn_TRGPOL = 0x00800000U					// 23 Trigger Polarity
};

enum{
	kFLEXIO_TIMER_PINCFG_OUTPUT_DISABLED,
	kFLEXIO_TIMER_PINCFG_OPEN_DRAIN,
	kFLEXIO_TIMER_PINCFG_BIDIRECTIONAL,
	kFLEXIO_TIMER_PINCFG_OUTPUT
};

enum{
	kFLEXIO_TIMOD_DISABLED,
	kFLEXIO_TIMOD_8BIT_CNT_BIT_MODE,
	kFLEXIO_TIMOD_8BIT_CNT_PWM_MODE,
	kFLEXIO_TIMOD_16BIT_CNT_MODE
};
typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t TIMOD		:2;  							// Timer Mode
		uint32_t 			:5;	  							// Reserved
		uint32_t PINPOL		:1;								// Timer Pin Polarity
		uint32_t PINSEL		:3;	  							// Timer Pin Select
		uint32_t 			:5;	  							// Reserved
		uint32_t PINCFG		:2;	  							// Timer Pin Configuration
		uint32_t 			:4;	  							// Reserved
		uint32_t TRGSRC		:1;	  							// Trigger Source
		uint32_t TRGPOL		:1;	  							// Trigger Polarity
		uint32_t TRGSEL		:4;	  							// Trigger Select
		uint32_t 			:4;	  							// Reserved
	} BITS;
} __flexio_timctln_t;


#define mFLEXIO_TIMCFGn_TSTOP(X) ((X & 0x00000003U)<<4)		// Timer Stop Bit
#define mFLEXIO_TIMCFGn_TIMENA(X) ((X & 0x00000007U)<<8)	// Timer Enable
#define mFLEXIO_TIMCFGn_TIMDIS(X) ((X & 0x00000007U)<<12)	// Timer Disable
#define mFLEXIO_TIMCFGn_TIMRST(X) ((X & 0x00000007U)<<16)	// Timer Reset
#define mFLEXIO_TIMCFGn_TIMDEC(X) ((X & 0x00000003U)<<20)	// Timer Decrement
#define mFLEXIO_TIMCFGn_TIMOUT(X) ((X & 0x00000003U)<<24)	// Timer Output

enum{
	mFLEXIO_TIMCFGn_TSTART =	0x00000002U					// 1 Timer Start Bit
};

enum{
	kFLEXIO_TIMOUT_ONE_WHEN_ENABLE_DONTCARE_RESET,
	kFLEXIO_TIMOUT_ZERO_WHEN_ENABLE_DONTCARE_RESET,
	kFLEXIO_TIMOUT_ONE_ON_ENABLE_AND_RESET,
	kFLEXIO_TIMOUT_ZERO_ON_ENABLE_AND_RESET
};

enum{
	kFLEXIO_TIMDEC_ON_FLEXIO_CLOCK,
	kFLEXIO_TIMDEC_ON_TRIGGER_INPUT_EDGE_TIM_OUTPUT,
	kFLEXIO_TIMDEC_ON_INPUT_PIN_EDGE,
	kFLEXIO_TIMDEC_ON_TRIGGER_INPUT_EDGE_TRG_INPUT
};

enum{
	kFLEXIO_TIMRST_NEVER,
	kFLEXIO_TIMRST_ON_TIMPIN_EQUAL_TIMOUT=2,
	kFLEXIO_TIMRST_ON_TIMTRG_EQUAL_TIMOUT,
	kFLEXIO_TIMRST_ON_TIMPIN_RISING_EDGE,
	kFLEXIO_TIMRST_TIMTRG_RISING_EDGE=6,
	kFLEXIO_TIMRST_TIMTRG_BOTH_EDGES
};

enum{
	kFLEXIO_TIMDIS_NEVER,
	kFLEXIO_TIMDIS_ON_N_MINUS_1,
	kFLEXIO_TIMDIS_ON_COMPARE,
	kFLEXIO_TIMDIS_ON_COMPARE_AND_TRIGGER_LOW,
	kFLEXIO_TIMDIS_ON_PIN_BOTH_EDGES,
	kFLEXIO_TIMDIS_ON_PIN_BOTH_EDGES_TRIGGER_LOW,
	kFLEXIO_TIMDIS_ON_TRIGGER_FALLING_EDGE
};

enum{
	kFLEXIO_TIMENA_ALWAYS,
	kFLEXIO_TIMENA_ON_N_MINUS_1,
	kFLEXIO_TIMENA_ON_TRIGGER_HIGH,
	kFLEXIO_TIMENA_ON_TRIGGER_AND_PIN_HIGH,
	kFLEXIO_TIMENA_ON_PIN_RISING_EDGE,
	kFLEXIO_TIMENA_ON_PIN_BOTH_EDGES,
	kFLEXIO_TIMENA_ON_TRIGGER_RISING_EDGE,
	kFLEXIO_TIMENA_ON_TRIGGER_BOTH_EDGES
};

enum{
	kFLEXIO_TSTOP_DISABLED,
	kFLEXIO_TSTOP_ENABLED_ON_COMPARE,
	kFLEXIO_TSTOP_ENABLED_ON_TIMER_DISABLED,
	kFLEXIO_TSTOP_ENABLED_ON_TIMER_COMPARE
};

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t 			:1;  							// Reserved
		uint32_t TSTART		:1;	  							// Timer Start Bit
		uint32_t 	 		:2;								// Reserved
		uint32_t TSTOP		:2;	  							// Timer Stop Bit
		uint32_t 			:2;	  							// Reserved
		uint32_t TIMENA		:3;	  							// Timer Enable
		uint32_t 			:1;	  							// Reserved
		uint32_t TIMDIS		:3;	  							// Timer Disable
		uint32_t 			:1;	  							// Reserved
		uint32_t TIMRST		:3;	  							// Timer Reset
		uint32_t 			:1;	  							// Reserved
		uint32_t TIMDEC		:2;	  							// Timer Decrement
		uint32_t 			:2;	  							// Reserved
		uint32_t TIMOUT		:2;	  							// Timer Output
		uint32_t 			:6;	  							// Reserved
	} BITS;
} __flexio_timcfgn_t;


#define mFLEXIO_TIMCMPn_CMP(X) (X & 0x0000FFFFU)			// Timer Compare Value

typedef union {
	uint32_t WORD;											// Access to the whole register
	struct {
		uint32_t CMP		:16;  							// Timer Compare Value
		uint32_t 			:16;	  						// Reserved
	} BITS;
} __flexio_timcmpn_t;

typedef struct{
	volatile __flexio_verid_t VERID;					// x000 Version ID Register (FLEXIO_VERID)
	volatile __flexio_param_t PARAM;					// x004 Parameter Register (FLEXIO_PARAM)
	volatile __flexio_ctrl_t CTRL;						// x008 FlexIO Control Register (FLEXIO_CTRL)
	volatile uint32_t RESERVED0;						// x00C Reserved
	volatile __flexio_shiftstat_t SHIFTSTAT;			// x010 Shifter Status Register (FLEXIO_SHIFTSTAT)
	volatile __flexio_shifterr_t SHIFTERR;				// x014 Shifter Error Register (FLEXIO_SHIFTERR)
	volatile __flexio_timstat_t TIMSTAT;				// x018 Timer Status Register (FLEXIO_TIMSTAT)
	volatile uint32_t RESERVED1;						// x01C Reserved
	volatile __flexio_shiftsien_t SHIFTSIEN;			// x020 Shifter Status Interrupt Enable (FLEXIO_SHIFTSIEN)
	volatile __flexio_shifteien_t SHIFTEIEN;			// x024 Shifter Error Interrupt Enable (FLEXIO_SHIFTEIEN)
	volatile __flexio_timien_t TIMIEN;					// x028 Timer Interrupt Enable Register (FLEXIO_TIMIEN)
	volatile uint32_t RESERVED2;						// x02C Reserved
	volatile __flexio_shiftsden_t SHIFTSDEN;			// x030 Shifter Status DMA Enable (FLEXIO_SHIFTSDEN)
	volatile uint32_t RESERVED3[20];					// x034 - x07C Reserved
	volatile __flexio_shiftctln_t SHIFTCTL0;			// x080 Shifter Control N Register (FLEXIO_SHIFTCTL0)
	volatile __flexio_shiftctln_t SHIFTCTL1;			// x084 Shifter Control N Register (FLEXIO_SHIFTCTL1)
	volatile __flexio_shiftctln_t SHIFTCTL2;			// x088 Shifter Control N Register (FLEXIO_SHIFTCTL2)
	volatile __flexio_shiftctln_t SHIFTCTL3;			// x08C Shifter Control N Register (FLEXIO_SHIFTCTL3)
	volatile uint32_t RESERVED4[4];						// x090 - x09C Reserved
	volatile __flexio_shiftcfgn_t SHIFTCFG0;			// x100 Shifter Configuration N Register (FLEXIO_SHIFTCFG0)
	volatile __flexio_shiftcfgn_t SHIFTCFG1;			// x104 Shifter Configuration N Register (FLEXIO_SHIFTCFG1)
	volatile __flexio_shiftcfgn_t SHIFTCFG2;			// x108 Shifter Configuration N Register (FLEXIO_SHIFTCFG2)
	volatile __flexio_shiftcfgn_t SHIFTCFG3;			// x10C Shifter Configuration N Register (FLEXIO_SHIFTCFG3)
	volatile uint32_t RESERVED5[60];					// x110 - x1FC Reserved
	volatile __flexio_shiftbufn_t SHIFTBUF0;			// x200 Shifter Buffer N Register (FLEXIO_SHIFTBUF0)
	volatile __flexio_shiftbufn_t SHIFTBUF1;			// x204 Shifter Buffer N Register (FLEXIO_SHIFTBUF1)
	volatile __flexio_shiftbufn_t SHIFTBUF2;			// x208 Shifter Buffer N Register (FLEXIO_SHIFTBUF2)
	volatile __flexio_shiftbufn_t SHIFTBUF3;			// x20C Shifter Buffer N Register (FLEXIO_SHIFTBUF3)
	volatile uint32_t RESERVED6[28];					// x210 - x27C Reserved
	volatile __flexio_shiftbufbisn_t SHIFTBUFBIS0;		// x280 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS0)
	volatile __flexio_shiftbufbisn_t SHIFTBUFBIS1;		// x284 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS1)
	volatile __flexio_shiftbufbisn_t SHIFTBUFBIS2;		// x288 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS2)
	volatile __flexio_shiftbufbisn_t SHIFTBUFBIS3;		// x28C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS3)
	volatile uint32_t RESERVED7[28];					// x290 - x2FC Reserved
	volatile __flexio_shiftbufbysn_t SHIFTBUFBYS0;		// x300 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS0)
	volatile __flexio_shiftbufbysn_t SHIFTBUFBYS1;		// x304 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS1)
	volatile __flexio_shiftbufbysn_t SHIFTBUFBYS2;		// x308 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS2)
	volatile __flexio_shiftbufbysn_t SHIFTBUFBYS3;		// x30C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS3)
	volatile uint32_t RESERVED8[28];					// x310 - x37C Reserved
	volatile __flexio_shiftbufbbsn_t SHIFTBUFBBS0;		// x380 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS0)
	volatile __flexio_shiftbufbbsn_t SHIFTBUFBBS1;		// x384 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS1)
	volatile __flexio_shiftbufbbsn_t SHIFTBUFBBS2;		// x388 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS2)
	volatile __flexio_shiftbufbbsn_t SHIFTBUFBBS3;		// x38C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS3)
	volatile uint32_t RESERVED9[28];					// x390 - x3FC Reserved
	volatile __flexio_timctln_t TIMCTL0;				// x400 Timer Control N Register (FLEXIO_TIMCTL0)
	volatile __flexio_timctln_t TIMCTL1;				// x404 Timer Control N Register (FLEXIO_TIMCTL1)
	volatile __flexio_timctln_t TIMCTL2;				// x408 Timer Control N Register (FLEXIO_TIMCTL2)
	volatile __flexio_timctln_t TIMCTL3;				// x40C Timer Control N Register (FLEXIO_TIMCTL3)
	volatile uint32_t RESERVEDA[28];					// x410 - x47C Reserved
	volatile __flexio_timcfgn_t TIMCFG0;				// x480 Timer Configuration N Register (FLEXIO_TIMCFG0)
	volatile __flexio_timcfgn_t TIMCFG1;				// x484 Timer Configuration N Register (FLEXIO_TIMCFG1)
	volatile __flexio_timcfgn_t TIMCFG2;				// x488 Timer Configuration N Register (FLEXIO_TIMCFG2)
	volatile __flexio_timcfgn_t TIMCFG3;				// x48C Timer Configuration N Register (FLEXIO_TIMCFG3)
	volatile uint32_t RESERVEDB[28];					// x490 - x4FC Reserved
	volatile __flexio_timcmpn_t TIMCMP0;				// x500 Timer Compare N Register (FLEXIO_TIMCMP0)
	volatile __flexio_timcmpn_t TIMCMP1;				// x504Timer Compare N Register (FLEXIO_TIMCMP1)
	volatile __flexio_timcmpn_t TIMCMP2;				// x508Timer Compare N Register (FLEXIO_TIMCMP2)
	volatile __flexio_timcmpn_t TIMCMP3;				// x50cTimer Compare N Register (FLEXIO_TIMCMP3)
}__flexio_t;

#define sFLEXIO (*((__flexio_t *)(0x4005F000)))			// FLEXIO  General register structure

#define rFLEXIO_VERID sFLEXIO.VERID.WORD					// x000 Version ID Register (FLEXIO_VERID)
#define bFLEXIO_FEATURE sFLEXIO.VERID.BITS.FEATURE  						// Feature Specification Number
#define bFLEXIO_MINOR sFLEXIO.VERID.BITS.MINOR	  						// Minor Version Number
#define bFLEXIO_MAJOR sFLEXIO.VERID.BITS.MAJOR							// Major Version Number

#define rFLEXIO_PARAM sFLEXIO.PARAM.WORD					// x004 Parameter Register (FLEXIO_PARAM)
#define bFLEXIO_SHIFTER sFLEXIO.PARAM.BITS.SHIFTER  						// Shifter Number
#define bFLEXIO_TIMER sFLEXIO.PARAM.BITS.TIMER	  						// Timer Number
#define bFLEXIO_PIN sFLEXIO.PARAM.BITS.PIN							// Pin Number
#define bFLEXIO_TRIGGER sFLEXIO.PARAM.BITS.TRIGGER	  						// Trigger Number

#define rFLEXIO_CTRL sFLEXIO.CTRL.WORD						// x008 FlexIO Control Register (FLEXIO_CTRL)
#define bFLEXIO_FLEXEN sFLEXIO.CTRL.BITS.FLEXEN  							// FlexIO Enable
#define bFLEXIO_SWRST sFLEXIO.CTRL.BITS.SWRST	  							// Software Reset
#define bFLEXIO_FASTACC sFLEXIO.CTRL.BITS.FASTACC								// Fast Access
#define bFLEXIO_DBGE sFLEXIO.CTRL.BITS.DBGE	  							// Debug Enable
#define bFLEXIO_DOZEN sFLEXIO.CTRL.BITS.DOZEN	  							// Doze Enable

#define rFLEXIO_SHIFTSTAT sFLEXIO.SHIFTSTAT.WORD			// x010 Shifter Status Register (FLEXIO_SHIFTSTAT)
#define bFLEXIO_SSF sFLEXIO.SHIFTSTAT.BITS.SSF  							// Shifter Status Flag

#define rFLEXIO_SHIFTERR sFLEXIO.SHIFTERR.WORD				// x014 Shifter Error Register (FLEXIO_SHIFTERR)
#define bFLEXIO_SEF	sFLEXIO.SHIFTERR.BITS.SEF 							// Shifter Error Flags

#define rFLEXIO_TIMSTAT sFLEXIO.TIMSTAT.WORD				// x018 Timer Status Register (FLEXIO_TIMSTAT)
#define bFLEXIO_TSF	sFLEXIO.TIMSTAT.BITS.TSF  							// Timer Status Flags

#define rFLEXIO_SHIFTSIEN sFLEXIO.SHIFTSIEN.WORD			// x020 Shifter Status Interrupt Enable (FLEXIO_SHIFTSIEN)
#define bFLEXIO_SSIE sFLEXIO.SHIFTSIEN.BITS.SSIE  							// Shifter Status Interrupt Enable

#define rFLEXIO_SHIFTEIEN sFLEXIO.SHIFTEIEN.WORD			// x024 Shifter Error Interrupt Enable (FLEXIO_SHIFTEIEN)
#define bFLEXIO_SEIE sFLEXIO.SHIFTEIEN.BITS.SEIE  							// Shifter Error Interrupt Enable

#define rFLEXIO_TIMIEN sFLEXIO.TIMIEN.WORD					// x028 Timer Interrupt Enable Register (FLEXIO_TIMIEN)
#define bFLEXIO_TEIE sFLEXIO.TIMIEN.BITS.TEIE  							// Timer Status Interrupt Enable

#define rFLEXIO_SHIFTSDEN sFLEXIO.SHIFTSDEN.WORD			// x030 Shifter Status DMA Enable (FLEXIO_SHIFTSDEN)
#define bFLEXIO_SSDE sFLEXIO.SHIFTSDEN.BITS.SSDE  							// Shifter Status DMA Enable

#define rFLEXIO_SHIFTCTL0 sFLEXIO.SHIFTCTL0.WORD			// x080 Shifter Control N Register (FLEXIO_SHIFTCTL0)
#define bFLEXIO_SH0_SMOD sFLEXIO.SHIFTCTL0.BITS.SMOD  		// Shifter Mode
#define bFLEXIO_SH0_PINPOL sFLEXIO.SHIFTCTL0.BITS.PINPOL	// Shifter Pin Polarity
#define bFLEXIO_SH0_PINSEL sFLEXIO.SHIFTCTL0.BITS.PINSEL	// Shifter Pin Select
#define bFLEXIO_SH0_PINCFG sFLEXIO.SHIFTCTL0.BITS.PINCFG	// Shifter Pin Configuration
#define bFLEXIO_SH0_TIMPOL sFLEXIO.SHIFTCTL0.BITS.TIMPOL	// Timer Polarity
#define bFLEXIO_SH0_TIMSEL sFLEXIO.SHIFTCTL0.BITS.TIMSEL	// Timer Select

#define rFLEXIO_SHIFTCTL1 sFLEXIO.SHIFTCTL1.WORD			// x084 Shifter Control N Register (FLEXIO_SHIFTCTL1)
#define bFLEXIO_SH1_SMOD sFLEXIO.SHIFTCTL1.BITS.SMOD  		// Shifter Mode
#define bFLEXIO_SH1_PINPOL sFLEXIO.SHIFTCTL1.BITS.PINPOL	// Shifter Pin Polarity
#define bFLEXIO_SH1_PINSEL sFLEXIO.SHIFTCTL1.BITS.PINSEL	// Shifter Pin Select
#define bFLEXIO_SH1_PINCFG sFLEXIO.SHIFTCTL1.BITS.PINCFG	// Shifter Pin Configuration
#define bFLEXIO_SH1_TIMPOL sFLEXIO.SHIFTCTL1.BITS.TIMPOL	// Timer Polarity
#define bFLEXIO_SH1_TIMSEL sFLEXIO.SHIFTCTL1.BITS.TIMSEL	// Timer Select

#define rFLEXIO_SHIFTCTL2 sFLEXIO.SHIFTCTL2.WORD			// x088 Shifter Control N Register (FLEXIO_SHIFTCTL2)
#define bFLEXIO_SH2_SMOD sFLEXIO.SHIFTCTL2.BITS.SMOD  		// Shifter Mode
#define bFLEXIO_SH2_PINPOL sFLEXIO.SHIFTCTL2.BITS.PINPOL	// Shifter Pin Polarity
#define bFLEXIO_SH2_PINSEL sFLEXIO.SHIFTCTL2.BITS.PINSEL	// Shifter Pin Select
#define bFLEXIO_SH2_PINCFG sFLEXIO.SHIFTCTL2.BITS.PINCFG	// Shifter Pin Configuration
#define bFLEXIO_SH2_TIMPOL sFLEXIO.SHIFTCTL2.BITS.TIMPOL	// Timer Polarity
#define bFLEXIO_SH2_TIMSEL sFLEXIO.SHIFTCTL2.BITS.TIMSEL	// Timer Select

#define rFLEXIO_SHIFTCTL3 sFLEXIO.SHIFTCTL3.WORD			// x08C Shifter Control N Register (FLEXIO_SHIFTCTL3)
#define bFLEXIO_SH3_SMOD sFLEXIO.SHIFTCTL3.BITS.SMOD  		// Shifter Mode
#define bFLEXIO_SH3_PINPOL sFLEXIO.SHIFTCTL3.BITS.PINPOL	// Shifter Pin Polarity
#define bFLEXIO_SH3_PINSEL sFLEXIO.SHIFTCTL3.BITS.PINSEL	// Shifter Pin Select
#define bFLEXIO_SH3_PINCFG sFLEXIO.SHIFTCTL3.BITS.PINCFG	// Shifter Pin Configuration
#define bFLEXIO_SH3_TIMPOL sFLEXIO.SHIFTCTL3.BITS.TIMPOL	// Timer Polarity
#define bFLEXIO_SH3_TIMSEL sFLEXIO.SHIFTCTL3.BITS.TIMSEL	// Timer Select

#define rFLEXIO_SHIFTCFG0 sFLEXIO.SHIFTCFG0.WORD			// x100 Shifter Configuration N Register (FLEXIO_SHIFTCFG0)
#define bFLEXIO_SH0_SSTART sFLEXIO.SHIFTCFG0.BITS.SSTART	// Shifter Start bit
#define bFLEXIO_SH0_SSTOP sFLEXIO.SHIFTCFG0.BITS.SSTOP		// Shifter Stop bit
#define bFLEXIO_SH0_INSRC sFLEXIO.SHIFTCFG0.BITS.INSRC		// Input Source

#define rFLEXIO_SHIFTCFG1 sFLEXIO.SHIFTCFG1.WORD			// x104 Shifter Configuration N Register (FLEXIO_SHIFTCFG1)
#define bFLEXIO_SH1_SSTART sFLEXIO.SHIFTCFG1.BITS.SSTART	// Shifter Start bit
#define bFLEXIO_SH1_SSTOP sFLEXIO.SHIFTCFG1.BITS.SSTOP		// Shifter Stop bit
#define bFLEXIO_SH1_INSRC sFLEXIO.SHIFTCFG1.BITS.INSRC		// Input Source

#define rFLEXIO_SHIFTCFG2 sFLEXIO.SHIFTCFG2.WORD			// x108 Shifter Configuration N Register (FLEXIO_SHIFTCFG2)
#define bFLEXIO_SH2_SSTART sFLEXIO.SHIFTCFG2.BITS.SSTART	// Shifter Start bit
#define bFLEXIO_SH2_SSTOP sFLEXIO.SHIFTCFG2.BITS.SSTOP		// Shifter Stop bit
#define bFLEXIO_SH2_INSRC sFLEXIO.SHIFTCFG2.BITS.INSRC		// Input Source

#define rFLEXIO_SHIFTCFG3 sFLEXIO.SHIFTCFG3.WORD			// x10C Shifter Configuration N Register (FLEXIO_SHIFTCFG3)
#define bFLEXIO_SH3_SSTART sFLEXIO.SHIFTCFG3.BITS.SSTART	// Shifter Start bit
#define bFLEXIO_SH3_SSTOP sFLEXIO.SHIFTCFG3.BITS.SSTOP		// Shifter Stop bit
#define bFLEXIO_SH3_INSRC sFLEXIO.SHIFTCFG3.BITS.INSRC		// Input Source

#define rFLEXIO_SHIFTBUF0 sFLEXIO.SHIFTBUF0.WORD			// x200 Shifter Buffer N Register (FLEXIO_SHIFTBUF0)

#define rFLEXIO_SHIFTBUF1 sFLEXIO.SHIFTBUF1.WORD			// x204 Shifter Buffer N Register (FLEXIO_SHIFTBUF1)

#define rFLEXIO_SHIFTBUF2 sFLEXIO.SHIFTBUF2.WORD			// x208 Shifter Buffer N Register (FLEXIO_SHIFTBUF2)

#define rFLEXIO_SHIFTBUF3 sFLEXIO.SHIFTBUF3.WORD			// x20C Shifter Buffer N Register (FLEXIO_SHIFTBUF3)

#define rFLEXIO_SHIFTBUFBIS0 sFLEXIO.SHIFTBUFBIS0.WORD		// x280 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS0)

#define rFLEXIO_SHIFTBUFBIS1 sFLEXIO.SHIFTBUFBIS1.WORD		// x284 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS1)

#define rFLEXIO_SHIFTBUFBIS2 sFLEXIO.SHIFTBUFBIS2.WORD		// x288 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS2)

#define rFLEXIO_SHIFTBUFBIS3 sFLEXIO.SHIFTBUFBIS3.WORD		// x28C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBIS3)

#define rFLEXIO_SHIFTBUFBYS0 sFLEXIO.SHIFTBUFBYS0.WORD		// x300 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS0)

#define rFLEXIO_SHIFTBUFBYS1 sFLEXIO.SHIFTBUFBYS1.WORD		// x304 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS1)

#define rFLEXIO_SHIFTBUFBYS2 sFLEXIO.SHIFTBUFBYS2.WORD		// x308 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS2)

#define rFLEXIO_SHIFTBUFBYS3 sFLEXIO.SHIFTBUFBYS3.WORD		// x30C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBYS3)

#define rFLEXIO_SHIFTBUFBBS0 sFLEXIO.SHIFTBUFBBS0.WORD		// x380 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS0)

#define rFLEXIO_SHIFTBUFBBS1 sFLEXIO.SHIFTBUFBBS1.WORD		// x384 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS1)

#define rFLEXIO_SHIFTBUFBBS2 sFLEXIO.SHIFTBUFBBS2.WORD		// x388 Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS2)

#define rFLEXIO_SHIFTBUFBBS3 sFLEXIO.SHIFTBUFBBS3.WORD		// x38C Shifter Buffer N Bit Swapped Register (FLEXIO_SHIFTBUFBBS3)

#define rFLEXIO_TIMCTL0 sFLEXIO.TIMCTL0.WORD				// x400 Timer Control N Register (FLEXIO_TIMCTL0)
#define bFLEXIO_TIM0_TIMOD sFLEXIO.TIMCTL0.BITS.TIMOD  		// Timer Mode
#define bFLEXIO_TIM0_PINPOL sFLEXIO.TIMCTL0.BITS.PINPOL		// Timer Pin Polarity
#define bFLEXIO_TIM0_PINSEL sFLEXIO.TIMCTL0.BITS.PINSEL		// Timer Pin Select
#define bFLEXIO_TIM0_PINCFG sFLEXIO.TIMCTL0.BITS.PINCFG		// Timer Pin Configuration
#define bFLEXIO_TIM0_TRGSRC sFLEXIO.TIMCTL0.BITS.TRGSRC		// Trigger Source
#define bFLEXIO_TIM0_TRGPOL sFLEXIO.TIMCTL0.BITS.TRGPOL		// Trigger Polarity
#define bFLEXIO_TIM0_TRGSEL sFLEXIO.TIMCTL0.BITS.TRGSEL		// Trigger Select

#define rFLEXIO_TIMCTL1 sFLEXIO.TIMCTL1.WORD				// x404 Timer Control N Register (FLEXIO_TIMCTL1)
#define bFLEXIO_TIM1_TIMOD sFLEXIO.TIMCTL1.BITS.TIMOD  		// Timer Mode
#define bFLEXIO_TIM1_PINPOL sFLEXIO.TIMCTL1.BITS.PINPOL		// Timer Pin Polarity
#define bFLEXIO_TIM1_PINSEL sFLEXIO.TIMCTL1.BITS.PINSEL		// Timer Pin Select
#define bFLEXIO_TIM1_PINCFG sFLEXIO.TIMCTL1.BITS.PINCFG		// Timer Pin Configuration
#define bFLEXIO_TIM1_TRGSRC sFLEXIO.TIMCTL1.BITS.TRGSRC		// Trigger Source
#define bFLEXIO_TIM1_TRGPOL sFLEXIO.TIMCTL1.BITS.TRGPOL		// Trigger Polarity
#define bFLEXIO_TIM1_TRGSEL sFLEXIO.TIMCTL1.BITS.TRGSEL		// Trigger Select

#define rFLEXIO_TIMCTL2 sFLEXIO.TIMCTL2.WORD				// x408 Timer Control N Register (FLEXIO_TIMCTL2)
#define bFLEXIO_TIM2_TIMOD sFLEXIO.TIMCTL2.BITS.TIMOD  		// Timer Mode
#define bFLEXIO_TIM2_PINPOL sFLEXIO.TIMCTL2.BITS.PINPOL		// Timer Pin Polarity
#define bFLEXIO_TIM2_PINSEL sFLEXIO.TIMCTL2.BITS.PINSEL		// Timer Pin Select
#define bFLEXIO_TIM2_PINCFG sFLEXIO.TIMCTL2.BITS.PINCFG		// Timer Pin Configuration
#define bFLEXIO_TIM2_TRGSRC sFLEXIO.TIMCTL2.BITS.TRGSRC		// Trigger Source
#define bFLEXIO_TIM2_TRGPOL sFLEXIO.TIMCTL2.BITS.TRGPOL		// Trigger Polarity
#define bFLEXIO_TIM2_TRGSEL sFLEXIO.TIMCTL2.BITS.TRGSEL		// Trigger Select

#define rFLEXIO_TIMCTL3 sFLEXIO.TIMCTL3.WORD				// x40C Timer Control N Register (FLEXIO_TIMCTL3)
#define bFLEXIO_TIM3_TIMOD sFLEXIO.TIMCTL3.BITS.TIMOD  		// Timer Mode
#define bFLEXIO_TIM3_PINPOL sFLEXIO.TIMCTL3.BITS.PINPOL		// Timer Pin Polarity
#define bFLEXIO_TIM3_PINSEL sFLEXIO.TIMCTL3.BITS.PINSEL		// Timer Pin Select
#define bFLEXIO_TIM3_PINCFG sFLEXIO.TIMCTL3.BITS.PINCFG		// Timer Pin Configuration
#define bFLEXIO_TIM3_TRGSRC sFLEXIO.TIMCTL3.BITS.TRGSRC		// Trigger Source
#define bFLEXIO_TIM3_TRGPOL sFLEXIO.TIMCTL3.BITS.TRGPOL		// Trigger Polarity
#define bFLEXIO_TIM3_TRGSEL sFLEXIO.TIMCTL3.BITS.TRGSEL		// Trigger Select

#define rFLEXIO_TIMCFG0 sFLEXIO.TIMCFG0.WORD				// x480 Timer Configuration N Register (FLEXIO_TIMCFG0)
#define bFLEXIO_TIM0_TSTART sFLEXIO.TIMCFG0.BITS.TSTART		// Timer Start Bit
#define bFLEXIO_TIM0_TSTOP sFLEXIO.TIMCFG0.BITS.TSTOP		// Timer Stop Bit
#define bFLEXIO_TIM0_TIMENA sFLEXIO.TIMCFG0.BITS.TIMENA		// Timer Enable
#define bFLEXIO_TIM0_TIMDIS sFLEXIO.TIMCFG0.BITS.TIMDIS		// Timer Disable
#define bFLEXIO_TIM0_TIMRST sFLEXIO.TIMCFG0.BITS.TIMRST		// Timer Reset
#define bFLEXIO_TIM0_TIMDEC sFLEXIO.TIMCFG0.BITS.TIMDEC		// Timer Decrement
#define bFLEXIO_TIM0_TIMOUT sFLEXIO.TIMCFG0.BITS.TIMOUT		// Timer Output

#define rFLEXIO_TIMCFG1 sFLEXIO.TIMCFG1.WORD				// x484 Timer Configuration N Register (FLEXIO_TIMCFG1)
#define bFLEXIO_TIM1_TSTART sFLEXIO.TIMCFG1.BITS.TSTART		// Timer Start Bit
#define bFLEXIO_TIM1_TSTOP sFLEXIO.TIMCFG1.BITS.TSTOP		// Timer Stop Bit
#define bFLEXIO_TIM1_TIMENA sFLEXIO.TIMCFG1.BITS.TIMENA		// Timer Enable
#define bFLEXIO_TIM1_TIMDIS sFLEXIO.TIMCFG1.BITS.TIMDIS		// Timer Disable
#define bFLEXIO_TIM1_TIMRST sFLEXIO.TIMCFG1.BITS.TIMRST		// Timer Reset
#define bFLEXIO_TIM1_TIMDEC sFLEXIO.TIMCFG1.BITS.TIMDEC		// Timer Decrement
#define bFLEXIO_TIM1_TIMOUT sFLEXIO.TIMCFG1.BITS.TIMOUT		// Timer Output

#define rFLEXIO_TIMCFG2 sFLEXIO.TIMCFG2.WORD				// x488 Timer Configuration N Register (FLEXIO_TIMCFG2)
#define bFLEXIO_TIM2_TSTART sFLEXIO.TIMCFG2.BITS.TSTART		// Timer Start Bit
#define bFLEXIO_TIM2_TSTOP sFLEXIO.TIMCFG2.BITS.TSTOP		// Timer Stop Bit
#define bFLEXIO_TIM2_TIMENA sFLEXIO.TIMCFG2.BITS.TIMENA		// Timer Enable
#define bFLEXIO_TIM2_TIMDIS sFLEXIO.TIMCFG2.BITS.TIMDIS		// Timer Disable
#define bFLEXIO_TIM2_TIMRST sFLEXIO.TIMCFG2.BITS.TIMRST		// Timer Reset
#define bFLEXIO_TIM2_TIMDEC sFLEXIO.TIMCFG2.BITS.TIMDEC		// Timer Decrement
#define bFLEXIO_TIM2_TIMOUT sFLEXIO.TIMCFG2.BITS.TIMOUT		// Timer Output

#define rFLEXIO_TIMCFG3 sFLEXIO.TIMCFG3.WORD				// x48C Timer Configuration N Register (FLEXIO_TIMCFG3)
#define bFLEXIO_TIM3_TSTART sFLEXIO.TIMCFG3.BITS.TSTART		// Timer Start Bit
#define bFLEXIO_TIM3_TSTOP sFLEXIO.TIMCFG3.BITS.TSTOP		// Timer Stop Bit
#define bFLEXIO_TIM3_TIMENA sFLEXIO.TIMCFG3.BITS.TIMENA		// Timer Enable
#define bFLEXIO_TIM3_TIMDIS sFLEXIO.TIMCFG3.BITS.TIMDIS		// Timer Disable
#define bFLEXIO_TIM3_TIMRST sFLEXIO.TIMCFG3.BITS.TIMRST		// Timer Reset
#define bFLEXIO_TIM3_TIMDEC sFLEXIO.TIMCFG3.BITS.TIMDEC		// Timer Decrement
#define bFLEXIO_TIM3_TIMOUT sFLEXIO.TIMCFG3.BITS.TIMOUT		// Timer Output

#define rFLEXIO_TIMCMP0 sFLEXIO.TIMCMP0.WORD				// x500 Timer Compare N Register (FLEXIO_TIMCMP0)
#define bFLEXIO_TIM0_CMP sFLEXIO.TIMCMP0.BITS.CMP  			// Timer Compare Value

#define rFLEXIO_TIMCMP1 sFLEXIO.TIMCMP1.WORD				// x504Timer Compare N Register (FLEXIO_TIMCMP1)
#define bFLEXIO_TIM1_CMP sFLEXIO.TIMCMP1.BITS.CMP  			// Timer Compare Value

#define rFLEXIO_TIMCMP2 sFLEXIO.TIMCMP2.WORD				// x508Timer Compare N Register (FLEXIO_TIMCMP2)
#define bFLEXIO_TIM2_CMP sFLEXIO.TIMCMP2.BITS.CMP  			// Timer Compare Value

#define rFLEXIO_TIMCMP3 sFLEXIO.TIMCMP3.WORD				// x50cTimer Compare N Register (FLEXIO_TIMCMP3)
#define bFLEXIO_TIM3_CMP sFLEXIO.TIMCMP3.BITS.CMP  			// Timer Compare Value

#endif /* FLEXIO_LIB_H_ */

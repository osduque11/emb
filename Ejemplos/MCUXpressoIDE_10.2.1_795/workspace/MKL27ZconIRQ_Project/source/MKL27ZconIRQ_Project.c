/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    MKL27ZconIRQ_Project.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"

#define RED_LED  1<<18
#define GREEN_LED  1<<19

#define SW3_PRESS  GPIOC->PDIR && 0x2)

#define NMI_LED_HANDLER NMI_Handler // interrupcción no enmascarable
#define PTC1_SW3_IRQ PORTB_PORTC_PORTD_PORTE_IRQHandler //Numero del vector 47

volatile bool NMIIsrFlag = false;

/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */

	void NMI_LED_HANDLER(void)
	{
		NMIIsrFlag = true;
	}
	void PTC1_SW3_IRQ(void)
	{
		GPIOB->PTOR=RED_LED;
		PORTC->PCR[1] |= PORT_PCR_ISF_MASK;
	}

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
    NVIC_EnableIRQ(PORTB_PORTC_PORTD_PORTE_IRQn); // 31 interrupcción que proviene de los puertos


    printf("Hello World\n");


    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */


    while(1) {
        i++ ;
//        if(GPIOC->PDIR && 0x2)  		// if SW3 PRESS
//        	GPIOB->PDOR |= RED_LED; 	// PTB18 = 1 (Red LED OFF)
//        	//GPIOB->PSOR= RED_LED; 	// PTB18 = 1 (Red LED OFF)
//        else
//        	GPIOB->PDOR &= ~RED_LED; 	// PTB18 = 0 (Red LED ON)
//        	//GPIOB->PCOR= RED_LED; 	// PTB18 = 1 (Red LED OFF)
//
        /* Check whether occur interupt and toggle LED */
        if (true == NMIIsrFlag)
        {
        	GPIOB->PTOR=GREEN_LED;		//LED TOGGLE
            NMIIsrFlag = false;
        }

    }
    return 0 ;
}

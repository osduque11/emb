/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Magnetometro.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
#include "SIM_LIB.h"
#include "PORT_LIB.h"
#include "i2c_data_lib.h"
#include "I2C_LIB.h"
#define MAG3110 0x1C
#define CTRL_REG1 0x10
#define CTRL_REG2 0x11
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int readacc(uint8_t data, uint8_t direccion){
	int16_t accx;
	int i;
    int error = i2c1_single_byte_read(&data, 0x07);		//Leer direccion 0x0D

	error = i2c1_single_byte_read(&data, direccion);
	if(!error){
		accx = data<<8;
	}
	error = i2c1_single_byte_read(&data, direccion+1);
	if(!error){
		accx |= data;
	}

	for(i=0; i<100000;i++);
	return accx;
}

int X_max = -200;
int X_min = 0;

int Y_max = 0;
int Y_min = 3000;

#define X_OFFSET -264
#define Y_OFFSET 2656

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    bSIM_CG_I2C1 = 1;			// Disable I2C clock gating
    bSIM_CG_PORTD = 1;			// Disable PORTD clock gating
    bSIM_CG_PORTC = 1;			// Disable PORTC clock gating

    bPTD7_MUX = kPTD7_MUX_I2C1_SCL;	// PTD7 as SCL signal for I2C1 module
    bPTD6_MUX = kPTD6_MUX_I2C1_SDA; // PTD6 as SDA signal for I2C1 module
    bPTD7_PS = 1;					// Pull up selected for PTD7
    bPTD6_PS = 1;					// Pull up selected for PTD6
    bPTD7_PE = 1;					// PTD7 pull enable required by I2C interface
    bPTD6_PE = 1;					// PTD6 pull enable required by I2C interface
    bPTD7_DSE = 1;					// PTD7 pull enable required by I2C interface
    bPTD6_DSE = 1;					// PTD6 pull enable required by I2C interface

    bPTC2_MUX = kPTC2_MUX_GPIO;		// MMA8451 Interrupt
    bPTC2_IRQC = kPORT_IRQC_INTERRUPT_RISING_EDGE;

    mag3110_init(MAG3110);


    printf("Let's try!!!");
    	    uint8_t data = 0x80;
    	    //Enable reset: Automatic Magnetic Sensor Reset. reg 2 0x11, bit 11
    	    int error=i2c1_single_byte_write(data,CTRL_REG2);
    	    if (!error)
    	        printf("\nReset Enable\n");
    	    //Leer direccion 0x07 who Am I?
			error = i2c1_single_byte_read(&data, 0x07);
    	    if (!error)
    	    	printf("\nWho AM I?: %x\n",data);

    	  //  data = X_OFFSET >> 8;
    	    error=i2c1_single_byte_write(0,0x09);
    	  //  data = X_OFFSET & 0xFF;
    	    error=i2c1_single_byte_write(0,0x0A);

    	   // data = Y_OFFSET >> 8;
    	    error=i2c1_single_byte_write(0,0x0B);
    	   // data = Y_OFFSET & 0xFF;
    	    error=i2c1_single_byte_write(0,0x0C);

    	    printf("off_x: %d, off_y: %d",readacc(data,0x09),readacc(data,0x0B));

    	    // Cambiar modo de operación a activo
    	    error=i2c1_single_byte_write(0x11,CTRL_REG1);

        	/* Force the counter to be placed into memory. */
        		    volatile static int32_t i = 0 ;

        		    int16_t accx, accy, accz;
        		    accx = 0;
        		    accy = 0;
        		    accz = 0;
        		    /* Enter an infinite loop, just incrementing a counter. */
        		    while(1) {
        		    	accx = readacc(data,0x01);
        		    	accy = readacc(data,0x03);
        		    	accz = readacc(data,0x05);

        		    	if(accx < X_min){
        		    		X_min = accx;
        		    	}
        		    	if(accy < Y_min){
							Y_min = accy;
						}

        		    	if(accx > X_max){
        		    		X_max = accx;
        		    	}
        		    	if(accy > Y_max){
        		    		Y_max = accy;
        		    	}
        		    	printf("X: %d Y:%d Z:%d\n",accx,accy,accz);
        		        i++ ;
        		    }
    return 0 ;
}

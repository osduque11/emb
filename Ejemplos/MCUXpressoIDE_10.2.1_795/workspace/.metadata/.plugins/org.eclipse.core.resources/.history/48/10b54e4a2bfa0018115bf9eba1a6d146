/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Proyecto_Final_V3.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include <math.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */
#include "GPIO_LIB.h"
#include "PORT_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "TPM_LIB.h"
#include "LPUART_LIB.h"
#include "i2c_data_lib.h"
#include "I2C_LIB.h"
/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */

uint16_t conteo_hall = 0;		//Decrementa cada 6cm recorridos

uint8_t flag_motor = 0;			// 1->Motor encendido 0->Motor apagado
uint8_t flag_dir_Motor=0;       // 0->Adelante 1->Atras

uint32_t DatoCapturado=0; 		//var Ultra
uint32_t tiempoEvento = 0;		//var Ultra
uint8_t	 si=0;					//var Ultra
uint8_t  contOver=0;			//var Ultra
uint16_t Distancia=0;			//var ultra

void PWM_Init_Servo(void){

	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK (8MHz)
	bSIM_CG_TPM0 = 1;	//Habilita CLK al módulo TPM0
	bSIM_CG_PORTE = 1;	// Habilita reloj al módulo Port E.
	bPTE24_MUX = 3;		//PORTE24 es configurado como TPM0_CH0, ALT3

	bTPM0_PS = 3;		// Preescaladro, divide por 8-> 1MHZ
	bTPM0_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM0_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM0->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida

	rTPM0_MOD = 20000; //50hZ
	rTPM0_C0V = 1500;
}


void PosServo(uint8_t angulo){
	rTPM0_C0V = 500 + 11*angulo;
}

void PWM_Init_Motor(void){
	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK
	bSIM_CG_TPM1 = 1;	//Habilita CLK al módulo TPM1
	bSIM_CG_PORTA = 1;	// Habilita reloj al módulo Port A.
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	bGPIOA_PDDR12 = 1;	//PORTA12 es configurado como puerto out
	bGPIOE_PDDR20 = 1;	//PORTE20 es configurado como puerto out

	bGPIOA_PDOR12 = 0;	//PORTA12 es llevado a un nivel logico 0
	bGPIOE_PDOR20 = 0;	//PORTE20 es llevado a un nivel logico 0

	bTPM1_PS = 3;		// Preescaladro, divide por 8 trabajando con un reloj de 1MHz
	bTPM1_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM1_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM1->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida
	rTPM1_MOD = 5000;	//Frecuencia de la señal de 200Hz
	rTPM1_C0V = 2500;
}

void Avanzar(uint8_t Vel){ //ingresar velocidad de 0 a 100.
	bPTA12_MUX = 3;		//PORTA12 es configurado como PWM
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	if (Vel>=0 && Vel<=100){
		flag_motor = 1;
		flag_dir_Motor=0;
		rTPM1_C0V = Vel*50;}
	else
		rTPM1_C0V=0;
}

void Retroceder(uint8_t Vel){ //ingresar velocidad de 0 a 100.

	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTE20_MUX = 3;		//PORTE20 es configurado como PWM

	if (Vel>=0 && Vel<=100){
		rTPM1_C0V = Vel*50;
		flag_motor = 1;
		flag_dir_Motor=1;
	}
	else
		rTPM1_C0V=0;

}

void Parar(){
	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	flag_motor = 0;
}

//Sensor de efecto hall
void Input_Capturte_Init(){
	bSIM_CG_TPM2 = 1;	//Habilita CLK al módulo TPM2 8Mhz
	bPTA1_MUX = 3;		//PORTA1 es configurado timer
	bTPM2_PS = 7;		// Preescaladro, divide por 128 trabajando con un reloj de 62.5KHz
	bTPM2_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.
	bTPM2_DBGMODE = 3;
	TPM2->CONTROLS[0].CnSC |= TPM_CnSC_ELSB_MASK;

	rTPM2_MOD = 62500; // 1 overflow cada segundo
	bTPM2_CH0_CHIE = 1;  //Habilita interrupciones del canal
}

void TPM2_IRQHandler(){
	if (bTPM2_CH0F==1){
		bTPM2_CH0F=1;
		conteo_hall--;

		/*if(flag_dir_Motor==0){ //Carro avanzando
			if(conteo_hall == 1){
				Retroceder(30);
					}
			if(conteo_hall == 0){
				Parar();
			}
		}
		else{					//carro retrocediendo
			if(conteo_hall == 1){
				Avanzar(30);
								}
			if(conteo_hall == 0){
				Parar();
			}
		}*/
	}
}

//Ultrasonido
void Input_capture_Init_Ultra(void){
	bPTE25_MUX=3; //Timer 0 canal 1;
	//TPM0->CONTROLS[1].CnSC |= 0x6;  //rising and falling edge
	bTPM0_CH1_ELS=3;
	bTPM0_CH1_CHIE = 1;  //Habilita interrupciones del canal

}
void Init_trigger(void){
	bPTA5_MUX=1; // pin
	bGPIOA_PDDR5 = 1; //Pin A5 como salida

}
void Trigger_ultra(void){
	bGPIOA_PDOR5 = 1; //Pin A5  en 1 lógico
	Delay_us(10);
	bGPIOA_PDOR5 = 0; //Pin A5 en 0 lógico
}
void TPM0_IRQHandler(){

	if (bTPM0_CH1F==1){
		bTPM0_CH1F=1;
		if((rTPM0_C1V-DatoCapturado) < 0){
			tiempoEvento = 200000 - DatoCapturado + rTPM0_C1V;
		}else{
			tiempoEvento = rTPM0_C1V-DatoCapturado;
		}
		DatoCapturado = rTPM0_C1V;


		switch (si){
			case 0:
				si=1;
				break;
			default:
				Distancia = tiempoEvento*0.017; //distancia en cm
				if(Distancia > 300){
					Distancia -= 7000;
				}
				printf("%d\n",Distancia);
				si=0;
				break;
		}
		/*tiempoEvento = (rTPM0_C1V-DatoCapturado+ (contOver*20000)); //resultado en us
		Distancia= tiempoEvento*0.017; //distancia en cm
		printf(" %d %d %d\n",rTPM0_C1V,DatoCapturado,Distancia);*/
	}
}


//Delays
void Delay_ms(uint32_t ms){
	uint32_t j;
	ms *= 4000;
	for(j = 0;j<=ms;j++);
}

void Delay_us(uint32_t us){
	uint32_t j;
	us *= 4;
	for(j = 0;j<=us;j++);
}

//Magnetometro
#define MAG3110 0x1C
#define CTRL_REG1 0x10
#define CTRL_REG2 0x11
#define X_OFFSET 1006
#define Y_OFFSET -1787
int16_t mx, my;
double angulo_mag;


int readacc(uint8_t direccion){
	int16_t accx;
	uint8_t data;
	int i;
    int error = i2c1_single_byte_read(&data, 0x07);		//Leer direccion 0x0D

	error = i2c1_single_byte_read(&data, direccion);
	if(!error){
		accx = data<<8;
	}
	error = i2c1_single_byte_read(&data, direccion+1);
	if(!error){
		accx |= data;
	}

	for(i=0; i<100000;i++);
	return accx;
}

void Init_I2C_MAG3110(void){
	bSIM_CG_I2C1 = 1;			// Disable I2C clock gating
	bSIM_CG_PORTD = 1;			// Disable PORTD clock gating
	bSIM_CG_PORTC = 1;			// Disable PORTC clock gating

	bPTD7_MUX = kPTD7_MUX_I2C1_SCL;	// PTD7 as SCL signal for I2C1 module
	bPTD6_MUX = kPTD6_MUX_I2C1_SDA; // PTD6 as SDA signal for I2C1 module
	bPTD7_PS = 1;					// Pull up selected for PTD7
	bPTD6_PS = 1;					// Pull up selected for PTD6
	bPTD7_PE = 1;					// PTD7 pull enable required by I2C interface
	bPTD6_PE = 1;					// PTD6 pull enable required by I2C interface
	bPTD7_DSE = 1;					// PTD7 pull enable required by I2C interface
	bPTD6_DSE = 1;					// PTD6 pull enable required by I2C interface

	bPTC2_MUX = kPTC2_MUX_GPIO;		// MMA8451 Interrupt
	bPTC2_IRQC = kPORT_IRQC_INTERRUPT_RISING_EDGE;

	mag3110_init(MAG3110);

	uint8_t data = 0x80;
	//Enable reset: Automatic Magnetic Sensor Reset. reg 2 0x11, bit 11
	int error = i2c1_single_byte_write(data,CTRL_REG2);
	//Leer direccion 0x07 who Am I?
	error = i2c1_single_byte_read(&data, 0x07);

	error=i2c1_single_byte_write(0,0x09);
	error=i2c1_single_byte_write(0,0x0A);
	error=i2c1_single_byte_write(0,0x0B);
	error=i2c1_single_byte_write(0,0x0C);

	// Cambiar modo de operación a activo
	error=i2c1_single_byte_write(0x01,CTRL_REG1);
}

uint8_t m_mag = 90;

void PI_Controller_Mag(uint16_t SetPoint){
	int16_t error_ant = 0;
	int16_t error = 0;

	error =  angulo_mag-SetPoint;
	if((SetPoint >= 0 ) && (SetPoint <= 180)){
		if(angulo_mag > (SetPoint+180)){
			error = -SetPoint + (angulo_mag - 360);
		}
	}else{
		if(angulo_mag < (SetPoint -180)){
			error = -SetPoint + (angulo_mag + 360);
		}
	}

	m_mag = 1.2*error - 0.8*error_ant + m_mag;
	if(m_mag > 180){
		m_mag = 180;
	}
	if(m_mag < 0){
		m_mag = 0;
	}

	error_ant = error;
	//printf("Error: %d", error);

}

void sensado_mag(void){
	mx = readacc(0x01)+X_OFFSET;
	my = readacc(0x03)+Y_OFFSET;

	angulo_mag = (double)(atan((my*0.8)/mx)*180/3.14);
	if((mx <= 0) && (my >= 0)){
		angulo_mag = 180 + angulo_mag;
	}else if((mx < 0) && (my < 0)){
		angulo_mag = angulo_mag + 180;
	}else if((mx > 0) && (my < 0)){
		angulo_mag = 360 + angulo_mag;
	}
}

//////LPUART/////////

uint8_t BufferRX[255];
//uint8_t BufferRX[]="A17,80,G180,A17,50\n";

uint8_t index_escritura = 0;
uint8_t index_lectura = 0;

uint16_t contador_datos = 0;
//uint16_t contador_datos = 19;

uint16_t valor = 0;
uint8_t data_read;


void Init_UART(void){
	//Inicializar LPUART

	bSIM_LPUART1SRC = kSIM_LPUART1SRC_IRC48M;	//Selecciona fuente de reloj
	bSIM_LPUART1RXSRC = 0;		//Selecciona fuente RX
	bSIM_LPUART1TXSRC = 0;		//Selecciona fuente TX
	bSIM_CG_LPUART1 = 1;		//Activa reloj LPUART1

	//Inicializar pines lpuart 1

	bPTE1_MUX = kPTE1_MUX_LPUART1_RX;	//PTE1 es RX pin
	bPTE0_MUX = kPTE0_MUX_LPUART1_TX;	//PTE0 es TX pin

	//Inicicalizar LPUART1 para comunicación 9600 bps, no paridad, 1 bit stop, data 8 bits
	//Baudio_CLK/((OSR+1)*SBR)
	bLPUART1_OSR = 15;
	bLPUART1_SBR = 312;
	bLPUART1_TE = 1;	//Habilita Transmisión
	bLPUART1_RE = 1;	//Habilita Recepción
}

void printLPUART1(){
    while(contador_datos > 0){
    	if(bLPUART1_TDRE == 1){
    		bLPUART1_DATA_8BITS = BufferRX[index_lectura];
    		index_lectura++;
    		contador_datos--;
    	}
    }
}

uint8_t LeerBufferRX(){
	uint8_t dato;
	dato = BufferRX[index_lectura];
	index_lectura++;
	contador_datos--;
	return dato;
}

void Maquina_Secuencia(void){

	static uint8_t estado = 0;
	//static uint8_t estado = 1;

	printf("C = %d --- E= %d \n",contador_datos,estado);
	switch(estado){
	case 0:
		while(1){
			if(bLPUART1_RDRF == 1){
				//printf("Entro\n");
				BufferRX[index_escritura] = bLPUART1_DATA_8BITS;
				index_escritura++;
				contador_datos++;
				if(BufferRX[index_escritura-1] =='\n'){
					estado = 1;
					break;
				}
			}
		}
		break;
	case 1:
		if(contador_datos >= 1){
			switch(LeerBufferRX()){
			case 'A':
				estado = 2;
				break;
			case 'R':
				estado = 5;
				break;
			case 'G':
				estado = 7;
				break;
			}
		}else{
			estado = 0;
		}
		break;
	case 2:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				conteo_hall = (int)(valor/6);
				valor = 0;
				estado = 3;
			}
		}else{
			estado = 0;
		}
		break;
	case 3:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);

			}else{
				Avanzar(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	case 4:
		if(flag_motor == 0){
			Delay_ms(1000);
			estado = 1;
		}
		break;
	case 5:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				conteo_hall = (int)(valor/6);
				valor = 0;
				estado = 6;
			}
		}else{
			estado = 0;
		}
		break;
	case 6:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{

				Retroceder(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	case 7:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				PosServo(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	}
}

uint8_t m = 90;

void PI_Controller(void){
	uint8_t SetPoint = 20;
	int8_t error_ant = 0;
	int8_t error = 0;

	error = SetPoint - Distancia;
	m = 1.7*error + 0.9*error_ant + m;
	if(m > 180){
		m = 180;
	}
	if(m < 0){
		m= 0;
	}
	error_ant = SetPoint - Distancia;

}

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
    PWM_Init_Servo();
    PWM_Init_Motor();
    Input_Capturte_Init();
    NVIC_EnableIRQ(TPM2_IRQn);
    Init_UART();
 //   Init_trigger();
 //   Input_capture_Init_Ultra();
 //   NVIC_EnableIRQ(TPM0_IRQn);
    Init_I2C_MAG3110();
    //NVIC_EnableIRQ(LPUART1_IRQn);
    Delay_ms(2000);

    PRINTF("Hello World\n");
    Avanzar(50);
    conteo_hall = 830;

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
    	/*Trigger_ultra();
    	Delay_ms(50);
    	PI_Controller();
    	PosServo(m);
    	printf("Angulo %d\n",m);
    	Delay_ms(20);*/
    	if((conteo_hall > 460) && (conteo_hall <= 830)){
    	sensado_mag();
    	PI_Controller_Mag(315);
    	PosServo(m_mag);
    	Delay_ms(20);
    	//printf("Angulo_servo: %d Angulo_mag: %d \n",m_mag,(int)(angulo_mag));
    	}else if((conteo_hall <= 460) && (conteo_hall > 410) ){
    		PosServo(115);
    		Delay_ms(20);
    	}else if(conteo_hall <= 420){
        	sensado_mag();
        	PI_Controller_Mag(105);
        	PosServo(m_mag);
        	Delay_ms(20);
    	}else{
    		Parar();
    	}
    	//printf("Conteo: %d\n", conteo_hall);
    	printf("mx: %d, my: %d, angulo: %d \n",mx,my,(int)(angulo_mag));

        i++ ;
    }
    return 0 ;
}

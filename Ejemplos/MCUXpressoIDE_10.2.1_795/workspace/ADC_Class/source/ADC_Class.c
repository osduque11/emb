/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    ADC_Class.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
#include "ADC_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 *
 *
 */
void PIT_Init(void){
	bSIM_CG_PIT = 1; 	//PIT Clock Gate Control Clock enabled

	bPIT_MDIS = 0;		//activa el modulo
	bPIT_FRZ = 0;		//Trabajar en modo debug
	//PIT RUNS AT BUS FRECUENCY 24MHz

	rPIT_LDVAL0 = 240000; //Periodo de la señal, interrupcion cda 100ms

	//bPIT_CH0_TIE = 1; //Activar interrupciones
	//EnableIRQ(PIT_IRQn);//permitir interrupcion al que controla interrupciones
	bPIT_CH0_TEN = 1; //Habilita el timer 0
    /* Enable at the NVIC*/
}

/*void PIT_IRQHandler(void){
	printf("entra\n");
	bPIT_CH0_TIF = 1;
}*/

void ADC_Init(void){

	bSIM_CG_PORTE = 1;		//Port E Clock Gate Control: Clock enabled habilita reloj para el puerto E
	bSIM_CG_ADC0 = 1;		//ADC0 Clock Gate Control Clock enabled
	bSIM_ADC0ALTTRGEN = 1;
	bSIM_ADC0TRGSEL = 4;

	rADC0_SC1A = 0x040; 	//Trabajar el ADC en modo no diferencial // habilita interrupcion // canal 0
	rADC0_SC2 |= 0x69; // habilita trigger por hardware y comparacion en un rango y habilita modo comparacion //VREF interno
	rADC0_CV1 = 78; // rango para 1
	rADC0_CV2 = 194; // rango para 2.5
	EnableIRQ(ADC0_IRQn);

}
void ADC0_IRQHandler(void){
	printf("WARNING fuera del rango\n");
	printf("%u\n",rADC0_RA);
	rADC0_SC1A |= 128;
}

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    PIT_Init();
    ADC_Init();

    PRINTF("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
    }
    return 0 ;
}

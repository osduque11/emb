/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    SPI_DAC.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
#include "ADC_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "PORT_LIB.h"
#include "SPI_LIB.h"
#include "GPIO_LIB.h"
/* TODO: insert other include files here. */

typedef union{
	uint16_t WORD;
	struct{
		uint16_t data	: 12;	// 12 DAC bits
		uint16_t SHDN	: 1;	// Output Shutdown Control bit (Active 1)
		uint16_t GA		: 1;	// Output Gain Selection bit (1 1x, y 0 2x)
		uint16_t 		: 1;	//Not used
		uint16_t AB		: 1;	// DAC Selection bit (1 channel B, 0 for channel A)
	}BITS;

	struct{
		uint8_t DH :8;	// Most significant byte
		uint8_t DL :8;	// Least significant byte
	}Bytes;

}mpc8412_t;

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */

//Vector con señal sinisoidal
uint16_t mySin[] = {2680,3250,3703,3994,4094,3994,3703,3250,2680,2047,1414,844,391,100,0,100,391,844,1414,2047};

volatile uint16_t gADC0Value = 0;
volatile uint8_t gADCFlag = 0;
volatile uint8_t gPITCH1Flag = 0;

void ADC0_IRQHandler(void)
{
	gADC0Value = rADC0_RA; // Interrupt Acknowledge
	gADCFlag = 1;
}

void PIT_IRQHandler(void)
{
	bPIT_CH1_TIF = 1;
	gPITCH1Flag = 1;

}

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    // Init PIT for 100ms time base
    bSIM_CG_PIT = 1; // activar reloj del PIT
    bPIT_MDIS = 0; // Activar PIT
    bPIT_FRZ = 0; // PIT Activo en Debug
    bPIT_CH0_TSV = 4800000; // 100ms time base

    //Init PIT CH1 for 100us time base (Trigger del DAC)
    bPIT_CH1_TIE = 1; 	//Activar interrupciones del PIT
    bPIT_CH1_TSV = 4800; // 100ms time base


    // Init ADC (Leer potenciometro y calcular nueva base de teimpo para el PIT_CH1)
    bSIM_CG_PORTE = 1;
    bPTE21_MUX = kPTE21_ADC0_SE4a;

    bSIM_CG_ADC0 = 1;
    bSIM_ADC0TRGSEL = kSIM_ADC0TRGSEL_PIT_CH0;
    rADC0_SC1A = mADC_SC1n_ADCH(4) | mADC_SC1n_AIEN;
    rADC0_CFG1 = mADC_CFG1_MODE(kADC_MODE_SE_16_BITS) | mADC_CFG1_ADIV(kADC_ADIV_DIVIDE_BY_1)
    		   | mADC_CFG1_ADICLK(kADC_ADICLK_BUS_CLOCK_DIVIDED_BY_2);
    rADC0_CV1 = 49648; // 2.5 voltios
    rADC0_CV1 = 19859; // 1 Voltios
    rADC0_SC2 = mADC_SC2_ADTRG | mADC_SC2_REFSEL(0);

    //Inicializar PIT
    bSIM_CG_PORTC = 1;	//Activar puerto C
    bSIM_CG_SPI0 = 1;	//Activar SPI0
    bPTC4_MUX = kPTC4_MUX_SPI0_PCS0;  	//CS pin 2 del MCP4812
    bPTC5_MUX = kPTC5_MUX_SPI0_SCK;		//SCK pin 3 del MCP4812
    bPTC6_MUX = kPTC6_MUX_SPI0_MOSI;	//SDI pin 4 del MCP4812
    bPTC7_MUX = kPTC6_MUX_GPIO;			//LDA pin 5 del MCP4812
    bGPIOC_PDDR7 = 1; 					//GPIOC_bit7 como salida
    bGPIOC_PSOR7 = 1;					//GPIOC_bit7 inicializado a 1

    bSPI0_SPPR = 0;		//Divide reloj por 1
    bSPI0_SPR = 1;		//Divide reloj por 4
    bSPI0_SPIMODE = 1;	//16bit mode
    bSPI0_MSTR = 1;		//MCU en el maestro



    NVIC_EnableIRQ(ADC0_IRQn);
    bPIT_CH0_TEN = 1;	//Activar PIT canal 0
    bPIT_CH1_TEN = 1;	//Activar PIT canal 1
    bSPI0_SPE = 1;		//Activar SPI0

    PRINTF("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    volatile static int j = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */

	mpc8412_t data;
	data.BITS.AB = 0;
	data.BITS.SHDN = 1;
	data.BITS.GA = 1;

    while(1) {
    	if(gADCFlag == 1)
    	{
    		uint32_t freq = 500 + (500*gADC0Value)/65535;	//Calcular frecuencia
    		uint32_t PIT_CH1 = 48000000/(freq*20);			//Calcular modulo PIT
    		bPIT_CH1_TSV = PIT_CH1;							//Reconfigurar PIT
    		gADCFlag = 0;									//Limpiar bandera ADC
    	}
    	if(gPITCH1Flag)
    	{
    		data.BITS.data = mySin[i];
    		i = (i+1)%20;
    		if(bSPI0_SPTEF)
    		{
    			rSPI0_DH = data.Bytes.DH;
    			rSPI0_DL = data.Bytes.DL;
    			SPTEFCI = 1;
    		}

    		while(!bSPI0_SPTEF);
    		bFGPIOC_PCOR7 = 1;

    		for(j=0; j<10; j++);
    		bFGPIOC_PCOR7 = 1;
    		gPITCH1Flag = 0;

    	}
    }
    return 0 ;
}

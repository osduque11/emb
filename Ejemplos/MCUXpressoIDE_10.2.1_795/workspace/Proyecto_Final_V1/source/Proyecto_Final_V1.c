/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Proyecto_Final_V1.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */
#include "GPIO_LIB.h"
#include "PORT_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "TPM_LIB.h"
#include "LPUART_LIB.h"

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
//Variable globales sensor Hall
uint8_t contOver=0;
uint16_t DatoCapturado=0;
uint16_t tiempoEvento = 0;
uint16_t conteo_hall = 0;

//Variable globales Mosuse
int16_t MouseX = 0;
int16_t MouseY = 0;
uint8_t MouseStat;
uint8_t flag_avanzar = 0;

//Inicializacion PWM 50Hz
void PWM_Init_Servo(void){

	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK
	bSIM_CG_TPM0 = 1;	//Habilita CLK al módulo TPM0
	bSIM_CG_PORTE = 1;	// Habilita reloj al módulo Port E.
	bPTE24_MUX = 3;		//PORTE24 (pin 20) es configurado como TPM0_CH0, ALT3

	bTPM0_PS = 3;		// Preescaladro, divide por 8
	bTPM0_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM0_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM0->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida
	//bTPM0_CH0_MS = 1;

	//bTPM0_CH0_ELS = 1;

	rTPM0_MOD = 20000;
	rTPM0_C0V = 1500;
}

void PosServo(int angulo){
	rTPM0_C0V = 500 + 11*angulo;
}

void PWM_Init_Motor(void){
	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK
	bSIM_CG_TPM1 = 1;	//Habilita CLK al módulo TPM0
	bSIM_CG_PORTA = 1;	// Habilita reloj al módulo Port A. A4 //E25
	bPTE20_MUX = 1;		//PORTE25 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTA12_MUX = 1;		//PORTA4 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	bGPIOA_PDDR12 = 1;	//PORTA4 es configurado como puerto out
	bGPIOE_PDDR20 = 1;	//PORTE25 es configurado como puerto out

	bGPIOA_PDOR12 = 0;	//PORTA4 es llevado a un nivel logico 0
	bGPIOE_PDOR20 = 0;	//PORTE25 es llevado a un nivel logico 0

	bTPM1_PS = 3;		// Preescaladro, divide por 8 trabajando con un reloj de 1MHz
	bTPM1_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM1_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM1->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida
	//bTPM0_CH0_MS = 1;
	//bTPM0_CH0_ELS = 1;
	rTPM1_MOD = 5000;
	rTPM1_C0V = 2500;
}

void Avanzar(uint8_t Vel){ //ingresar velocidad de 0 a 100.
	bPTA12_MUX = 3;		//PORTA12 es configurado como PWM
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	if (Vel>=0 && Vel<=100){
		//printf("Avanzando...");
		flag_avanzar = 1;
		rTPM1_C0V = Vel*50;}
	else
		rTPM1_C0V=0;
}

void Retroceder(uint8_t Vel){ //ingresar velocidad de 0 a 100.

	bPTA12_MUX = 1;		//PORTA4 es configurado como PWM
	bPTE20_MUX = 3;		//PORTE25 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	if (Vel>=0 && Vel<=100){
		//printf("Retrocediendo...");
		rTPM1_C0V = Vel*50;
		flag_avanzar = 1;
	}
	else
		rTPM1_C0V=0;

}

void Parar(){
	bPTA12_MUX = 1;		//PORTA4 es configurado como PWM
	bPTE20_MUX = 1;		//PORTE25 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	flag_avanzar = 0;
}

void Input_Capturte_Init(){
	bSIM_CG_TPM2 = 1;	//Habilita CLK al módulo TPM2
	bPTA1_MUX = 3;		//PORTA1 es configurado timer
	bTPM2_PS = 7;		// Preescaladro, divide por 128 trabajando con un reloj de 62.5KHz
	bTPM2_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.
	bTPM2_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM2->CONTROLS[0].CnSC |= TPM_CnSC_ELSB_MASK;
	//bTPM0_CH0_MS = 1;
	//bTPM0_CH0_ELS = 1;
	rTPM2_MOD = 62500; // 1 overflow cada segundo
	bTPM2_CH0_CHIE=1;  //Habilita interrupciones del canal
	bTPM2_TOIE=1;		//Habilta iterrupcion por overflow
}

void TPM2_IRQHandler(){
	if (bTPM2_CH0F==1){
		bTPM2_CH0F=1;
		conteo_hall--;
		if(conteo_hall == 0){
			Parar();
		}
		printf("%d\n",conteo_hall);
		//printf("inp capt\n");
		//tiempoEvento = (rTPM2_C0V-DatoCapturado+ (contOver*62500))/62.5; //resultado en ms
		//DatoCapturado = rTPM2_C0V;
		//printf("%d\n",tiempoEvento);
		contOver=0;
	}
	if (bTPM2_TOF_ST==1){
		bTPM2_TOF_SC=1;
		//printf("over\n");
		contOver++;
	}
}

void Delay_ms(uint32_t ms){
	uint32_t j;
	ms *= 4000;
	for(j = 0;j<=ms;j++);
}

void Delay_us(uint32_t us){
	uint32_t j;
	us *= 4;
	for(j = 0;j<=us;j++);
}

//Driver PS2
#define CLK 0
#define DATAS 1

void PS2GoHi(uint8_t pin){
	if(pin == CLK){
		bGPIOA_PDDR5 = 0; //Pin A13 CLK como entrada
		bGPIOA_PDOR5 = 1; //Pin A13 CLK en 1 lógico
	}else{
		bGPIOE_PDDR25 = 0;	//Pin E25 DATA como entrada
		bGPIOE_PDOR25 = 1;	//Pin E25 en 1 lógico
	}
}

void PS2GoLo(uint8_t pin){
	if(pin == CLK){
		bGPIOA_PDDR5 = 1; //Pin A13 CLK como entrada
		bGPIOA_PDOR5 = 0; //Pin A13 CLK en 1 lógico
	}else{
		bGPIOE_PDDR25 = 1;	//Pin E25 DATA como entrada
		bGPIOE_PDOR25 = 0;	//Pin E25 en 1 lógico
	}
}


void PS2Write(uint8_t data){
	uint8_t parity = 1;

	//Enviar start

	PS2GoHi(DATAS);
	PS2GoHi(CLK);
	Delay_us(300);
	PS2GoLo(CLK);
	Delay_us(300);
	PS2GoLo(DATAS);
	Delay_us(10);
	PS2GoHi(CLK);

	while(bGPIOA_PDIR5 == 1);

	//Enviar datos

	for(uint8_t i=0;i<8;i++){
		if(data&0x01) PS2GoHi(DATAS);
		else PS2GoLo(DATAS);
		while(bGPIOA_PDIR5 == 0);
		while(bGPIOA_PDIR5 == 1);
	    parity^=(data&0x01);
	    data=data>>1;
	}

	//Envia paridad
	if(parity) PS2GoHi(DATAS);
	else PS2GoLo(DATAS);

	//Enviar stop
	while(bGPIOA_PDIR5 == 0);
	while(bGPIOA_PDIR5 == 1);

	PS2GoHi(DATAS);
	Delay_us(50);

	while(bGPIOA_PDIR5 == 1);
	while(bGPIOA_PDIR5 == 0 || bGPIOE_PDIR25 == 0);

	PS2GoLo(CLK);
}

uint8_t PS2Read(){
	uint8_t data = 0, bit = 1;

	PS2GoHi(CLK);
	PS2GoHi(DATAS);
	Delay_us(50);

	while(bGPIOA_PDIR5 == 1);
	Delay_us(5);
	while(bGPIOA_PDIR5 == 0);

	for(uint8_t i=0; i<8; i++){
		while(bGPIOA_PDIR5 == 1);
		if(bGPIOE_PDIR25 == 1) data|=bit;
		while(bGPIOA_PDIR5 == 0);
		bit=bit<<1;
	}

	while(bGPIOA_PDIR5 == 1);
	while(bGPIOA_PDIR5 == 0);
	while(bGPIOA_PDIR5 == 1);
	while(bGPIOA_PDIR5 == 0);

	PS2GoLo(CLK);
	return data;
}

void Init_PS2(void){
	bPTE25_MUX = 1;  //Pin E25 como I/O
	bPTA5_MUX =1; 	//Pin A13 como I/O

	PS2GoHi(CLK);
	PS2GoHi(DATAS);

	PS2Write(0xFF); //Reset PS2

	for(uint8_t i=0; i<3; i++) PS2Read(); //Leer tres tramas. Responde con ACK, autoTEST, ID
	PS2Write(0xF0); 	//Modo remoto
	PS2Read(); 			//Responde con ACK
	Delay_us(100);

	printf("Mouse Ready\n");

}

void PS2MousePos(void){
  PS2Write(0xEB); //Pedir posición
  PS2Read();
  MouseStat = PS2Read();
  MouseX = PS2Read();
  MouseY = PS2Read();
  if(MouseStat&0b00100000){
	  MouseY = MouseY-256;
  }
  if(MouseStat&0b00010000){
	  MouseX = MouseX-256;

  }
}

void initpinE1(void){
	bPTE1_MUX = 1;
	bGPIOE_PDDR1 = 0;
//	bPTE1_IRQC = 10;	//configura Interrupcion flanco de bajada
//	NVIC_EnableIRQ(PORTB_PORTC_PORTD_PORTE_IRQn);
}

void Init_UART(void){
	//Inicializar LPUART

	bSIM_LPUART1SRC = kSIM_LPUART1SRC_IRC48M;	//Selecciona fuente de reloj
	bSIM_LPUART1RXSRC = 0;		//Selecciona fuente RX
	bSIM_LPUART1TXSRC = 0;		//Selecciona fuente TX
	bSIM_CG_LPUART1 = 1;		//Activa reloj LPUART1

	//Inicializar pines lpuart 1

	bPTE1_MUX = kPTE1_MUX_LPUART1_RX;	//PTA1 es RX pin
	bPTE0_MUX = kPTE0_MUX_LPUART1_TX;	//PTA2 es TX pin

	//Inicicalizar LPUART0 para comunicación 115200 bps, no paridad, 1 bit stop, data 8 bits
	//Baudio_CLK/((OSR+1)*SBR)
	bLPUART1_OSR = 15;
	bLPUART1_SBR = 312;
	bLPUART1_TE = 1;	//Habilita Transmisión
	bLPUART1_RE = 1;	//Habilita Recepción
}

void printLPUART1(char * str){
    uint8_t i = 0;
    while(str[i] != '\0'){
    	if(bLPUART1_TDRE == 1){
    		bLPUART1_DATA_8BITS = str[i];
    		i++;
    	}
    }
}

char datos[255];
uint8_t index_escritura = 0;
uint8_t index_lectura = 0;
uint8_t contador_datos = 0;

uint16_t LeerEntero(void){
	uint16_t valor = 0;
	while(datos[index_lectura]>=48 && datos[index_lectura]<=57){
		valor = valor*10 + (datos[index_lectura++]-48);
		contador_datos--;
	}
	index_lectura++;
	contador_datos--;
	return valor;
}

int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    PRINTF("Hello World\n");
    PWM_Init_Servo();
    PWM_Init_Motor();
    Input_Capturte_Init();
    Init_UART();
    //Init_PS2();
    NVIC_EnableIRQ(TPM2_IRQn);
    //initpinE1();
    Delay_ms(2000);
    /* Force  the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
    	while(1){
			if(bLPUART1_RDRF == 1){
				datos[index_escritura] = bLPUART1_DATA_8BITS;
				if(datos[index_escritura] =='\n'){
					index_escritura++;
					contador_datos++;
					break;
				}
				index_escritura++;
				contador_datos++;
    		}
    	}
    	while(contador_datos >= 1){
    		printf("Contadordatos = %d\n",contador_datos);
    		switch(datos[index_lectura++]){
    		case 'A':
    			contador_datos--;
    			conteo_hall = LeerEntero();
    			Avanzar(LeerEntero());
    			while(flag_avanzar);
    			Delay_ms(1000);
    			break;
    		case 'R':
    			contador_datos--;
    			conteo_hall = LeerEntero();
    			Retroceder(LeerEntero());
    			while(flag_avanzar);
    			Delay_ms(1000);
    			break;
    		case 'G':
    			contador_datos--;
    			PosServo(LeerEntero());
    			Delay_ms(1000);
    			break;
    		default:
    			contador_datos--;
    			break;


    		}
    	}
        i++ ;

    }
    return 0 ;
}

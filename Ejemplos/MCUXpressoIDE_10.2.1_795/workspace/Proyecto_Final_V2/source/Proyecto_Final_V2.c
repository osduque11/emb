/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Proyecto_Final_V2.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */
#include "GPIO_LIB.h"
#include "PORT_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "TPM_LIB.h"
#include "LPUART_LIB.h"
/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */


uint16_t conteo_hall = 0;		//Decrementa cada 6cm recorridos

uint8_t flag_motor = 0;			// 1->Motor encendido 0->Motor apagado
uint8_t flag_dir_Motor=0;       // 0->Adelante 1->Atras

void PWM_Init_Servo(void){

	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK (8MHz)
	bSIM_CG_TPM0 = 1;	//Habilita CLK al módulo TPM0
	bSIM_CG_PORTE = 1;	// Habilita reloj al módulo Port E.
	bPTE24_MUX = 3;		//PORTE24 es configurado como TPM0_CH0, ALT3

	bTPM0_PS = 3;		// Preescaladro, divide por 8
	bTPM0_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM0_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM0->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida

	rTPM0_MOD = 20000;
	rTPM0_C0V = 1500;
}

void PosServo(uint8_t angulo){
	rTPM0_C0V = 500 + 11*angulo;
}

void PWM_Init_Motor(void){
	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK
	bSIM_CG_TPM1 = 1;	//Habilita CLK al módulo TPM1
	bSIM_CG_PORTA = 1;	// Habilita reloj al módulo Port A.
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	bGPIOA_PDDR12 = 1;	//PORTA12 es configurado como puerto out
	bGPIOE_PDDR20 = 1;	//PORTE20 es configurado como puerto out

	bGPIOA_PDOR12 = 0;	//PORTA12 es llevado a un nivel logico 0
	bGPIOE_PDOR20 = 0;	//PORTE20 es llevado a un nivel logico 0

	bTPM1_PS = 3;		// Preescaladro, divide por 8 trabajando con un reloj de 1MHz
	bTPM1_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM1_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM1->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida
	rTPM1_MOD = 5000;	//Frecuencia de la señal de 200Hz
	rTPM1_C0V = 2500;
}

void Avanzar(uint8_t Vel){ //ingresar velocidad de 0 a 100.
	bPTA12_MUX = 3;		//PORTA12 es configurado como PWM
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato

	if (Vel>=0 && Vel<=100){
		flag_motor = 1;
		flag_dir_Motor=0;
		rTPM1_C0V = Vel*50;}
	else
		rTPM1_C0V=0;
}

void Retroceder(uint8_t Vel){ //ingresar velocidad de 0 a 100.

	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTE20_MUX = 3;		//PORTE20 es configurado como PWM

	if (Vel>=0 && Vel<=100){
		rTPM1_C0V = Vel*50;
		flag_motor = 1;
		flag_dir_Motor=1;
	}
	else
		rTPM1_C0V=0;

}

void Parar(){
	bPTA12_MUX = 1;		//PORTA12 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	bPTE20_MUX = 1;		//PORTE20 es configurado como puerto in/out, ALT1 //PWM deshabilidato
	flag_motor = 0;
}

void Input_Capturte_Init(){
	bSIM_CG_TPM2 = 1;	//Habilita CLK al módulo TPM2
	bPTA1_MUX = 3;		//PORTA1 es configurado timer
	bTPM2_PS = 7;		// Preescaladro, divide por 128 trabajando con un reloj de 62.5KHz
	bTPM2_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.
	bTPM2_DBGMODE = 3;
	TPM2->CONTROLS[0].CnSC |= TPM_CnSC_ELSB_MASK;

	rTPM2_MOD = 62500; // 1 overflow cada segundo
	bTPM2_CH0_CHIE = 1;  //Habilita interrupciones del canal
}

void TPM2_IRQHandler(){
	if (bTPM2_CH0F==1){
		bTPM2_CH0F=1;
		conteo_hall--;
		printf("Conteo %d\n",conteo_hall);
		if(flag_dir_Motor==0){ //Carro avanzando
			if(conteo_hall == 1){
				Retroceder(30);
					}
			if(conteo_hall == 0){
				Parar();
			}
		}
		else{					//carro retrocediendo
			if(conteo_hall == 1){
				Avanzar(30);
								}
			if(conteo_hall == 0){
				Parar();
			}
		}
	}
}

void Delay_ms(uint32_t ms){
	uint32_t j;
	ms *= 4000;
	for(j = 0;j<=ms;j++);
}

void Delay_us(uint32_t us){
	uint32_t j;
	us *= 4;
	for(j = 0;j<=us;j++);
}



//////LPUART/////////

uint8_t BufferRX[255];
//uint8_t BufferRX[]="A17,80,G180,A17,50\n";

uint8_t index_escritura = 0;
uint8_t index_lectura = 0;

uint16_t contador_datos = 0;
//uint16_t contador_datos = 19;

uint16_t valor = 0;
uint8_t data_read;


void Init_UART(void){
	//Inicializar LPUART

	bSIM_LPUART1SRC = kSIM_LPUART1SRC_IRC48M;	//Selecciona fuente de reloj
	bSIM_LPUART1RXSRC = 0;		//Selecciona fuente RX
	bSIM_LPUART1TXSRC = 0;		//Selecciona fuente TX
	bSIM_CG_LPUART1 = 1;		//Activa reloj LPUART1

	//Inicializar pines lpuart 1

	bPTE1_MUX = kPTE1_MUX_LPUART1_RX;	//PTE1 es RX pin
	bPTE0_MUX = kPTE0_MUX_LPUART1_TX;	//PTE0 es TX pin

	//Inicicalizar LPUART1 para comunicación 9600 bps, no paridad, 1 bit stop, data 8 bits
	//Baudio_CLK/((OSR+1)*SBR)
	bLPUART1_OSR = 15;
	bLPUART1_SBR = 312;
	bLPUART1_TE = 1;	//Habilita Transmisión
	bLPUART1_RE = 1;	//Habilita Recepción
}

void printLPUART1(){
    while(contador_datos > 0){
    	if(bLPUART1_TDRE == 1){
    		bLPUART1_DATA_8BITS = BufferRX[index_lectura];
    		index_lectura++;
    		contador_datos--;
    	}
    }
}

uint8_t LeerBufferRX(){
	uint8_t dato;
	dato = BufferRX[index_lectura];
	index_lectura++;
	contador_datos--;
	return dato;
}

void Maquina_Secuencia(void){

	static uint8_t estado = 0;
	//static uint8_t estado = 1;

	printf("C = %d --- E= %d \n",contador_datos,estado);
	switch(estado){
	case 0:
		while(1){
			if(bLPUART1_RDRF == 1){
				//printf("Entro\n");
				BufferRX[index_escritura] = bLPUART1_DATA_8BITS;
				index_escritura++;
				contador_datos++;
				if(BufferRX[index_escritura-1] =='\n'){
					estado = 1;
					break;
				}
			}
		}
		break;
	case 1:
		if(contador_datos >= 1){
			switch(LeerBufferRX()){
			case 'A':
				estado = 2;
				break;
			case 'R':
				estado = 5;
				break;
			case 'G':
				estado = 7;
				break;
			}
		}else{
			estado = 0;
		}
		break;
	case 2:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				conteo_hall = (int)(valor/6);
				valor = 0;
				estado = 3;
			}
		}else{
			estado = 0;
		}
		break;
	case 3:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);

			}else{
				Avanzar(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	case 4:
		if(flag_motor == 0){
			Delay_ms(1000);
			estado = 1;
		}
		break;
	case 5:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				conteo_hall = (int)(valor/6);
				valor = 0;
				estado = 6;
			}
		}else{
			estado = 0;
		}
		break;
	case 6:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{

				Retroceder(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	case 7:
		if(contador_datos >=1 ){
			data_read = LeerBufferRX();
			if(data_read >=48 && data_read <= 57){
				valor = valor*10 + (data_read-48);
			}else{
				PosServo(valor);
				valor = 0;
				estado = 4;
			}
		}else{
			estado = 0;
		}
		break;
	}
}

int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
    PWM_Init_Servo();
    PWM_Init_Motor();
    Input_Capturte_Init();
    NVIC_EnableIRQ(TPM2_IRQn);
    Init_UART();
    //NVIC_EnableIRQ(LPUART1_IRQn);
    Delay_ms(2000);
    conteo_hall = 50;
    PRINTF("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
    	//Maquina_Secuencia();
        i++ ;
    }
    return 0 ;
}

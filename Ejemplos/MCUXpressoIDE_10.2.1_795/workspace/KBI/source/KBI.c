/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
/**
 * @file    KBI.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"

/* TODO: insert other include files here. */

#include "GPIO_LIB.h"
#include "PORT_LIB.h"
#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "TPM_LIB.h"
volatile uint8_t gKey;
uint8_t estado = 0;
uint8_t sentido = 1; //Sentido 1-> Horario 0->antihorario
uint8_t digito1 = 0;
uint8_t digito2 = 0;


//Inicializacion PWM 50Hz
void PWM_Init(void){

	bSIM_TPMSRC = 3;	// Selecciona la fuente de reloj MCGIRCLK
	bSIM_CG_TPM0 = 1;	//Habilita CLK al módulo TPM0
	bSIM_CG_PORTE = 1;	// Habilita reloj al módulo Port E.
	bPTE24_MUX = 3;		//PORTE24 (pin 20) es configurado como TPM0_CH0, ALT3

	bTPM0_PS = 3;		// Preescaladro, divide por 8
	bTPM0_CMOD = 1;		//TPM incrementa el contador en cada contador de TPM.

	bTPM0_DBGMODE = 3;

	//PWM alineado al borde flanco de subida
	TPM0->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK; //PWM alineado al borde flanco de subida
	//bTPM0_CH0_MS = 1;

	//bTPM0_CH0_ELS = 1;

	rTPM0_MOD = 20000;
	rTPM0_C0V = 2500;
}



/* TODO: insert other definitions and declarations here. */

void PORTB_PORTC_PORTD_PORTE_IRQHandler(void){

	volatile uint8_t columna = ~(rFGPIOD_PDIR) & 0x0F; // Leer columna
	volatile uint8_t fila;


	// setup filas como entrada
	bPTD0_IRQC = kPORT_IRQC_DISABLED;
	bPTD1_IRQC = kPORT_IRQC_DISABLED;
	bPTD2_IRQC = kPORT_IRQC_DISABLED;
	bPTD3_IRQC = kPORT_IRQC_DISABLED;
	rGPIOD_PDDR = rGPIOD_PDDR & (~0x00000030); // Configura los bits 4 y 5 del PORTD como entradas
	rGPIOB_PDDR = rGPIOB_PDDR & (~0x0000000C); // Configura los bits 2 y 3 del PORTB como entradas
	rGPIOD_PDDR = rGPIOD_PDDR | 0x0000000F; // Configura los 3 a 0 del PORTD como salidas
	rGPIOD_PCOR = 0x0F; // Pone en 0 los bits 3 a 0 del PORTD
	for(uint8_t i=0; i<2; i++)
	{}
	fila = ((((~rGPIOD_PDIR) & 0x30)>>4)|(((~rGPIOB_PDIR) & 0x08)>>1)|(((~rGPIOB_PDIR) & 0x04)<<1));
	//fila2 = rGPIOB_PDIR;

	gKey = (fila<<4) | columna;

	// reestablecer puertos filas como salidas y columnas como entradas
	rGPIOD_PDDR = rGPIOD_PDDR & (~0x0F); // Configura los bits 3 a 0 del PORTD como entradas
	rGPIOD_PDDR = rGPIOD_PDDR | 0x30; // Configura los bits 4 y 5 del PORTD como salidas
	rGPIOB_PDDR = rGPIOB_PDDR | 0x0C; // Configura los bits 2 y 3 del PORTB como salidas
	rGPIOD_PCOR = 0x30; // Pone en 0 los bits 4 y 5
	rGPIOB_PCOR = 0x0C;

	bPIT_CH0_TEN = 1;


	//printf("%x-%x\n",fila,columna); // imprimir columna
	rPTD_ISFR = 0x0F; // reconocimiento de interrupción

}

void PIT_IRQHandler(void){
	bPIT_CH0_TIF = 1;

	if((rGPIOD_PDIR & 0x0F) == 0x0F){
		bPTD0_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
		bPTD1_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
		bPTD2_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
		bPTD3_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;

		bPIT_CH0_TEN = 0;
	}
}

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();


    // Inicialización PWM//
    PWM_Init();

    /*Incialización de los puertos*/

    bSIM_CG_PORTD = 1;
    bSIM_CG_PORTB = 1;

    // Para las Columnas
    bPTD0_PE = 1; // Activa Pull up/down
    bPTD0_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD0_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
    bPTD0_MUX = kPTD0_MUX_GPIO;
    bPTD1_PE = 1; // Activa Pull up/down
    bPTD1_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD1_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
    bPTD1_MUX = kPTD1_MUX_GPIO;
    bPTD2_PE = 1; // Activa Pull up/down
    bPTD2_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD2_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
    bPTD2_MUX = kPTD2_MUX_GPIO;
    bPTD3_PE = 1; // Activa Pull up/down
    bPTD3_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD3_IRQC = kPORT_IRQC_INTERRUPT_FALLING_EDGE;
    bPTD3_MUX = kPTD3_MUX_GPIO;

    // Para las Filas
    bPTD4_PE = 1; // Activa Pull up/down
    bPTD4_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD4_IRQC = kPORT_IRQC_DISABLED;
    bPTD4_MUX = kPTD4_MUX_GPIO;
    bPTD5_PE = 1; // Activa Pull up/down
    bPTD5_PS = 1; // Selecciona si es up (1) o down (0)
    bPTD5_IRQC = kPORT_IRQC_DISABLED;
    bPTD5_MUX = kPTD5_MUX_GPIO;
    bPTB3_PE = 1; // Activa Pull up/down
    bPTB3_PS = 1; // Selecciona si es up (1) o down (0)
    bPTB3_IRQC = kPORT_IRQC_DISABLED;
    bPTB3_MUX = kPTB3_MUX_GPIO;
    bPTB2_PE = 1; // Activa Pull up/down
    bPTB2_PS = 1; // Selecciona si es up (1) o down (0)
    bPTB2_IRQC = kPORT_IRQC_DISABLED;
    bPTB2_MUX = kPTB2_MUX_GPIO;
    rGPIOD_PDDR = rGPIOD_PDDR | 0x30; // Configura los bits 4 y 5 del PORTD como salidas
    rGPIOB_PDDR = rGPIOB_PDDR | 0x0C; // Configura los bits 2 y 3 del PORTB como salidas
    rGPIOD_PCOR = 0x30; // Pone en 0 los bits 4 y 5
    rGPIOB_PCOR = 0x0C;



    rPTD_ISFR = 0x0F;

    ////INICIALIZACION PIT para antirrebote //////////

    bSIM_CG_PIT = 1; // activa reloj del PIT
    bPIT_MDIS = 0; // habilita PIT
    bPIT_FRZ = 0; // PIT activo en debug

    bPIT_CH0_TIE = 1;
    bPIT_CH0_TIF = 1;
    bPIT_CH0_TSV = 2400000;

    NVIC_EnableIRQ(PORTB_PORTC_PORTD_PORTE_IRQn);
    NVIC_EnableIRQ(PIT_IRQn);


    printf("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
        if(gKey != 0){
        	uint8_t realKey;
        	switch(gKey){
        	case 0x11:
        		realKey = 1; // 1
        		break;
        	case 0x12:
        		//realKey = 2; // 2
        		realKey = 4; // 4
        		break;
        	case 0x14:
        		//realKey = 3; // 3
        		realKey = 7; // 7
        		break;
        	case 0x18:
        		//realKey = 10; //A
        		realKey = 14; //*
        		break;

        	case 0x21:
        		//realKey = 4; //4
        		realKey = 2; //2
        		break;
        	case 0x22:
        		realKey = 5; //5
        		break;
        	case 0x24:
        		//realKey = 6; //6
        		realKey = 8; //8
        		break;
        	case 0x28:
        		//realKey = 11; //B
        		realKey = 0; //0
        		break;

        	case 0x41:
        		//realKey = 7; //7
        		realKey = 3; //3
        		break;
        	case 0x42:
        		//realKey = 8; //8
        		realKey = 6; //6
        		break;
        	case 0x44:
        		realKey = 9; //9
        		break;
        	case 0x48:
        		//realKey = 12; //C
        		realKey = 15; //#
        		break;

        	case 0x81:
        		//realKey = 14; //*
        		realKey = 10; //A
        		break;
        	case 0x82:
        		//realKey = 0; //0
        		realKey = 11; //B
        		break;
        	case 0x84:
        		//realKey = 15; //#
        		realKey = 12; //C
        		break;
        	case 0x88:
        		realKey = 13; //D
        		break;
        	default:
        		realKey = 0;
        	}

        	printf("%d\n",realKey);
        	//printf("%d\n",rTPM0_C0V);

        	switch(estado){
        	case 0:

        		if(realKey == 14){
        			rTPM0_C0V = 1500;
        		}
        		if(realKey == 10){
        			sentido = 1;
        		}
        		if(realKey == 11){
        			sentido = 0;
        		}
        		if(realKey < 10){
        			digito1 = realKey;
        			estado = 1;
        		}
        		break;
        	case 1:
        		if(realKey == 15){
        			if(sentido == 1){
        				rTPM0_C0V = 1500 - 11*digito1;
        			}
        			if(sentido == 0){
        				rTPM0_C0V = 1500 + 11*digito1;
        			}
        			estado = 0;
        		}
        		if(realKey < 10){
        			digito2 = realKey;
        			estado = 2;
        		}
        		break;
        	case 2:
        		if(realKey == 15){
        			if(digito1*10+digito2 <=90)
        			{
						if(sentido == 1){
							rTPM0_C0V = 1500 - 11*(digito1*10+digito2);
						}
						if(sentido == 0){
							rTPM0_C0V = 1500 + 11*(digito1*10+digito2);
						}
        			}
        			estado = 0;
        		}
        		break;
        	}
        	gKey = 0;
        }
    }
    return 0 ;

}

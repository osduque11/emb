/*
 * Copyright (c) 2017, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    ADC_test.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
/* TODO: insert other include files here. */

#include "SIM_LIB.h"
#include "PIT_LIB.h"
#include "ADC_LIB.h"
#include "PORT_LIB.h"

/* TODO: insert other definitions and declarations here. */

volatile uint16_t gADC0Value = 0;
volatile uint8_t gADCFlag = 0;

void ADC0_IRQHandler(void)
{
	gADC0Value = rADC0_RA; // Interrupt Acknowledge
	gADCFlag = 1;
}

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    printf("Ejemplo Comparador - Fuera de rango inclusive\n");

    // Init PIT for 100ms time base

    bSIM_CG_PIT = 1; 									// activar reloj del PIT
    bPIT_MDIS = 0; 										// Activar PIT
    bPIT_FRZ = 0; 										// PIT Activo en Debug
    bPIT_CH0_TSV = 4800000; 							// 100ms time base

    // Init ADC for compare
    bSIM_CG_PORTE = 1;									// Activar reloj del puerto E
    bPTE21_MUX = kPTE21_ADC0_SE4a;  					// Configura PTE21 como entrada analoga del ADC0

    bSIM_CG_ADC0 = 1;									// Activar reloj del ADC
    bSIM_ADC0TRGSEL = kSIM_ADC0TRGSEL_PIT_CH0;			// Configura el CH0 del PIT como trigger del ADC
    bSIM_ADC0ALTTRGEN = 1;								// Configura el tipo de HW trigger para el ADC
    rADC0_SC1A = mADC_SC1n_ADCH(kADC_ADCH_SE4a) 		// Configura el canal de ADC
    		   | mADC_SC1n_AIEN;						// Activa interrupciones de ADC
    rADC0_CFG1 = mADC_CFG1_MODE(kADC_MODE_SE_16_BITS)   // Configura conversion de 16 bits
    		   | mADC_CFG1_ADIV(kADC_ADIV_DIVIDE_BY_1)  // Prescalador del reloj de ADC a 1
    		   | mADC_CFG1_ADICLK(kADC_ADICLK_BUS_CLOCK_DIVIDED_BY_2); // ADC usa el reloj del BUS divido entre 2
    rADC0_CV1 = 49648; 									// Rango superior, aprox 2.5 voltios
    rADC0_CV2 = 19859; 									// Rango inferior, aprox 1 Voltio
    rADC0_SC2 = mADC_SC2_ACFE 							// Activar modo comparación
    		  | mADC_SC2_ACFGT 							// Configurar comparacion outside rango cuando CV1 es mayor que CV2
			  | mADC_SC2_ACREN 							// Configura comparación de rango
			  | mADC_SC2_ADTRG 							// Configura inicio de conversion por medio de HW trigger
			  | mADC_SC2_REFSEL(0);						// Selecciona VREFL y VREFH como voltajes de referencia para el ADC

    NVIC_EnableIRQ(ADC0_IRQn);							// Activar interrupcions de ADC
    bPIT_CH0_TEN = 1;									// Activa conteo del PIT CH0

    /* Force the counter to be placed into memory. */
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
    	if (gADCFlag == 1)
    	{
    		if(gADC0Value>49648){
    			printf("Warning!!! Signal out of range (up) %d\n",gADC0Value);
    		}
    		else if (gADC0Value<19859)
    		{
    			printf("Warning!!! Signal out of range (down) %d\n",gADC0Value);
    		}
    		else
    		{
    			printf("OMG!!! this program does not work!!!\n");
    		}
    		gADCFlag = 0;
    	}
    }
    return 0 ;
}

/*
 * i2c_isr_lib.h
 *
 *  Created on: 12 oct. 2018
 *      Author: KATHE
 */

#ifndef I2C_ISR_LIB_H_
#define I2C_ISR_LIB_H_

#include <stdint.h>
#include "fifoLib.h"

typedef struct{
	fifo_t writeDataFIFO;
	fifo_t writeAddFIFO;
	fifo_t readDataFIFO;
	fifo_t readAddFIFO;
}i2c_data_t;

volatile i2c_data_t i2c0_data;

int i2c0Init();
int i2c0SingleByteWrite(uint8_t data, uint8_t address);
int i2c0SingleByteRead(uint8_t *data, uint8_t address);
int i2c0MultiByteWrite(uint8_t *data, uint8_t address, uint16_t num);
int i2c0MultiByteRead(uint8_t *data, uint8_t address, uint16_t num);

#endif /* I2C_ISR_LIB_H_ */

/*
 * fifoLib.h
 *
 *  Created on: 12 oct. 2018
 *      Author: KATHE
 */

#ifndef FIFOLIB_H_
#define FIFOLIB_H_

#include <stdint.h>

typedef struct{
	uint8_t *fifo;		//Data array
	uint16_t putIdx;	//First available position
	uint16_t getIdx;	//Frist data
	uint16_t size;
}fifo_t;

int fifoInit(fifo_t* FIFO, uint16_t size);
int fifoPush(fifo_t* FIFO, uint8_t data);
int fifoPull(fifo_t* FIFO, uint8_t *data);
uint16_t fifoGetAvailable(fifo_t* FIFO);
void fifoUnInit(fifo_t* FIFO);
void fifoClean(fifo_t* FIFO);

#endif /* FIFOLIB_H_ */

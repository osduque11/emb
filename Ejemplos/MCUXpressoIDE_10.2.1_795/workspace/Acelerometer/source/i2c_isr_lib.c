/*
 * i2c_isr_lib.c
 *
 *  Created on: 12 oct. 2018
 *      Author: KATHE
 */

#include <stdint.h>
#include "fifoLib.h"
#include "i2c_isr_lib.h"

int i2c0Init(){
	//Initialize I2C0 module
	//Initialize Data structures for ISR handle
	int error = fifoInit(&(i2c0_data.readAddFIFO),10);
	error += fifoInit(&(i2c0_data.writeAddFIFO),10);
	error += fifoInit(&(i2c0_data.readDataFIFO),50);
	error += fifoInit(&(i2c0_data.writeDataFIFO),50);
	return error;

}

int i2c0SingleByteWrite(uint8_t data, uint8_t address){
	int error = fifoPush(&i2c0_data.writeAddFIFO,10);


}
int i2c0SingleByteRead(uint8_t *data, uint8_t address){

}
int i2c0MultiByteWrite(uint8_t *data, uint8_t address, uint16_t num){

}
int i2c0MultiByteRead(uint8_t *data, uint8_t address, uint16_t num){

}


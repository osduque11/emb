/*
 * fifoLib.c
 *
 *  Created on: 12 oct. 2018
 *      Author: KATHE
 */

#include "fifoLib.h"
#include <stdint.h>
#include <stdlib.h>


int fifoInit(fifo_t* FIFO, uint16_t size){
	FIFO->fifo = (uint8_t *)malloc(size);
	if(FIFO->fifo == NULL)
		return -1;
	else {
		FIFO->getIdx = 0;
		FIFO->putIdx = 0;
		FIFO->size = size;
		return 0;
	}
}

int fifoPush(fifo_t* FIFO, uint8_t data){
	if(((FIFO->putIdx + 1) % FIFO->size)==FIFO->getIdx){
		return -1;
	}
	//Inicio zona crítica
	FIFO->fifo[FIFO->putIdx] = data;
	FIFO->putIdx = (FIFO->putIdx + 1) % FIFO->size;
	return 0;
	//Final zona crítica
}

int fifoPull(fifo_t* FIFO, uint8_t *data){
	if(FIFO->getIdx == FIFO->putIdx){
		return -1;
	}

	*data = FIFO->fifo[FIFO->getIdx];
	FIFO->getIdx = (FIFO->putIdx + 1) % FIFO->size;
	return 0;

}

uint16_t fifoGetAvailable(fifo_t* FIFO){
	uint16_t num = 0;
	if(FIFO->putIdx > FIFO->getIdx)
		num = FIFO->size - FIFO->putIdx + FIFO->getIdx - 1;
	else if(FIFO->putIdx < FIFO->getIdx)
		num = FIFO->getIdx - FIFO->putIdx - 1;
	else
		num = FIFO->size -1;
	return num;
}

void fifoUnInit(fifo_t* FIFO){
	free(FIFO->fifo);
}

void fifoClean(fifo_t* FIFO){
	FIFO->getIdx = 0;
	FIFO->putIdx = 0;
}



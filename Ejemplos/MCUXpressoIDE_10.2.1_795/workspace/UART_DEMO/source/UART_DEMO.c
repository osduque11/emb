/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    UART_DEMO.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include <string.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
#include "SIM_LIB.h"
#include "PORT_LIB.h"
#include "LPUART_LIB.h"
#include "TPM_LIB.h"


char msg[] = "No me hagas quedar mal, please!!!";
char color[20];

void printLPUART1(char * str){
    uint8_t i = 0;
    while(str[i] != '\0'){
    	if(bLPUART1_TDRE == 1){
    		bLPUART1_DATA_8BITS = str[i];
    		i++;
    	}
    }
}
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    PRINTF("Hello World\n");

    //Inicializar LPUART

    bSIM_LPUART1SRC = kSIM_LPUART1SRC_IRC48M;	//Selecciona fuente de reloj
    bSIM_LPUART1RXSRC = 0;		//Selecciona fuente RX
    bSIM_LPUART1TXSRC = 0;		//Selecciona fuente TX
    bSIM_CG_LPUART1 = 1;		//Activa reloj LPUART0

    //Inicializar pines lpuart 0
    bSIM_CG_PORTE = 1;	//Activar reloj puerto A
    bPTE1_MUX = kPTE1_MUX_LPUART1_RX;	//PTA1 es RX pin
    bPTE0_MUX = kPTE0_MUX_LPUART1_TX;	//PTA2 es TX pin

    //Inicicalizar LPUART0 para comunicación 115200 bps, no paridad, 1 bit stop, data 8 bits
    //Baudio_CLK/((OSR+1)*SBR)
    bLPUART1_OSR = 15;
    bLPUART1_SBR = 312;
    bLPUART1_TE = 1;
    bLPUART1_RE = 1;

    printLPUART1(msg);

    /* Force the counter to be placed into memory. */
   // volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */

    uint8_t i;
    char c = 0;
    while(1) {
    	i=0;
    	while(i < 20){
    		if(bLPUART1_RDRF == 1){
    			c = bLPUART1_DATA_8BITS;
    			if(c != '\n'){
        			color[i] = c;
        			i++;
    			}
    			else{
    				color[i] = '\0';
    				break;
    			}

    		}
    	}

    	printLPUART1(color);

    	if(strcmp(color,"verde") == 0){
    		//Prenda RGB verde
    		printLPUART1("OK\n"); //Imprime OK por serial
    	}
    	else if(strcmp(color,"rojo") == 0){
    		//Prenda RGB rojo
    		printLPUART1("OK\n"); //Imprime OK por serial
    	}
    	else if(strcmp(color,"azul") == 0){
    		//Prenda RGB azul
    		printLPUART1("OK\n"); //Imprime OK por serial
    	}
    	else{
    		//Imprima por serial
    		printLPUART1("No puedo :P");
    	}

    }
    return 0 ;
}

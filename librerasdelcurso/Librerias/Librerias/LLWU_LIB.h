/*
 * LLWU_LIB.h
 *
 *  Created on: 1/10/2017
 *      Author: Ricardo Andres Velasquez Velez
 */

#ifndef LLWU_LIB_H_
#define LLWU_LIB_H_

enum {
	kLLWU_WUPEx_PIN_DISABLED=0,								// External input pin disabled as wakeup input
	kLLWU_WUPEx_PIN_ENABLED_RISING_EDGE=1,					// External input pin enabled with rising edge detection
	kLLWU_WUPEx_PIN_ENABLED_FALLING_EDGE=2,					// External input pin enabled with falling edge detection
	kLLWU_WUPEx_PIN_ENABLED_ANY_CHANGE=3					// External input pin enabled with any change detection
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUPE0 			: 2;						// Wakeup Pin Enable For LLWU_P0
		uint8_t WUPE1 			: 2;						// Wakeup Pin Enable For LLWU_P1
		uint8_t WUPE2	 		: 2;						// Wakeup Pin Enable For LLWU_P2
		uint8_t WUPE3      		: 2;						// Wakeup Pin Enable For LLWU_P3
	}BITS;
}__llwu_pe1_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUPE4 			: 2;						// Wakeup Pin Enable For LLWU_P4
		uint8_t WUPE5 			: 2;						// Wakeup Pin Enable For LLWU_P5
		uint8_t WUPE6	 		: 2;						// Wakeup Pin Enable For LLWU_P6
		uint8_t WUPE7      		: 2;						// Wakeup Pin Enable For LLWU_P7
	}BITS;
}__llwu_pe2_t;


typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUPE8 			: 2;						// Wakeup Pin Enable For LLWU_P8
		uint8_t WUPE9 			: 2;						// Wakeup Pin Enable For LLWU_P9
		uint8_t WUPE10	 		: 2;						// Wakeup Pin Enable For LLWU_P10
		uint8_t WUPE11      	: 2;						// Wakeup Pin Enable For LLWU_P11
	}BITS;
}__llwu_pe3_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUPE12 			: 2;						// Wakeup Pin Enable For LLWU_P12
		uint8_t WUPE13 			: 2;						// Wakeup Pin Enable For LLWU_P13
		uint8_t WUPE14	 		: 2;						// Wakeup Pin Enable For LLWU_P14
		uint8_t WUPE15     		: 2;						// Wakeup Pin Enable For LLWU_P15
	}BITS;
}__llwu_pe4_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUME0 			: 1;						// Wakeup Module Enable For Module 0
		uint8_t WUME1 			: 1;						// Wakeup Module Enable For Module 1
		uint8_t WUME2	 		: 1;						// Wakeup Module Enable For Module 2
		uint8_t WUME3     		: 1;						// Wakeup Module Enable For Module 3
		uint8_t WUME4 			: 1;						// Wakeup Module Enable For Module 4
		uint8_t WUME5 			: 1;						// Wakeup Module Enable For Module 5
		uint8_t WUME6	 		: 1;						// Wakeup Module Enable For Module 6
		uint8_t WUME7     		: 1;						// Wakeup Module Enable For Module 7
	}BITS;
}__llwu_me_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUF0 			: 1;						// Wakeup Flag For LLWU_P0
		uint8_t WUF1 			: 1;						// Wakeup Flag For LLWU_P1
		uint8_t WUF2	 		: 1;						// Wakeup Flag For LLWU_P2
		uint8_t WUF3     		: 1;						// Wakeup Flag For LLWU_P3
		uint8_t WUF4 			: 1;						// Wakeup Flag For LLWU_P4
		uint8_t WUF5			: 1;						// Wakeup Flag For LLWU_P5
		uint8_t WUF6	 		: 1;						// Wakeup Flag For LLWU_P6
		uint8_t WUF7     		: 1;						// Wakeup Flag For LLWU_P7
	}BITS;
}__llwu_f1_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WUF8 			: 1;						// Wakeup Flag For LLWU_P8
		uint8_t WUF9 			: 1;						// Wakeup Flag For LLWU_P9
		uint8_t WUF10	 		: 1;						// Wakeup Flag For LLWU_P10
		uint8_t WUF11     		: 1;						// Wakeup Flag For LLWU_P11
		uint8_t WUF12 			: 1;						// Wakeup Flag For LLWU_P12
		uint8_t WUF13			: 1;						// Wakeup Flag For LLWU_P13
		uint8_t WUF14	 		: 1;						// Wakeup Flag For LLWU_P14
		uint8_t WUF15     		: 1;						// Wakeup Flag For LLWU_P15
	}BITS;
}__llwu_f2_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t MWUF0 			: 1;						// Wakeup Flag For module 0
		uint8_t MWUF1 			: 1;						// Wakeup Flag For module 1
		uint8_t MWUF2	 		: 1;						// Wakeup Flag For module 2
		uint8_t MWUF3     		: 1;						// Wakeup Flag For module 3
		uint8_t MWUF4 			: 1;						// Wakeup Flag For module 4
		uint8_t MWUF5			: 1;						// Wakeup Flag For module 5
		uint8_t MWUF6	 		: 1;						// Wakeup Flag For module 6
		uint8_t MWUF7     		: 1;						// Wakeup Flag For module 7
	}BITS;
}__llwu_f3_t;

enum{
	kLLWU_PIN0,												// Select LLWU_P0 for filter
	kLLWU_PIN1,												// Select LLWU_P1 for filter
	kLLWU_PIN2,												// Select LLWU_P2 for filter
	kLLWU_PIN3,												// Select LLWU_P3 for filter
	kLLWU_PIN4,												// Select LLWU_P4 for filter
	kLLWU_PIN5,												// Select LLWU_P5 for filter
	kLLWU_PIN6,												// Select LLWU_P6 for filter
	kLLWU_PIN7,												// Select LLWU_P7 for filter
	kLLWU_PIN8,												// Select LLWU_P8 for filter
	kLLWU_PIN9,												// Select LLWU_P9 for filter
	kLLWU_PIN10,											// Select LLWU_P10 for filter
	kLLWU_PIN11,											// Select LLWU_P11 for filter
	kLLWU_PIN12,											// Select LLWU_P12 for filter
	kLLWU_PIN13,											// Select LLWU_P13 for filter
	kLLWU_PIN14,											// Select LLWU_P14 for filter
	kLLWU_PIN15												// Select LLWU_P15 for filter
};

enum {
	kLLWU_FILTER_DISABLED,									// Filter disabled
	kLLWU_FILTER_POSEDGE,									// Filter posedge detect enabled
	kLLWU_FILTER_NEGEDGE,									// Filter negedge detect enabled
	kLLWU_FILTER_ANYEDGE,									// Filter any edge detect enabled
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t FILTSEL			: 4;						// Filter Pin Select
		uint8_t 	 			: 1;						//
		uint8_t FILTE	 		: 2;						// Digital Filter On External Pin
		uint8_t FILTF     		: 1;						// Filter Detect Flag
	}BITS;
}__llwu_filtx_t;

typedef struct{
	volatile __llwu_pe1_t LLWU_PE1; 						// 0x00 LLWU Pin Enable 1 register (LLWU_PE1)
	volatile __llwu_pe2_t LLWU_PE2; 						// 0x01 LLWU Pin Enable 2 register (LLWU_PE2)
	volatile __llwu_pe3_t LLWU_PE3; 						// 0x02 LLWU Pin Enable 3 register (LLWU_PE3)
	volatile __llwu_pe4_t LLWU_PE4; 						// 0x03 LLWU Pin Enable 4 register (LLWU_PE4)
	volatile __llwu_me_t LLWU_ME; 							// 0x04 LLWU Module Enable register (LLWU_ME)
	volatile __llwu_f1_t LLWU_F1; 							// 0x05 LLWU Flag 1 register (LLWU_F1)
	volatile __llwu_f2_t LLWU_F2; 							// 0x06 LLWU Flag 2 register (LLWU_F2)
	volatile __llwu_f3_t LLWU_F3; 							// 0x07 LLWU Flag 3 register (LLWU_F3)
	volatile __llwu_filtx_t LLWU_FILT1;						// 0x08 LLWU Pin Filter 1 register (LLWU_FILT1)
	volatile __llwu_filtx_t LLWU_FILT2;						// 0x09 LLWU Pin Filter 2 register (LLWU_FILT2)
} __llwu_t;

#define sLLWU (*((__llwu_t *)(0x4007C000)))  				// General structure to access the low-leakage wakeup unit

#define rLLWU_PE1 sLLWU.LLWU_PE1.BYTE						// LLWU Pin Enable 1 register (LLWU_PE1)
#define bLLWU_WUPE0 sLLWU.LLWU_PE1.BITS.WUPE0
#define bLLWU_WUPE1 sLLWU.LLWU_PE1.BITS.WUPE1
#define bLLWU_WUPE2 sLLWU.LLWU_PE1.BITS.WUPE2
#define bLLWU_WUPE3 sLLWU.LLWU_PE1.BITS.WUPE3

#define rLLWU_PE2 sLLWU.LLWU_PE2.BYTE 						// LLWU Pin Enable 2 register (LLWU_PE2)
#define bLLWU_WUPE4 sLLWU.LLWU_PE2.BITS.WUPE4
#define bLLWU_WUPE5 sLLWU.LLWU_PE2.BITS.WUPE5
#define bLLWU_WUPE6 sLLWU.LLWU_PE2.BITS.WUPE6
#define bLLWU_WUPE7 sLLWU.LLWU_PE2.BITS.WUPE7

#define rLLWU_PE3 sLLWU.LLWU_PE3.BYTE 						// LLWU Pin Enable 3 register (LLWU_PE3)
#define bLLWU_WUPE8 sLLWU.LLWU_PE3.BITS.WUPE8
#define bLLWU_WUPE9 sLLWU.LLWU_PE3.BITS.WUPE9
#define bLLWU_WUPE10 sLLWU.LLWU_PE3.BITS.WUPE10
#define bLLWU_WUPE11 sLLWU.LLWU_PE3.BITS.WUPE11

#define rLLWU_PE4 sLLWU.LLWU_PE4.BYTE 						// LLWU Pin Enable 4 register (LLWU_PE4)
#define bLLWU_WUPE12 sLLWU.LLWU_PE4.BITS.WUPE12
#define bLLWU_WUPE13 sLLWU.LLWU_PE4.BITS.WUPE13
#define bLLWU_WUPE14 sLLWU.LLWU_PE4.BITS.WUPE14
#define bLLWU_WUPE15 sLLWU.LLWU_PE4.BITS.WUPE15

#define rLLWU_ME sLLWU.LLWU_ME.BYTE 						// LLWU Module Enable register (LLWU_ME)
#define bLLWU_WUME0 sLLWU.LLWU_ME.BITS.WUME0				// Wakeup Module Enable For Module 0
#define bLLWU_WUME1 sLLWU.LLWU_ME.BITS.WUME1				// Wakeup Module Enable For Module 1
#define bLLWU_WUME2 sLLWU.LLWU_ME.BITS.WUME2				// Wakeup Module Enable For Module 2
#define bLLWU_WUME3 sLLWU.LLWU_ME.BITS.WUME3				// Wakeup Module Enable For Module 3
#define bLLWU_WUME4 sLLWU.LLWU_ME.BITS.WUME4				// Wakeup Module Enable For Module 4
#define bLLWU_WUME5 sLLWU.LLWU_ME.BITS.WUME5				// Wakeup Module Enable For Module 5
#define bLLWU_WUME6 sLLWU.LLWU_ME.BITS.WUME6				// Wakeup Module Enable For Module 6
#define bLLWU_WUME7 sLLWU.LLWU_ME.BITS.WUME7				// Wakeup Module Enable For Module 7

#define rLLWU_F1 sLLWU.LLWU_F1.BYTE 						// LLWU Flag 1 register (LLWU_F1)
#define bLLWU_WUF0 sLLWU.LLWU_F1.BITS.WUF0					// Wakeup Flag For LLWU_P0
#define bLLWU_WUF1 sLLWU.LLWU_F1.BITS.WUF1					// Wakeup Flag For LLWU_P1
#define bLLWU_WUF2 sLLWU.LLWU_F1.BITS.WUF2					// Wakeup Flag For LLWU_P2
#define bLLWU_WUF3 sLLWU.LLWU_F1.BITS.WUF3					// Wakeup Flag For LLWU_P3
#define bLLWU_WUF4 sLLWU.LLWU_F1.BITS.WUF4					// Wakeup Flag For LLWU_P4
#define bLLWU_WUF5 sLLWU.LLWU_F1.BITS.WUF5					// Wakeup Flag For LLWU_P5
#define bLLWU_WUF6 sLLWU.LLWU_F1.BITS.WUF6					// Wakeup Flag For LLWU_P6
#define bLLWU_WUF7 sLLWU.LLWU_F1.BITS.WUF7					// Wakeup Flag For LLWU_P7

#define rLLWU_F2 sLLWU.LLWU_F2.BYTE 						// LLWU Flag 2 register (LLWU_F2)
#define bLLWU_WUF8 sLLWU.LLWU_F2.BITS.WUF8					// Wakeup Flag For LLWU_P8
#define bLLWU_WUF9 sLLWU.LLWU_F2.BITS.WUF9					// Wakeup Flag For LLWU_P9
#define bLLWU_WUF10 sLLWU.LLWU_F2.BITS.WUF10				// Wakeup Flag For LLWU_P10
#define bLLWU_WUF11 sLLWU.LLWU_F2.BITS.WUF11				// Wakeup Flag For LLWU_P11
#define bLLWU_WUF12 sLLWU.LLWU_F2.BITS.WUF12				// Wakeup Flag For LLWU_P12
#define bLLWU_WUF13 sLLWU.LLWU_F2.BITS.WUF13				// Wakeup Flag For LLWU_P13
#define bLLWU_WUF14 sLLWU.LLWU_F2.BITS.WUF14				// Wakeup Flag For LLWU_P14
#define bLLWU_WUF15 sLLWU.LLWU_F2.BITS.WUF15				// Wakeup Flag For LLWU_P15

#define rLLWU_F3 sLLWU.LLWU_F3.BYTE 						// LLWU Flag 3 register (LLWU_F3)
#define bLLWU_MWUF0 sLLWU.LLWU_F3.BITS.WUF0					// Wakeup Flag For LLWU_P0
#define bLLWU_MWUF1 sLLWU.LLWU_F3.BITS.WUF1					// Wakeup Flag For LLWU_P1
#define bLLWU_MWUF2 sLLWU.LLWU_F3.BITS.WUF2					// Wakeup Flag For LLWU_P2
#define bLLWU_MWUF3 sLLWU.LLWU_F3.BITS.WUF3					// Wakeup Flag For LLWU_P3
#define bLLWU_MWUF4 sLLWU.LLWU_F3.BITS.WUF4					// Wakeup Flag For LLWU_P4
#define bLLWU_MWUF5 sLLWU.LLWU_F3.BITS.WUF5					// Wakeup Flag For LLWU_P5
#define bLLWU_MWUF6 sLLWU.LLWU_F3.BITS.WUF6					// Wakeup Flag For LLWU_P6
#define bLLWU_MWUF7 sLLWU.LLWU_F3.BITS.WUF7					// Wakeup Flag For LLWU_P7

#define rLLWU_FILT1 sLLWU.LLWU_FILT1.BYTE					// LLWU Pin Filter 1 register (LLWU_FILT1)
#define bLLWU_FILTSEL1 sLLWU.LLWU_FILT1.BITS.FILTSEL		// Filter Pin Select
#define bLLWU_FILTE1 sLLWU.LLWU_FILT1.BITS.FILTE			// Digital Filter On External Pin
#define bLLWU_FILTF1 sLLWU.LLWU_FILT1.BITS.FILTF			// Filter Detect Flag

#define rLLWU_FILT2 sLLWU.LLWU_FILT2.BYTE					// LLWU Pin Filter 2 register (LLWU_FILT2)
#define bLLWU_FILTSEL2 sLLWU.LLWU_FILT2.BITS.FILTSEL		// Filter Pin Select
#define bLLWU_FILTE2 sLLWU.LLWU_FILT2.BITS.FILTE			// Digital Filter On External Pin
#define bLLWU_FILTF2 sLLWU.LLWU_FILT2.BITS.FILTF			// Filter Detect Flag

#endif /* LLWU_LIB_H_ */

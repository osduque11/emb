/*
 * LPTMR_LIB.H
 *
 *  Created on: 24/09/2017
 *      Author: Mauricio Ramirez Henao
 */

#ifndef LPTMR_LIB_H_
#define LPTMR_LIB_H_

typedef union{
	unsigned long WORD;
	struct{
		uint32_t TEN 				:1;
		uint32_t TMS				:1;
		uint32_t TFC				:1;
		uint32_t TPP				:1;
		uint32_t TPS				:2;
		uint32_t TIE				:1;
		uint32_t TCF				:1;
		uint32_t					:8;
		uint32_t					:16;
	}BITS;
}_lptmr_csr_t;

typedef union{
	unsigned long WORD;
	struct{
		uint32_t PCS				:2;
		uint32_t PBYP				:1;
		uint32_t PRESCALE			:4;
		uint32_t					:9;
		uint32_t					:16;
	}BITS;
}_lptmr_psr_t;

typedef union{
	unsigned long WORD;
	struct{
		uint32_t COMPARE				:16;
		uint32_t 						:16;
	}BITS;
}_lptmr_cmr_t;

typedef union{
	unsigned long WORD;
	struct{
		uint32_t COUNTER				:16;
		uint32_t 						:16;
	}BITS;
}_lptmr_cnr_t;

typedef struct{
	volatile _lptmr_csr_t LPTMR_CSR; //0x00
	volatile _lptmr_psr_t LPTMR_PSR; //0x04
	volatile _lptmr_cmr_t LPTMR_CMR; //0x08
	volatile _lptmr_cnr_t LPTMR_CNR; //0x0C
} __lptmr0_t;

#define __LPTMR0 (*((__lptmr0_t *)(0x40040000)))

#define __LPTMR_CSR __LPTMR0.LPTM_CSR.WORD
#define __LPTMR_TEN __LPTMR0.LPTM_CSR.BITS.TEN
#define __LPTMR_TMS	__LPTMR0.LPTMR_CSR.BITS.TMS
#define __LPTMR_TFC __LPTMR0.LPTMR_CSR.BITS.TFC
#define __LPTMR_TPP __LPTMR0.LPTMR_CSR.BITS.TPP
#define __LPTMR_TPS __LPTMR0.LPTMR_CSR.BITS.TPS
#define __LPTMR_TIE __LPTMR0.LPTMR_CSR.BITS.TIE
#define __LPTMR_TCF __LPTMR0.LPTMR_CSR.BITS.TCF


#define __LPTMR_PSR __LPTMR0.LPTMR_PSR.WORD
#define __LPTMR_PCS __LPTMR0.LPTMR_PSR.BITS.PCS
#define __LPTMR_PBYP __LPTMR0.LPTMR_PSR.BITS.PBYP
#define __LPTMR_PRESCALE __LPTMR0.LPTMR_PSR.BITS.PRESCALE

#define __LPTMR_CMR	__LPTMR0.LPTMR_CMR.WORD
#define __LPTMR_COMPARE __LPTMR0.LPTMR_CMR.BITS.COMPARE

#define __LPTMR_CNR __LPTMR0.LPTMR_CNR.WORD
#define __LPTMR_COUNTER __LPTMR0.LPTMR_CNR.BITS.COUNTER


#endif /*LPTMR_LIB_H_*/




/*
 * RCM_LIB.h
 *
 *  Created on: 2/10/2017
 *      Author: Ricardo Andrés Velásquez Vélez
 */

#ifndef RCM_LIB_H_
#define RCM_LIB_H_

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t WAKEUP 			: 1;						// Low Leakage Wakeup Reset
		uint8_t LVD 			: 1;						// Low-Voltage Detect Reset
		uint8_t 		 		: 3;						// reserved
		uint8_t WDOG	 		: 1;						// Watchdog timeout Reset
		uint8_t PIN		 		: 1;						// External Reset Pin
		uint8_t POR		 		: 1;						// Power-On Reset
	}BITS;
}__rcm_srs0_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t 	 			: 1;						// reserved
		uint8_t LOCKUP 			: 1;						// Core Lockup Reset
		uint8_t SW		 		: 1;						// Software Reset
		uint8_t MDM_AP	 		: 1;						// MDM-AP System Reset Request
		uint8_t 		 		: 1;						// reserved
		uint8_t SACKERR		 	: 1;						// Stop Mode Acknowledge Error Reset
		uint8_t 		 		: 2;						// reserved
	}BITS;
}__rcm_srs1_t;

enum {
	kRCM_FILTERING_DISABLED,								// All filtering disabled
	kRCM_BUS_CLOCK_FILTER_ENABLED,							// Bus clock filter enabled for normal operation
	kRCM_LPO_CLOCK_FILTER_ENABLED							// LPO clock filter enabled for normal operation
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t RSTFLTSRW		: 2;						// Reset Pin Filter Select in Run and Wait Modes
		uint8_t RSTFLTSS		: 1;						// Reset Pin Filter Select in Stop Mode
		uint8_t 		 		: 5;						// reserved
	}BITS;
}__rcm_rpfc_t;

enum {
	kRCM_FILTER_COUNT_1,
	kRCM_FILTER_COUNT_2,
	kRCM_FILTER_COUNT_3,
	kRCM_FILTER_COUNT_4,
	kRCM_FILTER_COUNT_5,
	kRCM_FILTER_COUNT_6,
	kRCM_FILTER_COUNT_7,
	kRCM_FILTER_COUNT_8,
	kRCM_FILTER_COUNT_9,
	kRCM_FILTER_COUNT_10,
	kRCM_FILTER_COUNT_11,
	kRCM_FILTER_COUNT_12,
	kRCM_FILTER_COUNT_13,
	kRCM_FILTER_COUNT_14,
	kRCM_FILTER_COUNT_15,
	kRCM_FILTER_COUNT_16,
	kRCM_FILTER_COUNT_17,
	kRCM_FILTER_COUNT_18,
	kRCM_FILTER_COUNT_19,
	kRCM_FILTER_COUNT_20,
	kRCM_FILTER_COUNT_21,
	kRCM_FILTER_COUNT_22,
	kRCM_FILTER_COUNT_23,
	kRCM_FILTER_COUNT_24,
	kRCM_FILTER_COUNT_25,
	kRCM_FILTER_COUNT_26,
	kRCM_FILTER_COUNT_27,
	kRCM_FILTER_COUNT_28,
	kRCM_FILTER_COUNT_29,
	kRCM_FILTER_COUNT_30,
	kRCM_FILTER_COUNT_31,
	kRCM_FILTER_COUNT_32
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t RSTFLTSEL		: 5;						// Reset Pin Filter Select in Run and Wait Modes
		uint8_t 		 		: 3;						// reserved
	}BITS;
}__rcm_rpfw_t;

enum {
	kRCM_NO_EFFECT,											// No effect
	kRCM_BOOT_FROM_ROM_MR1,									// Force boot from ROM with RCM_MR[1] set.
	kRCM_BOOT_FROM_ROM_MR2,									// Force boot from ROM with RCM_MR[2] set.
	kRCM_BOOT_FROM_ROM_MR21									// Force boot from ROM with RCM_MR[2:1] set.
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t 		 		: 1;						// reserved
		uint8_t FORCEROM		: 2;						// Force ROM Boot
		uint8_t 		 		: 5;						// reserved
	}BITS;
}__rcm_fm_t;

enum {
	kRCM_BOOT_FROM_FLASH,									// Boot from Flash
	kRCM_BOOT_FROM_ROM_BOOTCFG0,							// Boot from ROM due to BOOTCFG0 pin assertion
	kRCM_BOOT_FROM_ROM_FOPT7,								// Boot form ROM due to FOPT[7] configuration
	kRCM_BOOT_FROM_ROM_BOOTCFG0_PLUS_FOPT7					// Boot from ROM due to both BOOTCFG0 pin assertion and FOPT[7] configuration
};

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t 		 		: 1;						// reserved
		uint8_t BOOTROM			: 2;						// Boot ROM Configuration
		uint8_t 		 		: 5;						// reserved
	}BITS;
}__rcm_mr_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t SWAKEUP 		: 1;						// Sticky Low Leakage Wakeup Reset
		uint8_t SLVD 			: 1;						// Sticky Low-Voltage Detect Reset
		uint8_t 		 		: 3;						// reserved
		uint8_t SWDOG	 		: 1;						// Sticky Watchdog timeout Reset
		uint8_t SPIN		 	: 1;						// Sticky External Reset Pin
		uint8_t SPOR		 	: 1;						// Sticky Power-On Reset
	}BITS;
}__rcm_ssrs0_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t 	 			: 1;						// reserved
		uint8_t SLOCKUP 		: 1;						// Sticky Core Lockup Reset
		uint8_t SSW		 		: 1;						// Sticky Software Reset
		uint8_t SMDM_AP	 		: 1;						// Sticky MDM-AP System Reset Request
		uint8_t 		 		: 1;						// reserved
		uint8_t SSACKERR		: 1;						// Sticky Stop Mode Acknowledge Error Reset
		uint8_t 		 		: 2;						// reserved
	}BITS;
}__rcm_ssrs1_t;

typedef struct{
	volatile __rcm_srs0_t RCM_SRS0;							// 0x00 System Reset Status Register 0 (RCM_SRS0)
	volatile __rcm_srs1_t RCM_SRS1;							// 0x01 System Reset Status Register 1 (RCM_SRS1)
	volatile uint8_t RESERVED[2];							// 0x02-0x03 Reserved
	volatile __rcm_rpfc_t RCM_RPFC;							// 0x04 Reset Pin Filter Control register (RCM_RPFC)
	volatile __rcm_rpfw_t RCM_RPFW; 						// 0x05 Reset Pin Filter Width register (RCM_RPFW)
	volatile __rcm_fm_t RCM_FM; 							// 0x06 Force Mode Register (RCM_FM)
	volatile __rcm_mr_t RCM_MR;								// 0x07 Mode Register (RCM_MR)
	volatile __rcm_ssrs0_t RCM_SSRS0;						// 0x08 Sticky System Reset Status Register 0 (RCM_SSRS0)
	volatile __rcm_ssrs1_t RCM_SSRS1;						// 0x09 Sticky System Reset Status Register 1 (RCM_SSRS1)
} __rcm_t;

#define sRCM (*((__rcm_t *)(0x4007F000)))  					// General structure to access the low-leakage wakeup unit

#define rRCM_SRS0 sRCM.RCM_SRS0.BYTE						// System Reset Status Register 0 (RCM_SRS0)
#define bRCM_WAKEUP sRCM.RCM_SRS0.BITS.WAKEUP				// Low Leakage Wakeup Reset
#define bRCM_LVD sRCM.RCM_SRS0.BITS.LVD						// Low-Voltage Detect Reset
#define bRCM_WDOG sRCM.RCM_SRS0.BITS.WDOG					// Watchdog timeout Reset
#define bRCM_PIN sRCM.RCM_SRS0.BITS.PIN						// External Reset Pin
#define bRCM_POR sRCM.RCM_SRS0.BITS.POR						// Power-On Reset

#define rRCM_SRS1 sRCM.RCM_SRS1.BYTE						// System Reset Status Register 1 (RCM_SRS1)
#define bRCM_LOCKUP sRCM.RCM_SRS1.BITS.LOCKUP				// Core Lockup Reset
#define bRCM_SW	sRCM.RCM_SRS1.BITS.SW						// Software Reset
#define bRCM_MDM_AP	sRCM.RCM_SRS1.BITS.MDM_AP				// MDM-AP System Reset Request
#define bRCM_SACKERR sRCM.RCM_SRS1.BITS.SACKERR				// Stop Mode Acknowledge Error Reset

#define rRCM_RPFC sRCM.RCM_RPFC.BYTE						// Reset Pin Filter Control register (RCM_RPFC)
#define bRCM_RSTFLTSRW	sRCM.RCM_RPFC.BITS.RSTFLTSRW		// Reset Pin Filter Select in Run and Wait Modes
#define bRCM_RSTFLTSS sRCM.RCM_RPFC.BITS.RSTFLTSS			// Reset Pin Filter Select in Stop Mode

#define rRCM_RPFW sRCM.RCM_RPFW.BYTE 						// Reset Pin Filter Width register (RCM_RPFW)
#define bRCM_RSTFLTSEL	sRCM.RCM_RPFW.BITS.RSTFLTSEL		// Reset Pin Filter Select in Run and Wait Modes

#define rRCM_FM sRCM.RCM_RM.BYTE 							// Force Mode Register (RCM_FM)
#define bRCM_FORCEROM sRCM.RCM_RM.BITS.FORCEROM				// Force ROM Boot

#define rRCM_MR sRCM.RCM_MR.BYTE							// Mode Register (RCM_MR)
#define bRCM_BOOTROM sRCM.RCM_MR.BITS.BOOTROM				// Boot ROM Configuration

#define rRCM_SSRS0 sRCM.RCM_SSRS0.BYTE						// Sticky System Reset Status Register 0 (RCM_SSRS0)
#define bRCM_SWAKEUP sRCM.RCM_SSRS0.BITS.SWAKEUP			// Low Leakage Wakeup Reset
#define bRCM_SLVD sRCM.RCM_SSRS0.BITS.SLVD					// Low-Voltage Detect Reset
#define bRCM_SWDOG sRCM.RCM_SSRS0.BITS.SWDOG				// Watchdog timeout Reset
#define bRCM_SPIN sRCM.RCM_SSRS0.BITS.SPIN					// External Reset Pin
#define bRCM_SPOR sRCM.RCM_SSRS0.BITS.SPOR					// Power-On Reset

#define rRCM_SSRS1 sRCM.RCM_SSRS1.BYTE						// Sticky System Reset Status Register 1 (RCM_SSRS1)
#define bRCM_SLOCKUP sRCM.RCM_SSRS1.BITS.SLOCKUP			// Core Lockup Reset
#define bRCM_SSW sRCM.RCM_SSRS1.BITS.SSW					// Software Reset
#define bRCM_SMDM_AP sRCM.RCM_SSRS1.BITS.SMDM_AP			// MDM-AP System Reset Request
#define bRCM_SSACKERR sRCM.RCM_SSRS1.BITS.SSACKERR			// Stop Mode Acknowledge Error Reset

#endif /* RCM_LIB_H_ */

/*
 * OSC_LIB.h
 *
 *  Created on: 13/10/2017
 *      Author: ricardo
 */

#ifndef OSC_LIB_H_
#define OSC_LIB_H_

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t SC16P			: 1;								// Oscillator 16 pF Capacitor Load Configure
		uint8_t SC8P	 		: 1;								// Oscillator 8 pF Capacitor Load Configure
		uint8_t SC4P 			: 1;								// Oscillator 4 pF Capacitor Load Configure
		uint8_t SC2P        	: 1;								// Oscillator 2 pF Capacitor Load Configure
		uint8_t  				: 1;								// Reserved
		uint8_t EREFSTEN		: 1;								// External Reference Stop Enable
		uint8_t 	 			: 1;								// Reserved
		uint8_t ERCLKEN			: 1;								// External Reference Enable
	}BITS;
}__osc_cr_t;

typedef volatile __osc_cr_t _osc_t;

#define sOSC0 (*((__osc_t *)(0x40065000)))

#define rOSC0_CR sOSC0.BYTE											// OSC Control Register (OSCx_CR)
#define	bOSC0_SC16P	sOSC0.BITS.SC16P								// Oscillator 16 pF Capacitor Load Configure
#define	bOSC0_SC8P	sOSC0.BITS.SC8P									// Oscillator 8 pF Capacitor Load Configure
#define	bOSC0_SC4P	sOSC0.BITS.SC4P 								// Oscillator 4 pF Capacitor Load Configure
#define	bOSC0_SC2P	sOSC0.BITS.SC2P  								// Oscillator 2 pF Capacitor Load Configure
#define	bOSC0_EREFSTEN	sOSC0.BITS.EREFSTEN							// External Reference Stop Enable
#define	bOSC0_ERCLKEN	sOSC0.BITS.ERCLKEN							// External Reference Enable

#endif /* OSC_LIB_H_ */

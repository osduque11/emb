/*
 * SPI_LIB.h Serial Peripheral Interface
 *
 *  Created on: 24/09/2017
 *      Author: jonathan gomez
 *      Review : Ricardo Andrés Velásquez
 */

#ifndef SPI_LIB_H_
#define SPI_LIB_H_

enum {
	mSPI_S_RFIFOEF = 0x01,								// SPI read FIFO empty flag
	mSPI_S_TXFULLF = 0x02,								// Transmit FIFO full flag
	mSPI_S_TNEAREF = 0x04,								// Transmit FIFO nearly empty flag
	mSPI_S_RNFULLF = 0x08,								// Receive FIFO nearly full flag
	mSPI_S_MODF = 0x10,									// Master Mode Fault Flag
	mSPI_S_SPTEF = 0x20,								// SPI Transmit Buffer Empty Flag
	mSPI_S_SPMF = 0x40,									// SPI Match Flag
	mSPI_S_SPRF = 0x80									// SPI Read Buffer Full Flag
};
typedef union{
	uint8_t BYTE;
	struct{
		uint8_t RFIFOEF		: 1;						// SPI read FIFO empty flag
		uint8_t TXFULLF 	: 1;						// Transmit FIFO full flag
		uint8_t TNEAREF 	: 1;						// Transmit FIFO nearly empty flag
		uint8_t RNFULLF    	: 1;						// Receive FIFO nearly full flag
		uint8_t MODF  		: 1;						// Master Mode Fault Flag
		uint8_t SPTEF	  	: 1;						// SPI Transmit Buffer Empty Flag
		uint8_t SPMF	    : 1;						// SPI Match Flag
		uint8_t SPRF	    : 1;						// SPI Read Buffer Full Flag
	}BITS;
}__spi_s_t;

#define mSPI_BR_SPPR(X) ((X & 0x07)<<4)					// SPI Baud Rate Prescale Divisor
#define mSPI_BR_SPR(X) (X & 0x0F)						// SPI Baud Rate Divisor

enum{
	kSPI_SPPR_DIVIDE_BY_1,
	kSPI_SPPR_DIVIDE_BY_2,
	kSPI_SPPR_DIVIDE_BY_3,
	kSPI_SPPR_DIVIDE_BY_4,
	kSPI_SPPR_DIVIDE_BY_5,
	kSPI_SPPR_DIVIDE_BY_6,
	kSPI_SPPR_DIVIDE_BY_7,
	kSPI_SPPR_DIVIDE_BY_8
};

enum{
	kSPI_SPR_DIVIDE_BY_2,
	kSPI_SPR_DIVIDE_BY_4,
	kSPI_SPR_DIVIDE_BY_8,
	kSPI_SPR_DIVIDE_BY_16,
	kSPI_SPR_DIVIDE_BY_32,
	kSPI_SPR_DIVIDE_BY_64,
	kSPI_SPR_DIVIDE_BY_128,
	kSPI_SPR_DIVIDE_BY_256,
	kSPI_SPR_DIVIDE_BY_512
};

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t SPR			: 4;						// SPI Baud Rate Divisor
		uint8_t SPPR	 	: 3;						// SPI Baud Rate Prescale Divisor
		uint8_t 		 	: 1;						// Reserved
	}BITS;
}__spi_br_t;

enum{
	mSPI_C2_SPC0 = 0x01, 								//SPI Pin Control 0
	mSPI_C2_SPISWAI = 0x02, 							// SPI Stop in Wait Mode
	mSPI_C2_RXDMAE = 0x04, 								// Receive DMA enable
	mSPI_C2_BIDIROE = 0x08, 							// Bidirectional Mode Output Enable
	mSPI_C2_MODFEN = 0x10, 								// Master Mode-Fault Function Enable
	mSPI_C2_TXDMAE = 0x20, 								// Transmit DMA enable
	mSPI_C2_SPIMODE = 0x40, 							// SPI 8-bit or 16-bit mode
	mSPI_C2_SPMIE = 0x80 								// SPI Match Interrupt Enable
};

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t SPC0		: 1;						// SPI Pin Control 0
		uint8_t SPISWAI 	: 1;						// SPI Stop in Wait Mode
		uint8_t RXDMAE 		: 1;						// Receive DMA enable
		uint8_t BIDIROE    	: 1;						// Bidirectional Mode Output Enable
		uint8_t MODFEN  	: 1;						// Master Mode-Fault Function Enable
		uint8_t TXDMAE	  	: 1;						// Transmit DMA enable
		uint8_t SPIMODE	    : 1;						// SPI 8-bit or 16-bit mode
		uint8_t SPMIE	    : 1;						// SPI Match Interrupt Enable
	}BITS;
}__spi_c2_t;

enum{
	mSPI_C1_LSBFE = 0x01, 								// LSB First (shifter direction)
	mSPI_C1_SSOE = 0x02, 								// Slave Select Output Enable
	mSPI_C1_CPHA = 0x04, 								// Clock Phase
	mSPI_C1_CPOL = 0x08, 								// Clock Polarity
	mSPI_C1_MSTR = 0x10, 								// Master/Slave Mode Select
	mSPI_C1_SPTIE = 0x20, 								// SPI Transmit Interrupt Enable
	mSPI_C1_SPE = 0x40, 								// SPI System Enable
	mSPI_C1_SPIE = 0x80 								// SPI Interrupt Enable
};
typedef union{
	uint8_t BYTE;
	struct{
		uint8_t LSBFE		: 1;						// LSB First (shifter direction)
		uint8_t SSOE 		: 1;						// Slave Select Output Enable
		uint8_t CPHA 		: 1;						// Clock Phase
		uint8_t CPOL    	: 1;						// Clock Polarity
		uint8_t MSTR  		: 1;						// Master/Slave Mode Select
		uint8_t SPTIE	  	: 1;						// SPI Transmit Interrupt Enable
		uint8_t SPE	    	: 1;						// SPI System Enable
		uint8_t SPIE	    : 1;						// SPI Interrupt Enable
	}BITS;
}__spi_c1_t;

typedef uint8_t __spi_ml_t;

typedef uint8_t __spi_mh_t;

typedef uint8_t __spi_dl_t;

typedef uint8_t __spi_dh_t;

enum{
	mSPI_CI_SPRFCI = 0x01,								// Receive FIFO full flag clear interrupt
	mSPI_CI_SPTEFCI = 0x02,								// Transmit FIFO empty flag clear interrupt
	mSPI_CI_RNFULLFCI = 0x04,							// Receive FIFO nearly full flag clear interrupt
	mSPI_CI_TNEAREFCI = 0x08,							// Transmit FIFO nearly empty flag clear interrupt
	mSPI_CI_RXFOF = 0x10,								// Receive FIFO overflow flag
	mSPI_CI_TXFOF = 0x20,								// Transmit FIFO overflow flag
	mSPI_CI_RXFERR = 0x40,								// Receive FIFO error flag
	mSPI_CI_TXFERR = 0x80								// Transmit FIFO error flag
};

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t SPRFCI		: 1;						// Receive FIFO full flag clear interrupt
		uint8_t SPTEFCI		: 1;						// Transmit FIFO empty flag clear interrupt
		uint8_t RNFULLFCI	: 1;						// Receive FIFO nearly full flag clear interrupt
		uint8_t TNEAREFCI  	: 1;						// Transmit FIFO nearly empty flag clear interrupt
		uint8_t RXFOF  		: 1;						// Receive FIFO overflow flag
		uint8_t TXFOF	  	: 1;						// Transmit FIFO overflow flag
		uint8_t RXFERR   	: 1;						// Receive FIFO error flag
		uint8_t TXFERR	    : 1;						// Transmit FIFO error flag
	}BITS;
}__spi_ci_t;

enum {
	mSPI_C3_FIFOMODE = 0x01,							// FIFO mode enable
	mSPI_C3_RNFULLIEN = 0x02,							// Receive FIFO nearly full interrupt enable
	mSPI_C3_TNEARIEN = 0x04,							// Transmit FIFO nearly empty interrupt enable
	mSPI_C3_INTCLR = 0x08,								// Interrupt clearing mechanism select
	mSPI_C3_RNFULLF_MARK = 0x10,						// Receive FIFO nearly full watermark
	mSPI_C3_TNEAREF_MARK = 0x20							// Transmit FIFO nearly empty watermark
};

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t FIFOMODE		: 1;					// FIFO mode enable
		uint8_t RNFULLIEN 		: 1;					// Receive FIFO nearly full interrupt enable
		uint8_t TNEARIEN 		: 1;					// Transmit FIFO nearly empty interrupt enable
		uint8_t INTCLR    		: 1;					// Interrupt clearing mechanism select
		uint8_t RNFULLF_MARK  	: 1;					// Receive FIFO nearly full watermark
		uint8_t TNEAREF_MARK	: 1;					// Transmit FIFO nearly empty watermark
		uint8_t 	    		: 2;					// Reserved
	}BITS;
}__spi_c3_t;

typedef struct{
	volatile __spi_s_t S;								// 0x00 SPI Status Register (SPIx_S)
	volatile __spi_br_t BR;								// 0x01 SPI Baud Rate Register (SPIx_BR)
	volatile __spi_c2_t C2;								// 0x02 SPI Control Register 2 (SPIx_C2)
	volatile __spi_c1_t C1;								// 0x03 SPI Control Register 1 (SPIx_C1)
	volatile __spi_ml_t ML;								// 0x04 SPI Match Register low (SPIx_ML) 8
	volatile __spi_mh_t MH;								// 0x05 SPI match register high (SPIx_MH)
	volatile __spi_dl_t DL;								// 0x06 SPI Data Register low (SPIx_DL)
	volatile __spi_dh_t DH;								// 0x07 SPI data register high (SPIx_DH)
	volatile uint8_t RESERVED[2];						// 0x08-0x09 Reserved
	volatile __spi_ci_t CI;								// 0x0A SPI clear interrupt register (SPIx_CI)
	volatile __spi_c3_t C3;								// 0x0B SPI control register 3 (SPIx_C3)

} __spi_t;

//********************		SPI0	*************************
#define sSPI0 (*((__spi_t *)(0x40076000)))				// SPI0 module general register structure

#define rSPI0_S sSPI0.SPI_S.BYTE						// SPI Status Register (SPI0_S)
#define bSPI0_RFIFOEF	sSPI0.SPI_S.BITS.RFIFOEF		// SPI read FIFO empty flag
#define bSPI0_TXFULLF	sSPI0.SPI_S.BITS.TXFULLF		// Transmit FIFO full flag
#define bSPI0_TNEAREF	sSPI0.SPI_S.BITS.TNEAREF		// Transmit FIFO nearly empty flag
#define bSPI0_RNFULLF	sSPI0.SPI_S.BITS.RNFULLF		// Receive FIFO nearly full flag
#define bSPI0_MODF		sSPI0.SPI_S.BITS.MODF			// Master Mode Fault Flag
#define bSPI0_SPTEF	sSPI0.SPI_S.BITS.SPTEF				// SPI Transmit Buffer Empty Flag
#define bSPI0_SPMF		sSPI0.SPI_S.BITS.SPMF			// SPI Match Flag
#define bSPI0_SPRF		sSPI0.SPI_S.BITS.SPRF			// SPI Read Buffer Full Flag

#define rSPI0_BR sSPI0.SPI_BR.BYTE						// SPI Baud Rate Register (SPI0_BR)
#define bSPI0_RPR sSPI0.SPI_BR.BITS.SPR					// SPI Baud Rate Divisor
#define bSPI0_SPPR sSPI0.SPI_BR.BITS.SPPR				// SPI Baud Rate Prescale Divisor

#define rSPI0_C2 sSPI0.SPI_C2.BYTE						// SPI Control Register 2 (SPI0_C2)
#define bSPI0_SPC0 sSPI0.SPI_C2.BITS.SPC0				// SPI Pin Control 0
#define bSPI0_SPISWAI sSPI0.SPI_C2.BITS.SPISWAI			// SPI Stop in Wait Mode
#define bSPI0_RXDMAE sSPI0.SPI_C2.BITS.RXDMAE			// Receive DMA enable
#define bSPI0_BIDIROE sSPI0.SPI_C2.BITS.BIDIROE			// Bidirectional Mode Output Enable
#define bSPI0_MODFEN sSPI0.SPI_C2.BITS.MODFEN			// Master Mode-Fault Function Enable
#define bSPI0_TXDMAE sSPI0.SPI_C2.BITS.TXDMAE			// Transmit DMA enable
#define bSPI0_SPIMODE sSPI0.SPI_C2.BITS.SPIMODE			// SPI 8-bit or 16-bit mode
#define bSPI0_SPMIE sSPI0.SPI_C2.BITS.SPMIE				// SPI Match Interrupt Enable

#define rSPI0_C1 sSPI0.SPI0_C1.BYTE						// SPI Control Register 1 (SPI0_C1)
#define bSPI0_LSBFE sSPI0.SPI_C1.BITS.LSBFE				// LSB First (shifter direction)
#define bSPI0_SSOE sSPI0.SPI_C1.BITS.SSOE				// Slave Select Output Enable
#define bSPI0_CPHA sSPI0.SPI_C1.BITS.CPHA				// Clock Phase
#define bSPI0_CPOL sSPI0.SPI_C1.BITS.CPOL				// Clock Polarity
#define bSPI0_MSTR sSPI0.SPI_C1.BITS.MSTR				// Master/Slave Mode Select
#define bSPI0_SPTIE sSPI0.SPI_C1.BITS.SPTIE				// SPI Transmit Interrupt Enable
#define bSPI0_SPE sSPI0.SPI_C1.BITS.SPE					// SPI System Enable
#define bSPI0_SPIE sSPI0.SPI_C1.BITS.SPIE				// SPI Interrupt Enable

#define rSPI0_ML sSPI0.SPI_ML						// SPI Match Register low (SPI0_ML)

#define rSPI0_MH sSPI0.SPI0_MH						// SPI match register high (SPI0_MH)

#define rSPI0_DL sSPI0.SPI0_DL						// SPI Data Register low (SPI0_DL)

#define rSPI0_DH sSPI0.SPI0_DH						// SPI data register high (SPI0_DH)

//********************		SPI1	*************************
#define sSPI1 (*((__spi_t *)(0x40077000)))

#define rSPI1_S sSPI1.SPI_S.BYTE						// SPI Status Register (SPI1_S)
#define bSPI1_RFIFOEF	sSPI1.SPI_S.BITS.RFIFOEF		// SPI read FIFO empty flag
#define bSPI1_TXFULLF	sSPI1.SPI_S.BITS.TXFULLF		// Transmit FIFO full flag
#define bSPI1_TNEAREF	sSPI1.SPI_S.BITS.TNEAREF		// Transmit FIFO nearly empty flag
#define bSPI1_RNFULLF	sSPI1.SPI_S.BITS.RNFULLF		// Receive FIFO nearly full flag
#define bSPI1_MODF		sSPI1.SPI_S.BITS.MODF			// Master Mode Fault Flag
#define bSPI1_SPTEF	sSPI1.SPI_S.BITS.SPTEF				// SPI Transmit Buffer Empty Flag
#define bSPI1_SPMF		sSPI1.SPI_S.BITS.SPMF			// SPI Match Flag
#define bSPI1_SPRF		sSPI1.SPI_S.BITS.SPRF			// SPI Read Buffer Full Flag

#define rSPI1_BR sSPI1.SPI_BR.BYTE						// SPI Baud Rate Register (SPI1_BR)
#define bSPI1_RPR sSPI1.SPI_BR.BITS.SPR					// SPI Baud Rate Divisor
#define bSPI1_SPPR sSPI1.SPI_BR.BITS.SPPR				// SPI Baud Rate Prescale Divisor

#define rSPI1_C2 sSPI1.SPI_C2.BYTE						// SPI Control Register 2 (SPI1_C2)
#define bSPI1_SPC0 sSPI1.SPI_C2.BITS.SPC0				// SPI Pin Control 0
#define bSPI1_SPISWAI sSPI1.SPI_C2.BITS.SPISWAI			// SPI Stop in Wait Mode
#define bSPI1_RXDMAE sSPI1.SPI_C2.BITS.RXDMAE			// Receive DMA enable
#define bSPI1_BIDIROE sSPI1.SPI_C2.BITS.BIDIROE			// Bidirectional Mode Output Enable
#define bSPI1_MODFEN sSPI1.SPI_C2.BITS.MODFEN			// Master Mode-Fault Function Enable
#define bSPI1_TXDMAE sSPI1.SPI_C2.BITS.TXDMAE			// Transmit DMA enable
#define bSPI1_SPIMODE sSPI1.SPI_C2.BITS.SPIMODE			// SPI 8-bit or 16-bit mode
#define bSPI1_SPMIE sSPI1.SPI_C2.BITS.SPMIE				// SPI Match Interrupt Enable

#define rSPI1_C1 sSPI1.SPI1_C1.BYTE						// SPI Control Register 1 (SPI1_C1)
#define bSPI1_LSBFE sSPI1.SPI_C1.BITS.LSBFE				// LSB First (shifter direction)
#define bSPI1_SSOE sSPI1.SPI_C1.BITS.SSOE				// Slave Select Output Enable
#define bSPI1_CPHA sSPI1.SPI_C1.BITS.CPHA				// Clock Phase
#define bSPI1_CPOL sSPI1.SPI_C1.BITS.CPOL				// Clock Polarity
#define bSPI1_MSTR sSPI1.SPI_C1.BITS.MSTR				// Master/Slave Mode Select
#define bSPI1_SPTIE sSPI1.SPI_C1.BITS.SPTIE				// SPI Transmit Interrupt Enable
#define bSPI1_SPE sSPI1.SPI_C1.BITS.SPE					// SPI System Enable
#define bSPI1_SPIE sSPI1.SPI_C1.BITS.SPIE				// SPI Interrupt Enable

#define rSPI1_ML sSPI1.SPI_ML							// SPI Match Register low (SPI1_ML)

#define rSPI1_MH sSPI1.SPI1_MH							// SPI match register high (SPI1_MH)

#define rSPI1_DL sSPI1.SPI1_DL							// SPI Data Register low (SPI1_DL)

#define rSPI1_DH sSPI1.SPI1_DH							// SPI data register high (SPI1_DH)



#endif /* SPI_LIB_H_ */

/*
 * RTC_LIB.h REAL TIMER COUNTER MODULE
 *
 *  Created on: 23/09/2017
 *      Author: Luis Angel Davila Causil
 */

#ifndef RTC_LIB_H_
#define RTC_LIB_H_

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t  	TSR		: 32;		//
	}BITS;
}__rtc_tsr_t;

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t  	TPR		: 16;		//
		uint32_t  			: 16;		//
	}BITS;
}__rtc_tpr_t;

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t  	TAR		: 32;		//
	}BITS;
}__rtc_tar_t;

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t TCR 		: 8;		//
		uint32_t CIR     	: 8;		//
		uint32_t TCV    	: 8;		//
		uint32_t CIC       	: 8;		//
	}BITS;
}__rtc_tcr_t;
typedef union{
	unsigned long WORD;
	 struct{
		uint32_t SWR 		: 1;		//
		uint32_t WPE     	: 1;		//
		uint32_t SUP    	: 1;		//
		uint32_t UM       	: 1;		//
		uint32_t WPS 		: 1;		//
		uint32_t        	: 3;		//
		uint32_t OSCE    	: 1;		//
		uint32_t CLKO       : 1;		//
		uint32_t SC16P    	: 1;		//
		uint32_t SC8P       : 1;		//
		uint32_t SC4P    	: 1;		//
		uint32_t SC2P       : 1;		//	
		uint32_t 		    : 1;		//
		uint32_t            : 17;		//  					
	}BITS;
}__rtc_cr;

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t TIF 	: 1;		//
		uint32_t TOF 	: 1;		//
		uint32_t TAF   	: 1;		//
		uint32_t     	: 1;		//
		uint32_t TCE   	: 1;		//
		uint32_t  		: 27;		//
  }BITS;
}__rtc_sr;

typedef union{
	unsigned long WORD;
	 struct{
		uint32_t  		: 3;		//
		uint32_t TCL 	: 1;		//
		uint32_t CRL   	: 1;		//
		uint32_t SRL   	: 1;		//
		uint32_t LRL  	: 1;		//
		uint32_t  		: 1;		//
		uint32_t  		: 24;		//
  }BITS;
}__rtc_lr;


typedef union{
	unsigned long WORD;
	 struct{
		uint32_t TIIE 		: 1;		//
		uint32_t TOIE 		: 1;		//
		uint32_t TAIE     	: 1;		//
		uint32_t 			: 1;		//
		uint32_t TSIE     	: 1;		//
		uint32_t 			: 2;		//
		uint32_t WPON 		: 1;		//
		uint32_t  			: 24;		//
  }BITS;
}__rtc_ier;


typedef struct{
	volatile __rtc_tsr_t RTC_TSR;
	volatile __rtc_tpr_t RTC_TPR;
	volatile __rtc_tar_t RTC_TAR;
	volatile __rtc_tcr_t RTC_TCR;
	volatile __rtc_cr 	 RTC_CR;
	volatile __rtc_sr 	 RTC_SR;
	volatile __rtc_lr 	 RTC_LR;
	volatile __rtc_ier 	 RTC_IER;
} __rtc;

#define __RTC (*((__rtc *)(0x4003D000)))

#define __RTC_TSR __RTC.RTC_TSR.WORD ////TIME SECONDS REGISTER
//#define __RTC_TSR __RTC.RTC_TSR.BITS.TSR

#define __RTC_TPR __RTC.RTC_TPR.WORD //
#define __RTC_TPR_TPR __RTC.RTC_TAR.BITS.TPR ////TIME PRESCALES REGISTER

#define __RTC_TAR __RTC.RTC_TAR.WORD //TIME ALARM REGISTER
//#define __RTC_TAR __RTC.RTC_TAR.BITS.TAR

#define __RTC_TCR __RTC.RTC_TCR.WORD //TIME COMPENSATION REGISTER
#define __RTC_TCR_TCR __RTC.RTC_TCR.BITS.TCR //TIME COMPENSATION REGISTER
#define __RTC_COR __RTC.RTC_TCR.BITS.CIR //COMPENSATION INTERVAL REGISTER
#define __RTC_TCV __RTC.RTC_TCR.BITS.TCV //TIME COMPENSATION VALUE
#define __RTC_CIC __RTC.RTC_TCR.BITS.CIC //COMPENSATION INTERVAL COUNTER


#define __RTC_CR __RTC.RTC_CR.WORD //RTC CONTROL REGISTER
#define __RTC_SWR __RTC.RTC_CR.BITS.SWR //SOFTWARE RESET
#define __RTC_WPE __RTC.RTC_CR.BITS.WPE //WAKEUP PIN ENABLE 0 DISABLED
#define __RTC_SUP __RTC.RTC_CR.BITS.SUP //SUPERVISOR ACCESS
#define __RTC_UM __RTC.RTC_CR.BITS.UM //UPDATE MODE
#define __RTC_WPS __RTC.RTC_CR.BITS.WPS //WAKEUP PIN SELECT
#define __RTC_OSCE __RTC.RTC_CR.BITS.OSCE //OSCILLATOR ENABLE
#define __RTC_CLKO __RTC.RTC_CR.BITS.CLKO //CLOCK OUTPUT
#define __RTC_SC16P __RTC.RTC_CR.BITS.SC16P //OSCILATOR 16pF LOAD CONFIGURE
#define __RTC_SC8P __RTC.RTC_CR.BITS.SC8P //OSCILATOR 8pF LOAD CONFIGURE
#define __RTC_SC4P __RTC.RTC_CR.BITS.SC4P //OSCILATOR 4pF LOAD CONFIGURE
#define __RTC_SC2P __RTC.RTC_CR.BITS.SC2P //OSCILATOR 2pF LOAD CONFIGURE

#define __RTC_SR __RTC.RTC_SR.WORD //RTC STATUS REGISTER
#define __RTC_TIF __RTC.RTC_SR.BITS.TIF //TIME INVALID FLAG
#define __RTC_TOF __RTC.RTC_SR.BITS.TOF //TIME OVERFLOW FLAG
#define __RTC_TAF __RTC.RTC_SR.BITS.TAF //TIME ALARM FLAG
#define __RTC_TCE __RTC.RTC_SR.BITS.TCE //TIME  COUNTER ENABLE

#define __RTC_LR __RTC.RTC_LR.WORD //RTC LOCK REGISTER
#define __RTC_TCL __RTC.RTC_LR.BITS.TCL //TIME COMPENSATION CLOCK
#define __RTC_CRL __RTC.RTC_LR.BITS.CRL //CONTROL REGISTER LOCK
#define __RTC_SRL __RTC.RTC_LR.BITS.SRL //STATUS LOCK
#define __RTC_LRL __RTC.RTC_LR.BITS.LRL //LOCK REGISTER LOCK

#define __RTC_IER __RTC.RTC_IER.WORD //RTC INTERRUPT ENABLE REGISTER
#define __RTC_TIIE __RTC.RTC_IER.BITS.TIIE //TIME INVALID INTERRUPT ENABLE
#define __RTC_TOIE __RTC.RTC_IER.BITS.TOIE //TIME OVERFLOW INTERRUPT ENABLE
#define __RTC_TAIE __RTC.RTC_IER.BITS.TAIE //TIME ALARM INTERRUPT ENABLE
#define __RTC_TSIE __RTC.RTC_IER.BITS.TSIE //TIME SECONDS INTERRUPT ENABLE
#define __RTC_WPON __RTC.RTC_IER.BITS.WPON //WAKEUP PIN ON



#endif /* RTC_LIB_H_ */



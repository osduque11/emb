/*
 * SMC_LIB.h
 *
 *  Created on: 24/09/2017
 *      Author: Ricardo Andrés Velásquez Vélez
 */

#ifndef SMC_LIB_H_
#define SMC_LIB_H_

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t  			: 1;						// Reserved
		uint8_t AVLLS 		: 1;						// Allow Very-Low-Leakage Stop Mode
		uint8_t 		 	: 1;						// Reserved
		uint8_t ALLS      	: 1;						// Allow Low-Leakage Stop Mode
		uint8_t 		  	: 1;						// Reserved
		uint8_t AVLP	  	: 1;						// Allow Very-Low-Power Modes
		uint8_t	    		: 2;						// Reserved
	}BITS;
}__smc_pmprot_t;

enum{
	kSCM_SCM_NORMAL_STOP = 0x0000U,						// Normal Stop (STOP)
	kSCM_VERY_LOW_POWER_STOP = 0x0002U,					// Very-Low-Power Stop (VLPS)
	kSCM_LOW_LEAKAGE_STOP = 0x0003U,					// Low-Leakage Stop (LLS)
	kSCM_VERY_LOW_LEAKAGE_STOP = 0x0004U				// Very-Low-Leakage Stop (VLLSx)
};// __scm_pmctrl_stopm_t;

enum{
	kSCM__NORMAL_RUN = 0x0000U,							// Normal Run mode (RUN)
	kSCM_ = 0x0002U										// Very-Low-Power Run mode (VLPR)
};// __scm_pmctrl_runm_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t STOPM		: 3;						// Stop Mode Control
		uint8_t STOPA 		: 1;						// Stop Aborted
		uint8_t 		 	: 1;						// Reserved
		uint8_t RUNM      	: 2;						// Run Mode Control
		uint8_t 		  	: 1;						// Reserved
	}BITS;
}__smc_pmctrl_t;


typedef enum{
	kSCM_NORMAL_STOP = 0x0000U,							// STOP - Normal Stop mode
	kSCM_PSTOP1 = 0x0001U,								// PSTOP1 - Partial Stop with both system and bus clocks disabled
	kSCM_PSTOP2 = 0x0002U								// PSTOP2 - Partial Stop with system clock disabled and bus clock enabled
} __scm_stopctrl_pstopo_t;

typedef enum{
	kSCM_VLLS0 = 0x0000U,								// VLLS0
	kSCM_VLLS1 = 0x0001U,								// VLLS1
	kSCM_VLLS3 = 0x0003U								// VLLS3
} __scm_stopctrl_vllsm_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t VLLSM		: 3;						// Stop Mode Control
		uint8_t LPOPO 		: 1;						// Stop Aborted
		uint8_t 		 	: 1;						// Reserved
		uint8_t PORPO      	: 1;						// POR Power Option
		uint8_t PSTOPO	  	: 2;						// Partial Stop Option
	}BITS;
}__smc_stopctrl_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t RUN_MODE	: 1;						// RUN
		uint8_t STOP_MODE	: 1;						// STOP
		uint8_t VLPR_MODE 	: 1;						// VLPR
		uint8_t VLPW_MODE 	: 1;						// VLPW
		uint8_t VLPS_MODE  	: 1;						// VLPS
		uint8_t LLS_MODE  	: 1;						// LLS
		uint8_t VLLS_MODE  	: 1;						// VLLS
		uint8_t	    		: 1;						// Reserved
	}BITS;
}__smc_pmstat_t;


typedef struct{
	volatile __smc_pmprot_t SMC_PMPROT; 				// 0x00
	volatile __smc_pmctrl_t SMC_PMCTRL; 				// 0x01
	volatile __smc_stopctrl_t SMC_STOPCTRL; 			// 0x02
	volatile __smc_stopctrl_t SMC_PMSTAT; 				// 0x03
} __smc_t;

#define sSMC (*((__smc_t *)(0x4007E000)))  			// General structure to access the System Mode Controller

// SMC_PMPROT
#define rSMC_PMPROT sSMC.SMC_PMPROT.BYTE				// Power Mode Protection register (SMC_PMPROT)
#define bSMC_AVLP sSMC.SMC_PMPROT.BITS.AVLP			// Allow Very-Low-Power Modes
#define bSMC_ALLS sSMC.SMC_PMPROT.BITS.ALLS			// Allow Low-Leakage Stop Mode
#define bSMC_AVLLS sSMC.SMC_PMPROT.BITS.AVLLS			// Allow Very-Low-Leakage Stop Mode

// SMC_PMCTRL
#define rSMC_PMCTRL sSMC.SMC_PMCTRL.BYTE				// Power Mode Control register (SMC_PMCTRL)
#define bSMC_RUNM __sSMC.SMC_PMCTRL.BITS.RUNM			// Run Mode Control
#define bSMC_STOPA __sSMC.SMC_PMCTRL.BITS.STOPA		// Stop Aborted
#define bSMC_STOPM __sSMC.SMC_PMCTRL.BITS.STOPM		// Stop Mode Control

// SMC_STOPCTRL
#define rSMC_STOPCTRL sSMC.SMC_STOPCTRL.BYTE			// Stop Control Register (SMC_STOPCTRL)
#define bSMC_PSTOPO sSMC.SMC_STOPCTRL.BITS.PSTOPO		// Partial Stop Option
#define bSMC_PORPO sSMC.SMC_STOPCTRL.BITS.PORPO		// POR Power Option
#define bSMC_LPOPO sSMC.SMC_STOPCTRL.BITS.LPOPO		// LPO Power Option
#define bSMC_VLLSM sSMC.SMC_STOPCTRL.BITS.VLLSM		// VLLS Mode Control


// SMC_PMSTAT
#define rSMC_PMSTAT sSMC.SMC_PMSTAT.BYTE				// Power Mode Status register (SMC_PMSTAT)
#define bSMC_RUN_MODE sSMC.SMC_PMSTAT.BITS.RUN_MODE	// Current power mode is RUN
#define bSMC_STOP_MODE sSMC.SMC_PMSTAT.BITS.STOP_MODE	// Current power mode is STOP
#define bSMC_VLPR_MODE sSMC.SMC_PMSTAT.BITS.VLPR_MODE	// Current power mode is VLPR
#define bSMC_VLPW_MODE sSMC.SMC_PMSTAT.BITS.VLPW_MODE	// Current power mode is VLPW
#define bSMC_VLPS_MODE sSMC.SMC_PMSTAT.BITS.VLPS_MODE	// Current power mode is VLPS
#define bSMC_LLS_MODE sSMC.SMC_PMSTAT.BITS.LLS_MODE	// Current power mode is LLS
#define bSMC_VLLS_MODE sSMC.SMC_PMSTAT.BITS.VLLS_MODE	// Current power mode is VLLS


#endif /* SMC_LIB_H_ */

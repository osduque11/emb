/*
 * DMA_LIB.h
 *
 *  Created on: 3/10/2017
 *      Author: Ricardo
 */

#ifndef DMA_LIB_H_
#define DMA_LIB_H_

enum {
	kDMA_SAR_DAR_VALUE_000=0x00000000UL,
	kDMA_SAR_DAR_VALUE_1FF=0x1FF00000UL,
	kDMA_SAR_DAR_VALUE_200=0x20000000UL,
	kDMA_SAR_DAR_VALUE_400=0x40000000UL
};

typedef  uint32_t __dma_sar_t;

typedef  uint32_t __dma_dar_t;



typedef union{
	unsigned long WORD;
	struct{
		uint32_t BCR		: 24;								// Byte Counter Register
		uint32_t DONE	 	: 1;								// Transaction DONE
		uint32_t BSY	 	: 1;								// Busy
		uint32_t REQ        : 1;								// Request
		uint32_t 		  	: 1;								// Reserved
		uint32_t BED	  	: 1;								// Bus Error on Destination
		uint32_t BES		: 1;								// Bus Error on Source
		uint32_t CE			: 1;								// Configuration Error
	}BITS;
}__dma_dsr_bcr_t;

enum{
	kDMA_SSIZE_32BIT,
	kDMA_SSIZE_8BIT,
	kDMA_SSIZE_16BIT
};

enum{
	kDMA_DSIZE_32BIT,
	kDMA_DSIZE_8BIT,
	kDMA_DSIZE_16BIT
};

enum{
	kDMA_SMOD_DISABLED,
	kDMA_SMOD_16_BYTES_BUFFER,
	kDMA_SMOD_32_BYTES_BUFFER,
	kDMA_SMOD_64_BYTES_BUFFER,
	kDMA_SMOD_128_BYTES_BUFFER,
	kDMA_SMOD_256_BYTES_BUFFER,
	kDMA_SMOD_512_BYTES_BUFFER,
	kDMA_SMOD_1024_BYTES_BUFFER,
	kDMA_SMOD_2048_BYTES_BUFFER,
	kDMA_SMOD_4096_BYTES_BUFFER,
	kDMA_SMOD_8192_BYTES_BUFFER,
	kDMA_SMOD_16384_BYTES_BUFFER,
	kDMA_SMOD_32768_BYTES_BUFFER,
	kDMA_SMOD_65536_BYTES_BUFFER,
	kDMA_SMOD_131072_BYTES_BUFFER,
	kDMA_SMOD_262144_BYTES_BUFFER,
};

enum{
	kDMA_DMOD_DISABLED,
	kDMA_DMOD_16_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_32_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_64_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_128_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_256_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_512_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_1024_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_2048_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_4096_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_8192_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_16384_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_32768_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_65536_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_131072_BYTES_CIRCULAR_BUFFER,
	kDMA_DMOD_262144_BYTES_CIRCULAR_BUFFER,
};

enum{
	kDMA_LINKCC_NO_LINK,
	kDMA_LINKCC_LCH1_CYCLE_STEAL_FOLLOW_LCH2_BCR_0,
	kDMA_LINKCC_LCH1_CYCLE_STEAL,
	kDMA_LINKCC_LCH1_BCR_0
};

enum{
	kDMA_LCHn_CHANNEL_0,
	kDMA_LCHn_CHANNEL_1,
	kDMA_LCHn_CHANNEL_2,
	kDMA_LCHn_CHANNEL_3
};

typedef union{
	unsigned long WORD;
	struct{
		uint32_t LCH2		: 2;								// Link Channel 2
		uint32_t LCH1	 	: 2;								// Link Channel 1
		uint32_t LINKCC	 	: 2;								// Link Channel Control
		uint32_t 	        : 1;								// Request
		uint32_t D_REQ	  	: 1;								// Disable Request
		uint32_t DMOD	  	: 4;								// Bus Error on Destination
		uint32_t SMOD		: 4;								// Source Address Modulo
		uint32_t START		: 1;								// Start Transferr
		uint32_t DSIZE		: 2;								// Destination Size
		uint32_t DINC		: 1;								// Destination Increment
		uint32_t SSIZE		: 2;								// Source Size
		uint32_t SINC		: 1;								// Source Increment
		uint32_t EADREQ		: 1;								// Enable asynchronous DMA requests
		uint32_t 			: 4;								// reserved
		uint32_t AA			: 1;								// Auto-Align
		uint32_t CS			: 1;								// Cycle Steal
		uint32_t ERQ		: 1;								// Enable Peripheral Request
		uint32_t EINT		: 1;								// Enable Interrupt on Completion of Transfer
	}BITS;
}__dma_dcr_t;

typedef struct{
	volatile __dma_sar_t DMA_SAR0; 								// 0x8100 Source Address Register (DMA_SAR0)
	volatile __dma_dar_t DMA_DAR0;								// 0x8104 Destination Address Register (DMA_DAR0)
	volatile __dma_dsr_bcr_t DMA_DSR_BCR0;						// 0x8108 DMA Status Register / Byte Count Register	(DMA_DSR_BCR0)
	volatile __dma_dcr_t DMA_DCR0; 								// 0x810C DMA Control Register (DMA_DCR0)
	volatile __dma_sar_t DMA_SAR1; 								// 0x8110 Source Address Register (DMA_SAR1)
	volatile __dma_dar_t DMA_DAR1;								// 0x8114 Destination Address Register (DMA_DAR1)
	volatile __dma_dsr_bcr_t DMA_DSR_BCR1;						// 0x8118 DMA Status Register / Byte Count Register	(DMA_DSR_BCR1)
	volatile __dma_dcr_t DMA_DCR1; 								// 0x811C DMA Control Register (DMA_DCR1)
	volatile __dma_sar_t DMA_SAR2; 								// 0x8120 Source Address Register (DMA_SAR2)
	volatile __dma_dar_t DMA_DAR2;								// 0x8124 Destination Address Register (DMA_DAR2)
	volatile __dma_dsr_bcr_t DMA_DSR_BCR2;						// 0x8128 DMA Status Register / Byte Count Register	(DMA_DSR_BCR2)
	volatile __dma_dcr_t DMA_DCR2; 								// 0x812C DMA Control Register (DMA_DCR2)
	volatile __dma_sar_t DMA_SAR3; 								// 0x8130 Source Address Register (DMA_SAR3)
	volatile __dma_dar_t DMA_DAR3;								// 0x8134 Destination Address Register (DMA_DAR3)
	volatile __dma_dsr_bcr_t DMA_DSR_BCR3;						// 0x8138 DMA Status Register / Byte Count Register	(DMA_DSR_BCR3)
	volatile __dma_dcr_t DMA_DCR3; 								// 0x813C DMA Control Register (DMA_DCR3)
} __dma_t;

#define sDMA (*((__dma_t *)(0x40008100)))						// DMA General Structure

#define rDMA_SAR0 sDMA.DMA_SAR0 								// Source Address Register (DMA_SAR0)

#define rDMA_DAR0 sDMA.DMA_DAR0									// Destination Address Register (DMA_DAR0)

#define rDMA_DSR_BCR0 sDMA.DMA_DSR_BCR0.WORD					// DMA Status Register / Byte Count Register (DMA_DSR_BCR0)
#define bDMA_CH0_BCR sDMA.DMA_DSR_BCR0.BITS.BCR					// Byte Counter Register
#define bDMA_CH0_DONE sDMA.DMA_DSR_BCR0.BITS.DONE				// Transaction DONE
#define bDMA_CH0_BSY sDMA.DMA_DSR_BCR0.BITS.BSY					// Busy
#define bDMA_CH0_REQ sDMA.DMA_DSR_BCR0.BITS.REQ					// Request
#define bDMA_CH0_BED sDMA.DMA_DSR_BCR0.BITS.BED					// Bus Error on Destination
#define bDMA_CH0_BES sDMA.DMA_DSR_BCR0.BITS.BES					// Bus Error on Source
#define bDMA_CH0_CE sDMA.DMA_DSR_BCR0.BITS.CE					// Configuration Error

#define rDMA_DCR0 sDMA.DMA_DCR0.WORD 							// DMA Control Register (DMA_DCR0)
#define bDMA_CH0_LCH2 sDMA.DMA_DCR0.BITS.LCH2					// Link Channel 2
#define bDMA_CH0_LCH1 sDMA.DMA_DCR0.BITS.LCH1					// Link Channel 1
#define bDMA_CH0_LINKCC sDMA.DMA_DCR0.BITS.LINKCC				// Link Channel Control
#define bDMA_CH0_D_REQ sDMA.DMA_DCR0.BITS.D_REQ					// Disable Request
#define bDMA_CH0_DMOD sDMA.DMA_DCR0.BITS.DMOD					// Bus Error on Destination
#define bDMA_CH0_SMOD sDMA.DMA_DCR0.BITS.SMOD					// Source Address Modulo
#define bDMA_CH0_START sDMA.DMA_DCR0.BITS.START					// Start Transferr
#define bDMA_CH0_DSIZE sDMA.DMA_DCR0.BITS.DSIZE					// Destination Size
#define bDMA_CH0_DINC sDMA.DMA_DCR0.BITS.DINC					// Destination Increment
#define bDMA_CH0_SSIZE sDMA.DMA_DCR0.BITS.SSIZE					// Source Size
#define bDMA_CH0_SINC sDMA.DMA_DCR0.BITS.SINC					// Source Increment
#define bDMA_CH0_EADREQ sDMA.DMA_DCR0.BITS.EADREQ				// Enable asynchronous DMA requests
#define bDMA_CH0_AA sDMA.DMA_DCR0.BITS.AA						// Auto-Align
#define bDMA_CH0_CS sDMA.DMA_DCR0.BITS.CS						// Cycle Steal
#define bDMA_CH0_ERQ sDMA.DMA_DCR0.BITS.ERQ						// Enable Peripheral Request
#define bDMA_CH0_EINT sDMA.DMA_DCR0.BITS.EINT					// Enable Interrupt on Completion of Transfer

#define rDMA_SAR1 sDMA.DMA_SAR1 								// Source Address Register (DMA_SAR1)

#define rDMA_DAR1 sDMA.DMA_DAR1									// Destination Address Register (DMA_DAR1)

#define rDMA_DSR_BCR1 sDMA.DMA_DSR_BCR1.WORD					// DMA Status Register / Byte Count Register (DMA_DSR_BCR1)
#define bDMA_CH1_BCR sDMA.DMA_DSR_BCR1.BITS.BCR					// Byte Counter Register
#define bDMA_CH1_DONE sDMA.DMA_DSR_BCR1.BITS.DONE				// Transaction DONE
#define bDMA_CH1_BSY sDMA.DMA_DSR_BCR1.BITS.BSY					// Busy
#define bDMA_CH1_REQ sDMA.DMA_DSR_BCR1.BITS.REQ					// Request
#define bDMA_CH1_BED sDMA.DMA_DSR_BCR1.BITS.BED					// Bus Error on Destination
#define bDMA_CH1_BES sDMA.DMA_DSR_BCR1.BITS.BES					// Bus Error on Source
#define bDMA_CH1_CE sDMA.DMA_DSR_BCR1.BITS.CE					// Configuration Error

#define rDMA_DCR1 sDMA.DMA_DCR1.WORD 							// DMA Control Register (DMA_DCR1)
#define bDMA_CH1_LCH2 sDMA.DMA_DCR1.BITS.LCH2					// Link Channel 2
#define bDMA_CH1_LCH1 sDMA.DMA_DCR1.BITS.LCH1					// Link Channel 1
#define bDMA_CH1_LINKCC sDMA.DMA_DCR1.BITS.LINKCC				// Link Channel Control
#define bDMA_CH1_D_REQ sDMA.DMA_DCR1.BITS.D_REQ					// Disable Request
#define bDMA_CH1_DMOD sDMA.DMA_DCR1.BITS.DMOD					// Bus Error on Destination
#define bDMA_CH1_SMOD sDMA.DMA_DCR1.BITS.SMOD					// Source Address Modulo
#define bDMA_CH1_START sDMA.DMA_DCR1.BITS.START					// Start Transferr
#define bDMA_CH1_DSIZE sDMA.DMA_DCR1.BITS.DSIZE					// Destination Size
#define bDMA_CH1_DINC sDMA.DMA_DCR1.BITS.DINC					// Destination Increment
#define bDMA_CH1_SSIZE sDMA.DMA_DCR1.BITS.SSIZE					// Source Size
#define bDMA_CH1_SINC sDMA.DMA_DCR1.BITS.SINC					// Source Increment
#define bDMA_CH1_EADREQ sDMA.DMA_DCR1.BITS.EADREQ				// Enable asynchronous DMA requests
#define bDMA_CH1_AA sDMA.DMA_DCR1.BITS.AA						// Auto-Align
#define bDMA_CH1_CS sDMA.DMA_DCR1.BITS.CS						// Cycle Steal
#define bDMA_CH1_ERQ sDMA.DMA_DCR1.BITS.ERQ						// Enable Peripheral Request
#define bDMA_CH1_EINT sDMA.DMA_DCR1.BITS.EINT					// Enable Interrupt on Completion of Transfer

#define rDMA_SAR2 sDMA.DMA_SAR2 								// Source Address Register (DMA_SAR2)

#define rDMA_DAR2 sDMA.DMA_DAR2									// Destination Address Register (DMA_DAR2)

#define rDMA_DSR_BCR2 sDMA.DMA_DSR_BCR2.WORD					// DMA Status Register / Byte Count Register (DMA_DSR_BCR2)
#define bDMA_CH2_BCR sDMA.DMA_DSR_BCR2.BITS.BCR					// Byte Counter Register
#define bDMA_CH2_DONE sDMA.DMA_DSR_BCR2.BITS.DONE				// Transaction DONE
#define bDMA_CH2_BSY sDMA.DMA_DSR_BCR2.BITS.BSY					// Busy
#define bDMA_CH2_REQ sDMA.DMA_DSR_BCR2.BITS.REQ					// Request
#define bDMA_CH2_BED sDMA.DMA_DSR_BCR2.BITS.BED					// Bus Error on Destination
#define bDMA_CH2_BES sDMA.DMA_DSR_BCR2.BITS.BES					// Bus Error on Source
#define bDMA_CH2_CE sDMA.DMA_DSR_BCR2.BITS.CE					// Configuration Error

#define rDMA_DCR2 sDMA.DMA_DCR2.WORD 							// DMA Control Register (DMA_DCR2)
#define bDMA_CH2_LCH2 sDMA.DMA_DCR2.BITS.LCH2					// Link Channel 2
#define bDMA_CH2_LCH1 sDMA.DMA_DCR2.BITS.LCH1					// Link Channel 1
#define bDMA_CH2_LINKCC sDMA.DMA_DCR2.BITS.LINKCC				// Link Channel Control
#define bDMA_CH2_D_REQ sDMA.DMA_DCR2.BITS.D_REQ					// Disable Request
#define bDMA_CH2_DMOD sDMA.DMA_DCR2.BITS.DMOD					// Bus Error on Destination
#define bDMA_CH2_SMOD sDMA.DMA_DCR2.BITS.SMOD					// Source Address Modulo
#define bDMA_CH2_START sDMA.DMA_DCR2.BITS.START					// Start Transferr
#define bDMA_CH2_DSIZE sDMA.DMA_DCR2.BITS.DSIZE					// Destination Size
#define bDMA_CH2_DINC sDMA.DMA_DCR2.BITS.DINC					// Destination Increment
#define bDMA_CH2_SSIZE sDMA.DMA_DCR2.BITS.SSIZE					// Source Size
#define bDMA_CH2_SINC sDMA.DMA_DCR2.BITS.SINC					// Source Increment
#define bDMA_CH2_EADREQ sDMA.DMA_DCR2.BITS.EADREQ				// Enable asynchronous DMA requests
#define bDMA_CH2_AA sDMA.DMA_DCR2.BITS.AA						// Auto-Align
#define bDMA_CH2_CS sDMA.DMA_DCR2.BITS.CS						// Cycle Steal
#define bDMA_CH2_ERQ sDMA.DMA_DCR2.BITS.ERQ						// Enable Peripheral Request
#define bDMA_CH2_EINT sDMA.DMA_DCR2.BITS.EINT					// Enable Interrupt on Completion of Transfer

#define rDMA_SAR3 sDMA.DMA_SAR3 								// Source Address Register (DMA_SAR3)

#define rDMA_DAR3 sDMA.DMA_DAR3									// Destination Address Register (DMA_DAR3)

#define rDMA_DSR_BCR3 sDMA.DMA_DSR_BCR3.WORD					// DMA Status Register / Byte Count Register (DMA_DSR_BCR3)
#define bDMA_CH3_BCR sDMA.DMA_DSR_BCR3.BITS.BCR					// Byte Counter Register
#define bDMA_CH3_DONE sDMA.DMA_DSR_BCR3.BITS.DONE				// Transaction DONE
#define bDMA_CH3_BSY sDMA.DMA_DSR_BCR3.BITS.BSY					// Busy
#define bDMA_CH3_REQ sDMA.DMA_DSR_BCR3.BITS.REQ					// Request
#define bDMA_CH3_BED sDMA.DMA_DSR_BCR3.BITS.BED					// Bus Error on Destination
#define bDMA_CH3_BES sDMA.DMA_DSR_BCR3.BITS.BES					// Bus Error on Source
#define bDMA_CH3_CE sDMA.DMA_DSR_BCR3.BITS.CE					// Configuration Error

#define rDMA_DCR3 sDMA.DMA_DCR3.WORD							// DMA Control Register (DMA_DCR3)
#define bDMA_CH3_LCH2 sDMA.DMA_DCR3.BITS.LCH2					// Link Channel 2
#define bDMA_CH3_LCH1 sDMA.DMA_DCR3.BITS.LCH1					// Link Channel 1
#define bDMA_CH3_LINKCC sDMA.DMA_DCR3.BITS.LINKCC				// Link Channel Control
#define bDMA_CH3_D_REQ sDMA.DMA_DCR3.BITS.D_REQ					// Disable Request
#define bDMA_CH3_DMOD sDMA.DMA_DCR3.BITS.DMOD					// Bus Error on Destination
#define bDMA_CH3_SMOD sDMA.DMA_DCR3.BITS.SMOD					// Source Address Modulo
#define bDMA_CH3_START sDMA.DMA_DCR3.BITS.START					// Start Transferr
#define bDMA_CH3_DSIZE sDMA.DMA_DCR3.BITS.DSIZE					// Destination Size
#define bDMA_CH3_DINC sDMA.DMA_DCR3.BITS.DINC					// Destination Increment
#define bDMA_CH3_SSIZE sDMA.DMA_DCR3.BITS.SSIZE					// Source Size
#define bDMA_CH3_SINC sDMA.DMA_DCR3.BITS.SINC					// Source Increment
#define bDMA_CH3_EADREQ sDMA.DMA_DCR3.BITS.EADREQ				// Enable asynchronous DMA requests
#define bDMA_CH3_AA sDMA.DMA_DCR3.BITS.AA						// Auto-Align
#define bDMA_CH3_CS sDMA.DMA_DCR3.BITS.CS						// Cycle Steal
#define bDMA_CH3_ERQ sDMA.DMA_DCR3.BITS.ERQ						// Enable Peripheral Request
#define bDMA_CH3_EINT sDMA.DMA_DCR3.BITS.EINT					// Enable Interrupt on Completion of Transfer
#endif /* DMA_LIB_H_ */

/*
 * MCM_LIB.h
 *
 *  Created on: 11/10/2017
 *      Author: Ricardo Andrés Velásquez Vélez
 */

#ifndef MCM_LIB_H_
#define MCM_LIB_H_

typedef union{
	uint16_t SHORT;
	struct{
		uint32_t ASC		: 8;								// AXBS Slave Configuration
		uint32_t 		 	: 8;								// Reserved
	}BITS;
}__mcm_plasc_t;

typedef union{
	uint16_t SHORT;
	struct{
		uint32_t AMC		: 8;								// AXBS Master Configuration
		uint32_t 		 	: 8;								// Reserved
	}BITS;
}__mcm_plamc_t;

typedef union{
	uint32_t WORD;
	struct{
		uint32_t 			: 9;								// Reserved
		uint32_t ARB	 	: 1;								// Arbitration select
		uint32_t CFCC	 	: 1;								// Clear Flash Controller Cache
		uint32_t DFCDA      : 1;								// Disable Flash Controller Data Caching
		uint32_t DFCIC		: 1;								// Disable Flash Controller Instruction Caching
		uint32_t DFCC	  	: 1;								// Disable Flash Controller Cache
		uint32_t EFDS		: 1;								// Enable Flash Data Speculation
		uint32_t DFCS		: 1;								// Disable Flash Controller Speculation
		uint32_t ESFC		: 1;								// Enable Stalling Flash Controller
		uint32_t 			: 15;								// Reserved
	}BITS;
}__mcm_placr_t;

typedef union{
	uint32_t WORD;
	struct{
		uint32_t CPOREQ	 	: 1;								// Compute Operation Request
		uint32_t CPOACK	 	: 1;								// Compute Operation Acknowledge
		uint32_t CPOWOI     : 1;								// Compute Operation Wake-up on Interrupt
		uint32_t 			: 29;								// Reserved
	}BITS;
}__mcm_cpo_t;

typedef struct{
	volatile __mcm_plasc_t MCM_PLASC; 							// 0x3008 Crossbar Switch (AXBS) Slave Configuration (MCM_PLASC)
	volatile __mcm_plamc_t MCM_PLAMC;							// 0x300A Crossbar Switch (AXBS) Master Configuration (MCM_PLAMC)
	volatile __mcm_placr_t MCM_PLACR; 							// 0x300C Platform Control Register (MCM_PLACR)
	volatile uint32_t RESERVED[12];								// 0x3010 - 0x303C
	volatile __mcm_cpo_t MCM_CPO; 								// 0x3040 Compute Operation Control Register (MCM_CPO)
} __mcm_t;

#define sMCM (*((__mcm_t *)(0xF0003008)))						// MCM General Structure 2

#define rMCM_PLASC sDMA.MCM_PLASC.SHORT 						// Crossbar Switch (AXBS) Slave Configuration (MCM_PLASC)
#define bMCM_ASC sDMA.MCM_PLASC.BITS.ASC						// AXBS Slave Configuration

#define rMCM_PLAMC sDMA.MCM_PLAMC.SHORT 						// Crossbar Switch (AXBS) Master Configuration (MCM_PLAMC)
#define bMCM_AMC sDMA.MCM_PLAMC.BITS.AMC						// AXBS Master Configuration

#define rMCM_PLACR sDMA.MCM_PLACR.WORD 							// Platform Control Register (MCM_PLACR)
#define bMCM_ARB sDMA.MCM_PLACR.BITS.ARB						// Arbitration select
#define bMCM_CFCC sDMA.MCM_PLACR.BITS.CFCC						// Clear Flash Controller Cache
#define bMCM_DFCDA sDMA.MCM_PLACR.BITS.DFCDA					// Disable Flash Controller Data Caching
#define bMCM_DFCIC sDMA.MCM_PLACR.BITS.DFCIC					// Disable Flash Controller Instruction Caching
#define bMCM_DFCC sDMA.MCM_PLACR.BITS.DFCC						// Disable Flash Controller Cache
#define bMCM_EFDS sDMA.MCM_PLACR.BITS.EFDS						// Enable Flash Data Speculation
#define bMCM_DFCS sDMA.MCM_PLACR.BITS.DFCS						// Disable Flash Controller Speculation
#define bMCM_ESFC sDMA.MCM_PLACR.BITS.ESFC						// Enable Stalling Flash Controller

#define rMCM_CPO sDMA.MCM_CPO.WORD 								// Compute Operation Control Register (MCM_CPO)
#define bMCM_CPOREQ sDMA.MCM_CPO.BITS.CPOREQ					// Compute Operation Request
#define bMCM_CPOACK sDMA.MCM_CPO.BITS.CPOACK					// Compute Operation Acknowledge
#define bMCM_CPOWOI sDMA.MCM_CPO.BITS.CPOWOI					// Compute Operation Wake-up on Interrupt


#endif /* MCM_LIB_H_ */

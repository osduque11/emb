/*
 * USB_LIB.h UNIVERSAL SERIAL BUS
 *
 *  Created on: 24/09/2017
 *      Author: Cesar Daniel de la Cruz Ruiz
 *		
 */

#ifndef USB_LIB_H_
#define USB_LIB_H_

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t ID			: 6;		//
		uint8_t  			: 2;		//
	}BITS;
}__usb_perid_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t ND			: 6;		//
		uint8_t  			: 2;		//
	}BITS;
}__usb_idcomp_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t REV		: 8;		//
	}BITS;
}__usb_rev_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t IEHOST		: 1;		//
		uint8_t  			: 7;		//
	}BITS;
}__usb_addinfo_t;




typedef struct{
	volatile __usb_perid_t USB0_PERID; 		// 0x2000
	volatile uint8_t RESERVED0; 			// 0x2002
	volatile __usb_idcomp_t USB0_IDCOMP;	// 0x2004
	volatile uint8_t RESERVED1; 			// 0x2006
	volatile __usb_rev_t USB0_REV;			// 0x2008
	volatile uint8_t RESERVED2; 			// 0x200A
	volatile __usb_addinfo_t USB0_ADDINFO;	// 0x200C
} __usb1_t;

#define __USB1 (*((__usb1_t *)(0x40072000)))

#define __USB0_PERID __USB1.USB0_PERID.BYTE
#define __USB_ID __USB1.USB0_PERID.BITS.ID	

#define __USB0_IDCOMP __USB1.USB0_IDCOMP.BYTE
#define __USB_NID __USB1.USB0_IDCOMP.BITS.NID

#define __USB0_REV __USB1.USB0_REV.BYTE

#define __USB0_ADDINFO __USB1.USB0_ADDINFO.BYTE
#define __USB_IEHOST __USB1.USB0_ADDINFO.BITS.IEHOST


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t USBRST		: 1;		//
		uint8_t ERROR		: 1;		//
		uint8_t SOFTOK		: 1;		//
		uint8_t TOKDNE		: 1;		//
		uint8_t SLEEP		: 1;		//
		uint8_t RESUME		: 1;		//
		uint8_t 			: 1;		//
		uint8_t STALL		: 1;		//

	}BITS;
}__usb_istat_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t USBRSTEN	: 1;		//
		uint8_t ERROREN		: 1;		//
		uint8_t SOFTOKEN	: 1;		//
		uint8_t TOKDNEEN	: 1;		//
		uint8_t SLEEPEN		: 1;		//
		uint8_t RESUMEEN	: 1;		//
		uint8_t 			: 1;		//
		uint8_t STALLEN		: 1;		//

	}BITS;
}__usb_inten_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t PIDERR		: 1;		//
		uint8_t CRC5		: 1;		//
		uint8_t CRC16		: 1;		//
		uint8_t DFN8		: 1;		//
		uint8_t BTOERR		: 1;		//
		uint8_t DMAERR		: 1;		//
		uint8_t 			: 1;		//
		uint8_t BTSERR		: 1;		//

	}BITS;
}__usb_errstat_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t PIDERREN 	: 1;		//
		uint8_t CRC5EOFEN	: 1;		//
		uint8_t CRC16EN		: 1;		//
		uint8_t DFN8EN		: 1;		//
		uint8_t BTOERREN	: 1;		//
		uint8_t DMAERREN	: 1;		//
		uint8_t 			: 1;		//
		uint8_t BTSERREN	: 1;		//

	}BITS;
}__usb_erren_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  	 		: 2;		//
		uint8_t ODD		    : 1;		//
		uint8_t TX			: 1;		//
		uint8_t ENDP		: 4;		//
	}BITS;
}__usb_stat_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t USBENSOFEN				: 1;		//
		uint8_t ODDRST					: 1;		//
		uint8_t RESUME					: 1;		//
		uint8_t 						: 2;		//
		uint8_t TXSUSPENDTOKENBUSY		: 1;		//
		uint8_t SE0						: 1;		//
		uint8_t JSTATE					: 1;		//

	}BITS;
}__usb_ctl_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t ADDR		: 7;		//
		uint8_t  			: 1;		//
	}BITS;
}__usb_addr_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  			: 1;		//
		uint8_t BDTBA		: 7;		//
	}BITS;
}__usb_bdtpage1_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t FRM		: 8;		// En este DUDA !
	}BITS;
}__usb_frmnuml_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t FRM	: 3;		//
		uint8_t 			: 5;		//
	}BITS;
}__usb_frmnumh_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t BTDBA		: 8;		//
	}BITS;
}__usb_bdtpage2_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t BTDBA		: 8;		//
	}BITS;
}__usb_bdtpage3_t;


typedef struct{
	volatile __usb_istat_t USB0_ISTAT; 		// 0x2080
	volatile uint8_t RESERVED3; 			// 0x2082
	volatile __usb_inten_t USB0_INTEN;		// 0x2084
	volatile uint8_t RESERVED4; 			// 0x2086
	volatile __usb_errstat_t USB0_ERRSTAT;	// 0x2088
	volatile uint8_t RESERVED5; 			// 0x208A
	volatile __usb_erren_t USB0_ERREN;	    // 0x208C
	volatile uint8_t RESERVED6; 			// 0x208E
	volatile __usb_stat_t USB0_STAT;	    // 0x2090
	volatile uint8_t RESERVED7; 			// 0x2092
	volatile __usb_ctl_t USB0_CTL;	        // 0x2094
	volatile uint8_t RESERVED8; 			// 0x2096
	volatile __usb_addr_t USB0_ADDR;	    // 0x2098
	volatile uint8_t RESERVED9; 			// 0x209A
	volatile __usb_bdtpage1_t USB0_BDTPAGE1;// 0x209C
	volatile uint8_t RESERVED10; 			// 0x209E
	volatile __usb_frmnuml_t USB0_FRMNUML;  // 0x20A0
	volatile uint8_t RESERVED11; 			// 0x20A2
	volatile __usb_frmnumh_t USB0_FRMNUMH;  // 0x20A4
	volatile uint8_t RESERVED12[4]; 		// 0x20A6 and 0x20AE
	volatile __usb_bdtpage2_t USB0_BDTPAGE2;// 0x20B0
	volatile uint8_t RESERVED13; 			// 0x20B2
	volatile __usb_bdtpage3_t USB0_BDTPAGE3;// 0x20B4
} __usb2_t;

#define __USB2 (*((__usb2_t *)(0x40072080)))

#define __USB0_ISTAT __USB2.USB0_ISTAT.BYTE
#define __USB_USBRST __USB2.USB0_ISTAT.BITS.USBRST
#define __USB_ERROR __USB2.USB0_ISTAT.BITS.ERROR
#define __USB_SOFTOK __USB2.USB0_ISTAT.BITS.SOFTOK
#define __USB_TOKDNE __USB2.USB0_ISTAT.BITS.TOKDNE
#define __USB_SLEEP __USB2.USB0_ISTAT.BITS.SLEEP
#define __USB_ISTAT_RESUME __USB2.USB0_ISTAT.BITS.RESUME
#define __USB_STALL __USB2.USB0_ISTAT.BITS.STALL

#define __USB0_INTEN __USB2.USB0_INTEN.BYTE
#define __USB_USBRSTEN __USB2.USB0_INTEN.BITS.USBRSTEN
#define __USB_ERROREN __USB2.USB0_INTEN.BITS.ERROREN
#define __USB_SOFTOKEN __USB2.USB0_INTEN.BITS.SOFTOKEN
#define __USB_TOKDNEEN __USB2.USB0_INTEN.BITS.TOKDNEEN
#define __USB_SLEEPEN __USB2.USB0_INTEN.BITS.SLEEPEN
#define __USB_RESUMEEN __USB2.USB0_INTEN.BITS.RESUMEEN
#define __USB_STALLEN __USB2.USB0_INTEN.BITS.STALLEN

#define __USB0_ERRSTAT __USB2.USB0_ERRSTAT.BYTE
#define __USB_PIDERR __USB2.USB0_ERRSTAT.BITS.PIDERR
#define __USB_CRC5 __USB2.USB0_ERRSTAT.BITS.CRC5
#define __USB_CRC16 __USB2.USB0_ERRSTAT.BITS.CRC16
#define __USB_DFN8 __USB2.USB0_ERRSTAT.BITS.DFN8
#define __USB_BTOERR __USB2.USB0_ERRSTAT.BITS.BTOERR
#define __USB_DMAERR __USB2.USB0_ERRSTAT.BITS.DMAERR
#define __USB_BTSERR __USB2.USB0_ERRSTAT.BITS.BTSERR

#define __USB0_ERREN __USB2.USB0_ERREN.BYTE
#define __USB_PIDERREN __USB2.USB0_ERRSTAT.BITS.PIDERREN
#define __USB_CRC5EOFEN __USB2.USB0_ERRSTAT.BITS.CRC5EOFEN
#define __USB_CRC16EN __USB2.USB0_ERRSTAT.BITS.CRC16EN
#define __USB_DFN8EN __USB2.USB0_ERRSTAT.BITS.DFN8EN
#define __USB_BTOERREN __USB2.USB0_ERRSTAT.BITS.BTOERREN
#define __USB_DMAERREN __USB2.USB0_ERRSTAT.BITS.DMAERREN
#define __USB_BTSERREN __USB2.USB0_ERRSTAT.BITS.BTSERREN

#define __USB0_STAT __USB2.USB0_STAT.BYTE
#define __USB_ODD __USB2.USB0_STAT.BITS.ODD
#define __USB_TX __USB2.USB0_STAT.BITS.TX
#define __USB_ENDP __USB2.USB0_STAT.BITS.ENDP

#define __USB0_CTL __USB2.USB0_CTL.BYTE
#define __USB_USBENSOFEN __USB2.USB0_CTL.BITS.USBENSOFEN
#define __USB_ODDRST __USB2.USB0_CTL.BITS.ODDRST
#define __USB_CTL_RESUME __USB2.USB0_CTL.BITS.RESUME
#define __USB_TXSUSPENDTOKENBUSY __USB2.USB0_CTL.BITS.TXSUSPENDTOKENBUSY
#define __USB_SE0 __USB2.USB0_CTL.BITS.SE0
#define __USB_JSTATE __USB2.USB0_CTL.BITS.JSTATE

#define __USB0_ADDR __USB2.USB0_ADDR.BYTE
#define __USB_ADDR __USB2.USB0_ADDR.BITS.ADDR

#define __USB0_BDTPAGE1 __USB2.USB0_BDTPAGE1.BYTE
#define __USB_BDTBA_PG1 __USB2.USB0_BDTPAGE1.BITS.BDTBA

#define __USB0_FRMNUML __USB2.USB0_FRMNUML.BYTE
#define __USB_FRML __USB2.USB0_FRMNUML.BITS.FRM

#define __USB0_FRMNUMH __USB2.USB0_FRMNUMH.BYTE
#define __USB_FRMH __USB2.USB0_FRMNUMH.BITS.FRM

#define __USB0_BDTPAGE2 __USB2.USB0_BDTPAGE2.BYTE
#define __USB_BDTBA_PG2 __USB2.USB0_BDTPAGE2.BITS.BDTBA

#define __USB0_BDTPAGE3 __USB2.USB0_BDTPAGE2.BYTE
#define __USB_BDTBA_PG3 __USB2.USB0_BDTPAGE3.BITS.BDTBA


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t EPHSHK		: 1;		//
		uint8_t EPSTALL		: 1;		//
		uint8_t EPTXEN		: 1;		//
		uint8_t EPRXEN		: 1;		//
		uint8_t EPCTLDIS	: 1;		//
		uint8_t 			: 3;		//
	}BITS;
}__usb_endpt_t;

typedef struct{
	volatile __usb_endpt_t USB0_ENDPT0; 	// 0x20C0
	volatile uint8_t RESERVED14; 			// 0x20C2
	volatile __usb_endpt_t USB0_ENDPT1;		// 0x20C4
	volatile uint8_t RESERVED15; 			// 0x20C6
	volatile __usb_endpt_t USB0_ENDPT2;		// 0x20C8
	volatile uint8_t RESERVED16; 			// 0x20CA
	volatile __usb_endpt_t USB0_ENDPT3;	    // 0x20CC
	volatile uint8_t RESERVED17; 			// 0x20CE
	volatile __usb_endpt_t USB0_ENDPT4;	    // 0x20D0
	volatile uint8_t RESERVED18; 			// 0x20D2
	volatile __usb_endpt_t USB0_ENDPT5;	    // 0x20D4
	volatile uint8_t RESERVED19; 			// 0x20D6
	volatile __usb_endpt_t USB0_ENDPT6;	    // 0x20D8
	volatile uint8_t RESERVED20; 			// 0x20DA
	volatile __usb_endpt_t USB0_ENDPT7;		// 0x20DC
	volatile uint8_t RESERVED21; 			// 0x20DE
	volatile __usb_endpt_t USB0_ENDPT8;     // 0x20E0
	volatile uint8_t RESERVED22; 			// 0x20E2
	volatile __usb_endpt_t USB0_ENDPT9;     // 0x20E4
	volatile uint8_t RESERVED23; 			// 0x20E6
	volatile __usb_endpt_t USB0_ENDPT10; 	// 0x20E8
	volatile uint8_t RESERVED24; 			// 0x20EA
	volatile __usb_endpt_t USB0_ENDPT11;	// 0x20EC
	volatile uint8_t RESERVED25; 			// 0x20EE
	volatile __usb_endpt_t USB0_ENDPT12;	// 0x20F0
	volatile uint8_t RESERVED26; 			// 0x20F2
	volatile __usb_endpt_t USB0_ENDPT13;	// 0x20F4
	volatile uint8_t RESERVED7; 			// 0x20F6
	volatile __usb_endpt_t USB0_ENDPT14;	// 0x20F8
	volatile uint8_t RESERVED28; 			// 0x20FA
	volatile __usb_endpt_t USB0_ENDPT15;	// 0x20FC
	
} __usb3_t;

#define __USB3 (*((__usb3_t *)(0x400720C0)))

#define __USB0_ENDPT0 __USB3.USB0_ENDPT0.BYTE
#define __ENDPT0_EPHSHK __USB3.USB0_ENDPT0.BITS.EPHSHK
#define __ENDPT0_EPSTALL __USB3.USB0_ENDPT0.BITS.EPSTALL
#define __ENDPT0_EPTXEN __USB3.USB0_ENDPT0.BITS.EPTXEN
#define __ENDPT0_EPRXEN __USB3.USB0_ENDPT0.BITS.EPRXEN
#define __ENDPT0_EPCTLDIS __USB3.USB0_ENDPT0.BITS.EPCTLDIS

#define __USB0_ENDPT1 __USB3.USB0_ENDPT1.BYTE
#define __ENDPT1_EPHSHK __USB3.USB0_ENDPT1.BITS.EPHSHK
#define __ENDPT1_EPSTALL __USB3.USB0_ENDPT1.BITS.EPSTALL
#define __ENDPT1_EPTXEN __USB3.USB0_ENDPT1.BITS.EPTXEN
#define __ENDPT1_EPRXEN __USB3.USB0_ENDPT1.BITS.EPRXEN
#define __ENDPT1_EPCTLDIS __USB3.USB0_ENDPT1.BITS.EPCTLDIS

#define __USB0_ENDPT2 __USB3.USB0_ENDPT2.BYTE
#define __ENDPT2_EPHSHK __USB3.USB0_ENDPT2.BITS.EPHSHK
#define __ENDPT2_EPSTALL __USB3.USB0_ENDPT2.BITS.EPSTALL
#define __ENDPT2_EPTXEN __USB3.USB0_ENDPT2.BITS.EPTXEN
#define __ENDPT2_EPRXEN __USB3.USB0_ENDPT2.BITS.EPRXEN
#define __ENDPT2_EPCTLDIS __USB3.USB0_ENDPT2.BITS.EPCTLDIS

#define __USB0_ENDPT3 __USB3.USB0_ENDPT3.BYTE
#define __ENDPT3_EPHSHK __USB3.USB0_ENDPT3.BITS.EPHSHK
#define __ENDPT3_EPSTALL __USB3.USB0_ENDPT3.BITS.EPSTALL
#define __ENDPT3_EPTXEN __USB3.USB0_ENDPT3.BITS.EPTXEN
#define __ENDPT3_EPRXEN __USB3.USB0_ENDPT3.BITS.EPRXEN
#define __ENDPT3_EPCTLDIS __USB3.USB0_ENDPT3.BITS.EPCTLDIS

#define __USB0_ENDPT4 __USB3.USB0_ENDPT4.BYTE
#define __ENDPT4_EPHSHK __USB3.USB0_ENDPT4.BITS.EPHSHK
#define __ENDPT4_EPSTALL __USB3.USB0_ENDPT4.BITS.EPSTALL
#define __ENDPT4_EPTXEN __USB3.USB0_ENDPT4.BITS.EPTXEN
#define __ENDPT4_EPRXEN __USB3.USB0_ENDPT4.BITS.EPRXEN
#define __ENDPT4_EPCTLDIS __USB3.USB0_ENDPT4.BITS.EPCTLDIS

#define __USB0_ENDPT5 __USB3.USB0_ENDPT5.BYTE
#define __ENDPT5_EPHSHK __USB3.USB0_ENDPT5.BITS.EPHSHK
#define __ENDPT5_EPSTALL __USB3.USB0_ENDPT5.BITS.EPSTALL
#define __ENDPT5_EPTXEN __USB3.USB0_ENDPT5.BITS.EPTXEN
#define __ENDPT5_EPRXEN __USB3.USB0_ENDPT5.BITS.EPRXEN
#define __ENDPT5_EPCTLDIS __USB3.USB0_ENDPT5.BITS.EPCTLDIS

#define __USB0_ENDPT6 __USB3.USB0_ENDPT6.BYTE
#define __ENDPT6_EPHSHK __USB3.USB0_ENDPT6.BITS.EPHSHK
#define __ENDPT6_EPSTALL __USB3.USB0_ENDPT6.BITS.EPSTALL
#define __ENDPT6_EPTXEN __USB3.USB0_ENDPT6.BITS.EPTXEN
#define __ENDPT6_EPRXEN __USB3.USB0_ENDPT6.BITS.EPRXEN
#define __ENDPT6_EPCTLDIS __USB3.USB0_ENDPT6.BITS.EPCTLDIS

#define __USB0_ENDPT7 __USB3.USB0_ENDPT7.BYTE
#define __ENDPT7_EPHSHK __USB3.USB0_ENDPT7.BITS.EPHSHK
#define __ENDPT7_EPSTALL __USB3.USB0_ENDPT7.BITS.EPSTALL
#define __ENDPT7_EPTXEN __USB3.USB0_ENDPT7.BITS.EPTXEN
#define __ENDPT7_EPRXEN __USB3.USB0_ENDPT7.BITS.EPRXEN
#define __ENDPT7_EPCTLDIS __USB3.USB0_ENDPT7.BITS.EPCTLDIS

#define __USB0_ENDPT8 __USB3.USB0_ENDPT8.BYTE
#define __ENDPT8_EPHSHK __USB3.USB0_ENDPT8.BITS.EPHSHK
#define __ENDPT8_EPSTALL __USB3.USB0_ENDPT8.BITS.EPSTALL
#define __ENDPT8_EPTXEN __USB3.USB0_ENDPT8.BITS.EPTXEN
#define __ENDPT8_EPRXEN __USB3.USB0_ENDPT8.BITS.EPRXEN
#define __ENDPT8_EPCTLDIS __USB3.USB0_ENDPT8.BITS.EPCTLDIS

#define __USB0_ENDPT9 __USB3.USB0_ENDPT9.BYTE
#define __ENDPT9_EPHSHK __USB3.USB0_ENDPT9.BITS.EPHSHK
#define __ENDPT9_EPSTALL __USB3.USB0_ENDPT9.BITS.EPSTALL
#define __ENDPT9_EPTXEN __USB3.USB0_ENDPT9.BITS.EPTXEN
#define __ENDPT9_EPRXEN __USB3.USB0_ENDPT9.BITS.EPRXEN
#define __ENDPT9_EPCTLDIS __USB3.USB0_ENDPT9.BITS.EPCTLDIS

#define __USB0_ENDPT10 __USB3.USB0_ENDPT10.BYTE
#define __ENDPT10_EPHSHK __USB3.USB0_ENDPT10.BITS.EPHSHK
#define __ENDPT10_EPSTALL __USB3.USB0_ENDPT10.BITS.EPSTALL
#define __ENDPT10_EPTXEN __USB3.USB0_ENDPT10.BITS.EPTXEN
#define __ENDPT10_EPRXEN __USB3.USB0_ENDPT10.BITS.EPRXEN
#define __ENDPT10_EPCTLDIS __USB3.USB0_ENDPT10.BITS.EPCTLDIS

#define __USB0_ENDPT11 __USB3.USB0_ENDPT11.BYTE
#define __ENDPT11_EPHSHK __USB3.USB0_ENDPT11.BITS.EPHSHK
#define __ENDPT11_EPSTALL __USB3.USB0_ENDPT11.BITS.EPSTALL
#define __ENDPT11_EPTXEN __USB3.USB0_ENDPT11.BITS.EPTXEN
#define __ENDPT11_EPRXEN __USB3.USB0_ENDPT11.BITS.EPRXEN
#define __ENDPT11_EPCTLDIS __USB3.USB0_ENDPT11.BITS.EPCTLDIS

#define __USB0_ENDPT12 __USB3.USB0_ENDPT12.BYTE
#define __ENDPT12_EPHSHK __USB3.USB0_ENDPT12.BITS.EPHSHK
#define __ENDPT12_EPSTALL __USB3.USB0_ENDPT12.BITS.EPSTALL
#define __ENDPT12_EPTXEN __USB3.USB0_ENDPT12.BITS.EPTXEN
#define __ENDPT12_EPRXEN __USB3.USB0_ENDPT12.BITS.EPRXEN
#define __ENDPT12_EPCTLDIS __USB3.USB0_ENDPT12.BITS.EPCTLDIS

#define __USB0_ENDPT13 __USB3.USB0_ENDPT13.BYTE
#define __ENDPT13_EPHSHK __USB3.USB0_ENDPT13.BITS.EPHSHK
#define __ENDPT13_EPSTALL __USB3.USB0_ENDPT13.BITS.EPSTALL
#define __ENDPT13_EPTXEN __USB3.USB0_ENDPT13.BITS.EPTXEN
#define __ENDPT13_EPRXEN __USB3.USB0_ENDPT13.BITS.EPRXEN
#define __ENDPT13_EPCTLDIS __USB3.USB0_ENDPT13.BITS.EPCTLDIS

#define __USB0_ENDPT14 __USB3.USB0_ENDPT14.BYTE
#define __ENDPT14_EPHSHK __USB3.USB0_ENDPT14.BITS.EPHSHK
#define __ENDPT14_EPSTALL __USB3.USB0_ENDPT14.BITS.EPSTALL
#define __ENDPT14_EPTXEN __USB3.USB0_ENDPT14.BITS.EPTXEN
#define __ENDPT14_EPRXEN __USB3.USB0_ENDPT14.BITS.EPRXEN
#define __ENDPT14_EPCTLDIS __USB3.USB0_ENDPT14.BITS.EPCTLDIS

#define __USB0_ENDPT15 __USB3.USB0_ENDPT15.BYTE
#define __ENDPT15_EPHSHK __USB3.USB0_ENDPT15.BITS.EPHSHK
#define __ENDPT15_EPSTALL __USB3.USB0_ENDPT15.BITS.EPSTALL
#define __ENDPT15_EPTXEN __USB3.USB0_ENDPT15.BITS.EPTXEN
#define __ENDPT15_EPRXEN __USB3.USB0_ENDPT15.BITS.EPRXEN
#define __ENDPT15_EPCTLDIS __USB3.USB0_ENDPT15.BITS.EPCTLDIS



typedef union{
	uint8_t BYTE;
	struct{
		uint8_t 		: 6;		//
		uint8_t PDE		: 1;		//
		uint8_t SUSP	: 1;		//
	}BITS;
}__usb_usbctrl_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t 			: 4;		//
		uint8_t DMPD		: 1;		//
		uint8_t 			: 1;		//
		uint8_t DPPD		: 1;		//
		uint8_t DPPU 		: 1;		//

	}BITS;
}__usb_observe_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t 					: 4;		//
		uint8_t DPPULLUPNONOTG		: 1;		//
		uint8_t 					: 3;		//
	}BITS;
}__usb_control_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t USB_RESUME_INT			: 1;		//
		uint8_t SYNC_DET				: 1;		//
		uint8_t USB_CLK_RECOVERY_INT	: 1;		//
		uint8_t 						: 2;		//
		uint8_t USBRESMEN				: 1;		//
		uint8_t 						: 1;		// Duda 
		uint8_t USBRESET				: 1;		//
	}BITS;
}__usb_usbtrc0_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t ADJ		: 8;		//
	}BITS;
}__usb_usbfrmadjust_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t KEEP_ALIVE_EN		: 1;		//
		uint8_t OWN_OVERRD_EN		: 1;		//
		uint8_t STOP_ACK_DLY_EN		: 1;		//
		uint8_t AHB_DLY_EN			: 1;		//
		uint8_t WAKE_INT_EN			: 1;		//
		uint8_t 					: 2;		// Duda 
		uint8_t WAKE_INT_STS		: 1;		//
	}BITS;
}__usb_keep_alive_ctrl_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t WAKE_ON_THIS	: 4;		//
		uint8_t WAKE_ENDPT		: 4;		//

	}BITS;
}__usb_keep_alive_wkctrl_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  						: 5;		//
		uint8_t RESTART_IFRTRIM_EN	 	: 1;		//
		uint8_t RESET_RESUME_ROUGH_EN	: 1;		//
		uint8_t CLOCK_RECOVER_EN		: 1;		//
	}BITS;
}__usb_clk_recover_ctrl_t;


typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  			: 1;		//
		uint8_t IRC_EN	 	: 1;		//
		uint8_t  			: 6;		//
	}BITS;
}__usb_clk_recover_irc_en_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  				: 4;		//
		uint8_t OVF_ERROR_EN 	: 1;		//
		uint8_t  				: 3;		//
	}BITS;
}__usb_clk_recover_int_en_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t  			: 4;		//
		uint8_t OVF_ERROR	: 1;		//
		uint8_t  			: 3;		//
	}BITS;
}__usb_clk_recover_int_status_t;



typedef struct{
	volatile __usb_usbctrl_t USB0_USBCTRL;  // 0x2100
	volatile uint8_t RESERVED29; 			// 0x2102
	volatile __usb_observe_t USB0_OBSERVE;	// 0x2104
	volatile uint8_t RESERVED30; 			// 0x2106
	volatile __usb_control_t USB0_CONTROL;	// 0x2108
	volatile uint8_t RESERVED31; 			// 0x210A
	volatile __usb_usbtrc0_t USB0_USBTRC0;  // 0x210C
	volatile uint8_t RESERVED32[2]; 		// 0x210E AND 0X2112
	volatile __usb_usbfrmadjust_t USB0_USBFRMADJUST;  // 0x2114
	volatile uint8_t RESERVED33[6]; 		// 0x2116 AND 0X2122
	volatile __usb_keep_alive_ctrl_t USB0_KEPP_ALIVE_CTRL;  // 0x2124
	volatile uint8_t RESERVED34; 			// 0x2126
	volatile __usb_keep_alive_wkctrl_t USB0_KEPP_ALIVE_WKCTRL;  // 0x2128
	volatile uint8_t RESERVED35[10]; 		// 0x212A AND 0X213E
	volatile __usb_clk_recover_ctrl_t USB0_CLK_RECOVER_CTRL;  // 0x2140
	volatile uint8_t RESERVED36; 			// 0x2142
	volatile __usb_clk_recover_irc_en_t USB0_CLK_RECOVER_IRC_EN;  // 0x2144
	volatile uint8_t RESERVED37[6]; 		// 0x2146 AND 0X2152
	volatile __usb_clk_recover_int_en_t USB0_CLK_RECOVER_INT_EN;  // 0x2154
	volatile uint8_t RESERVED38[2]; 		// 0x2156 AND 0X215A
	volatile __usb_clk_recover_int_status_t USB0_CLK_RECOVER_INT_STATUS;  // 0x215C

	
} __usb4_t;

#define __USB4 (*((__usb4_t *)(0x40072100)))

#define __USB0_USBCTRL __USB4.USB0_USBCTRL.BYTE
#define __USB_PDE __USB4.USB0_USBCTRL.BITS.PDE
#define __USB_SUSP __USB4.USB0_USBCTRL.BITS.SUSP

#define __USB0_OBSERVE __USB4.USB0_OBSERVE.BYTE
#define __USB_DMPD __USB4.USB0_OBSERVE.BITS.DMPD
#define __USB_DPPD __USB4.USB0_OBSERVE.BITS.DPPD
#define __USB_DPPU __USB4.USB0_OBSERVE.BITS.DPPU

#define __USB0_CONTROL __USB4.USB0_CONTROL.BYTE
#define __USB_DPPULLUPNONOTG __USB4.USB0_CONTROL.BITS.DPPULLUPNONOTG

#define __USB0_USBTRC0 __USB4.USB0_USBTRC0.BYTE
#define __USB_USB_RESUME_INT __USB4.USB0_USBTRC0.BITS.USB_RESUME_INT
#define __USB_SYNC_DET __USB4.USB0_USBTRC0.BITS.SYNC_DET
#define __USB_USB_CLK_RECOVERY_INT __USB4.USB0_USBTRC0.BITS.USB_CLK_RECOVERY_INT
#define __USB_USBRESMEN __USB4.USB0_USBTRC0.BITS.USBRESMEN
#define __USB_USBRESET __USB4.USB0_USBTRC0.BITS.USBRESET

#define __USB0_USBFRMADJUST __USB4.USB0_USBFRMADJUST.BYTE
#define __USB_ADJ __USB4.USB0_USBFRMADJUST.BITS.ADJ

#define __USB0_KEEP_ALIVE_CTRL __USB4.USB0_KEPP_ALIVE_CTRL.BYTE
#define __USB_KEEP_ALIVE_EN __USB4.USB0_KEPP_ALIVE_CTRL.BITS.KEEP_ALIVE_EN
#define __USB_OWN_OVERRD_EN __USB4.USB0_KEPP_ALIVE_CTRL.BITS.OWN_OVERRD_EN
#define __USB_STOP_ACK_DLY_EN __USB4.USB0_KEPP_ALIVE_CTRL.BITS.STOP_ACK_DLY_EN
#define __USB_AHB_DLY_EN __USB4.USB0_KEPP_ALIVE_CTRL.BITS.AHB_DLY_EN
#define __USB_WAKE_INT_EN __USB4.USB0_KEPP_ALIVE_CTRL.BITS.WAKE_INT_EN
#define __USB_WAKE_INT_STS __USB4.USB0_KEPP_ALIVE_CTRL.BITS.WAKE_INT_STS

#define __USB0_KEEP_ALIVE_WKCTRL __USB4.USB0_KEPP_ALIVE_WKCTRL.BYTE
#define __USB_WAKE_ON_THIS __USB4.USB0_KEPP_ALIVE_WKCTRL.BITS.WAKE_ON_THIS
#define __USB_WAKE_ENDPT __USB4.USB0_KEPP_ALIVE_WKCTRL.BITS.WAKE_ENDPT

#define __USB0_CLK_RECOVER_CTRL __USB4.USB0_CLK_RECOVER_CTRL.BYTE
#define __USB_RESTART_IFRTRIM_EN __USB4.USB0_CLK_RECOVER_CTRL.BITS.RESTART_IFRTRIM_EN
#define __USB_RESET_RESUME_ROUGH_EN __USB4.USB0_CLK_RECOVER_CTRL.BITS.RESET_RESUME_ROUGH_EN
#define __USB_CLOCK_RECOVER_EN __USB4.USB0_CLK_RECOVER_CTRL.BITS.CLOCK_RECOVER_EN

#define __USB0_CLK_RECOVER_IRC_EN __USB4.USB0_CLK_RECOVER_IRC_EN.BYTE
#define __USB_IRC_EN __USB4.USB0_CLK_RECOVER_IRC_EN.BITS.IRC_EN

#define __USB0_CLK_RECOVER_INT_EN __USB4.USB0_CLK_RECOVER_INT_EN.BYTE
#define __USB_OVF_ERROR_EN __USB4.USB0_CLK_RECOVER_INT_EN.BITS.OVF_ERROR_EN

#define __USB0_CLK_RECOVER_INT_STATUS __USB4.USB0_CLK_RECOVER_INT_STATUS.BYTE
#define __USB_OVF_ERROR __USB4.USB0_CLK_RECOVER_INT_STATUS.BITS.OVF_ERROR

#endif

/*
 * MCG_LIB.h
 *
 *  Created on: 13/10/2017
 *      Author: Ricardo Andrés Velásquez Vélez
 */

#ifndef MCG_LIB_H_
#define MCG_LIB_H_

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t IREFSTEN		: 1;								// Internal Reference Stop Enable
		uint8_t IRCLKEN	 		: 1;								// Internal Reference Clock Enable
		uint8_t 	 			: 4;								// Reserved
		uint8_t CLKS        	: 2;								// Clock Source Select
	}BITS;
}__mcg_c1_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t IRCS			: 1;								// Low-frequency Internal Reference Clock Select
		uint8_t 	 			: 1;								// Reserved
		uint8_t EREFS0	 		: 1;								// External Clock Source Select
		uint8_t HGO0        	: 1;								// Crystal Oscillator Operation Mode Select
		uint8_t RANGE0	  		: 2;								// External Clock Source Frequency Range Select
		uint8_t 	 			: 2;								// Reserved
	}BITS;
}__mcg_c2_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t 				: 1;								// Reserved
		uint8_t OSCINIT0	 	: 1;								// OSC Initialization Status
		uint8_t CLKST	 		: 1;								// Clock Mode Status
		uint8_t         		: 5;								// Reserved
	}BITS;
}__mcg_s_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t 				: 1;								// Reserved
		uint8_t FCRDIV	 		: 3;								// Low-frequency Internal Reference Clock Divider
		uint8_t 	 			: 4;								// Reserved
	}BITS;
}__mcg_sc_t;

typedef union{
	uint8_t BYTE;
	struct{
		uint8_t LIRC_DIV2		: 3;								// Second Low-frequency Internal Reference Clock Divider
		uint8_t 	 			: 3;								// Reserved
		uint8_t HIRCLPEN	 	: 1;								// High-frequency IRC Low-power Mode Enable
		uint8_t HIRCEN      	: 1;								// High-frequency IRC Enable
	}BITS;
}__mcg_mc_t;

typedef struct{
	volatile __mcg_c1_t MCG_C1; 									// 0x4000 MCG Control Register 1 (MCG_C1)
	volatile __mcg_c2_t MCG_C2;										// 0x4001 MCG Control Register 2 (MCG_C2)
	volatile uint8_t RESERVED1[4];									// 0x4002-0x4005 Reserved
	volatile __mcg_s_t MCG_S;										// 0x4006 MCG Status Register (MCG_S)
	volatile uint8_t RESERVED2;										// 0x4007 Reserved
	volatile __mcg_sc_t MCG_SC; 									// 0x4008 MCG Status and Control Register (MCG_SC)
	volatile uint8_t RESERVED3[9];									// 0x4009-0x4017 Reserved
	volatile __mcg_mc_t MCG_MC; 									// 0x4018 MCG Miscellaneous Control Register (MCG_MC)
} __mcg_t;

#define sMCG (*((__mcg_t *)(0x40064000)))							// MCG General Structure

#define rMCG_C1 sMCG.MCG_C1.BYTE 									// MCG Control Register 1 (MCG_C1)
#define bMCG_IREFSTEN sMCG.MCG_C1.BITS.IREFSTEN						// Internal Reference Stop Enable
#define bMCG_IRCLKEN sMCG.MCG_C1.BITS.IRCLKEN						// Internal Reference Clock Enable
#define bMCG_CLKS sMCG.MCG_C1.BITS.CLKS								// Clock Source Select

#define rMCG_C2 sMCG.MCG_C2.BYTE									// 0x4001 MCG Control Register 2 (MCG_C2)
#define bMCG_IRCS sMCG.MCG_C2.BITS.IRCS								// Low-frequency Internal Reference Clock Select
#define bMCG_EREFS0	sMCG.MCG_C2.BITS.EREFS0							// External Clock Source Select
#define bMCG_HGO0 sMCG.MCG_C2.BITS.HGO0								// Crystal Oscillator Operation Mode Select
#define bMCG_RANGE0 sMCG.MCG_C2.BITS.RANGE0							// External Clock Source Frequency Range Select

#define rMCG_S sMCG.MCG_S.BYTE										// 0x4006 MCG Status Register (MCG_S)
#define bMCG_OSCINIT0 sMCG.MCG_S.BITS.OSCINIT0						// OSC Initialization Status
#define bMCG_CLKST sMCG.MCG_S.BITS.CLKST							// Clock Mode Status

#define rMCG_SC sMCG.MCG_SC.BYTE 									// 0x4008 MCG Status and Control Register (MCG_SC)
#define bMCG_FCRDIV	 sMCG.MCG_SC.BITS.FCRDIV						// Low-frequency Internal Reference Clock Divider

#define rMCG_MC sMCG.MCG_MC.BYTE									// 0x4018 MCG Miscellaneous Control Register (MCG_MC)
#define bMCG_LIRC_DIV2	 sMCG.MCG_MC.BITS.LIRC_DIV2					// Second Low-frequency Internal Reference Clock Divider
#define bMCG_HIRCLPEN sMCG.MCG_MC.BITS.HIRCLP						// High-frequency IRC Low-power Mode Enable
#define bMCG_HIRCENV sMCG.MCG_MC.BITS.HIRCENV						// High-frequency IRC Enable

#endif /* MCG_LIB_H_ */

/*
 * CMP_LIB.h COMPARATOR MODULE
 *
 *  Created on: 22/09/2017
 *      Author: Sim�n Zapata Caro
 */

#ifndef CMP_LIB_H_
#define CMP_LIB_H_

typedef union{
	unsigned char WORD;
	struct{
		uint8_t HYSTCTR 			: 2;
		uint8_t 		 			: 2;
		uint8_t FILTER_CNT 			: 3;
		uint8_t 		 			: 1;			
	}BITS;
}__cmpx_cr0_t;

typedef union{
	unsigned char WORD;
	struct{
		uint8_t EN 					: 1;
		uint8_t OPE		 			: 1;
		uint8_t COS		 			: 1;
		uint8_t INV		 			: 1;		
		uint8_t PMODE	 			: 1;
		uint8_t TRIGM	 			: 1;		
		uint8_t WE		 			: 1;
		uint8_t SE		 			: 1;				
	}BITS;
}__cmpx_cr1_t;

//cmpx_fpr_t :FILT PER : 8 BITS (NO OLVIDAR EN LA ESTRUCTURA!!!)
typedef union{
	unsigned char WORD;
	struct{
		uint8_t COUT 					: 1;
		uint8_t CFF		 				: 1;
		uint8_t CFR		 				: 1;
		uint8_t IEF		 				: 1;		
		uint8_t IER	 					: 1;
		uint8_t 		 				: 1;		
		uint8_t DMAEN		 			: 1;
		uint8_t 		 				: 1;				
	}BITS;
}__cmpx_scr_t;

typedef union{
	unsigned char WORD;
	struct{
		uint8_t  VOSEL						: 6;
		uint8_t  VRSEL		 				: 1;
		uint8_t  DACEN		 				: 1;
	}BITS;
}__cmpx_daccr_t;

typedef union{
	unsigned char WORD;
	struct{
		uint8_t  MSEL						: 3;
		uint8_t  PSEL		 				: 3;
		uint8_t  			 				: 1;
		uint8_t  PSTM		 				: 1;
	}BITS;
}__cmpx_muxcr_t;

typedef struct{
	volatile __cmpx_cr0_t CMPX_CR0; // 0x4007_3000
	volatile __cmpx_cr1_t CMPX_CR1; 
	volatile   uint8_t	  CMPX_FPR;
	volatile __cmpx_scr_t CMPX_SCR;
	volatile __cmpx_daccr_t CMPX_DACCR;
	volatile __cmpx_muxcr_t CMPX_MUXCR;	
} __cmpx_t;

#define sCMP0 (*((__cmpx_t *)(0x40073000)))

#define rCMP0_CR0 sCMP0.CMPX_CR0.WORD							//CMP Control Register 0
#define bCMP0_HYSTCTR sCMP0.CMPX_CR0.BITS.HYSTCTR			// Comparator hard block hysteresis control
#define bCMP0_FILTER_CNT sCMP0.CMPX_CR0.BITS.FILTER_CNT			// Filter Sample Count
		
#define rCMP0_CR1 sCMP0.CMPX_CR1.WORD			//CMP Control Register 1
#define bCMP0_EN sCMP0.CMPX_CR1.BITS.EN		//Comparator Module Enable
#define bCMP0_OPE sCMP0.CMPX_CR1.BITS.OPE		//Comparator Output Pin Enable
#define bCMP0_COS sCMP0.CMPX_CR1.BITS.COS			//Comparator Output Select
#define bCMP0_INV sCMP0.CMPX_CR1.BITS.INV		//Comparator INVERT
#define bCMP0_PMODE sCMP0.CMPX_CR1.BITS.PMODE		//Power Mode Select
#define bCMP0_TRIGM sCMP0.CMPX_CR1.BITS.TRIGM		//Trigger Mode Enable
#define bCMP0_WE sCMP0.CMPX_CR1.BITS.WE		//Windowing Enable
#define bCMP0_SE sCMP0.CMPX_CR1.BITS.SE		//Sample Enable

#define rCMP0_FPR sCMP0.CMPX_FPR			//CMP Filter Period Register

#define rCMP0_SCR sCMP0.CMPX_SCR.WORD			//CMP Status and Control Register
#define bCMP0_COUT sCMP0.CMPX_SCR.BITS.COUT		//Analog Comparator Output
#define bCMP0_CFF sCMP0.CMPX_SCR.BITS.CFF		//Analog Comparator Flag Falling
#define bCMP0_CFR sCMP0.CMPX_SCR.BITS.CFR		//Analog Comparator Flag Rising
#define bCMP0_IEF sCMP0.CMPX_SCR.BITS.IEF		//Comparator Interrupt Enable Falling
#define bCMP0_IER sCMP0.CMPX_SCR.BITS.IER		//Comparator Interrupt Enable Rising
#define bCMP0_DMAEN sCMP0.CMPX_SCR.BITS.DMAEN		//DMA Enable Control

#define rCMP0_DACCR sCMP0.CMPX_DACCR.WORD				//DAC Control Register
#define bCMP0_VOSEL sCMP0.CMPX_DACCR.BITS.VOSEL		//DAC Output Voltage Select
#define bCMP0_VRSEL sCMP0.CMPX_DACCR.BITS.VRSEL		//Supply Voltage Reference Source Select
#define bCMP0_DACEN sCMP0.CMPX_DACCR.BITS.DACEN		//DAC Enable

#define rCMP0_MUXCR sCMP0.CMPX_MUXCR.WORD			//MUX Control Register
#define bCMP0_MSEL sCMP0.CMPX_MUXCR.BITS.MSEL		//Minus Input Mux Control
#define bCMP0_PSEL sCMP0.CMPX_MUXCR.BITS.PSEL		//Plus Input Mux Control
#define bCMP0_PSTM sCMP0.CMPX_MUXCR.BITS.PSTM		//Pass Through Mode Enable

	


#endif /* CMP_LIB_H_ */

/*
 * I2S_LIB.h: Inter-IC Sound Controller
 *
 *  Created on: 25/08/2019
 *      Author: Oscar Giovanny Duque Perdomo
 */

#ifndef I2S_LIB_H_
#define I2S_LIB_H_

/*******************************************************************/
/****************Start Bits Fields Type Definition******************/
/*******************************************************************/

//Constants
enum{                                        //CLKCTRLn
	kI2S_SLOTSIZE_8  = 0,                    //8-bit Slot for Clock Unit n
    kI2S_SLOTSIZE_16 = 1,                   //16-bit Slot for Clock Unit n
    kI2S_SLOTSIZE_24 = 2,                   //24-bit Slot for Clock Unit n
    kI2S_SLOTSIZE_32 = 3,   	            //32-bit Slot for Clock Unit n
	kI2S_NBSLOTS_1,                         //Each Frame for Clock Unit n is composed of (NBSLOTS + 1) Slots.
    kI2S_NBSLOTS_2,
    kI2S_NBSLOTS_3,
    kI2S_NBSLOTS_4,
    kI2S_NBSLOTS_5,
    kI2S_NBSLOTS_6,
    kI2S_NBSLOTS_7,
    kI2S_NBSLOTS_8,
   	kI2S_NBSLOTS_9,
    kI2S_FSWIDTH_SLOT  = 0,                 //Frame Sync Pulse is 1 Slot wide (default for I2S protocol)
    kI2S_FSWIDTH_HALF  = 1,                 //Frame Sync Pulse is half a Frame wide
    kI2S_FSWIDTH_BIT   = 2,                 //Frame Sync Pulse is 1 Bit wide
    kI2S_FSWIDTH_BURST = 3,                 //Clock Unit n operates in Burst mode, with a 1-bit wide Frame Sync pulse per Data sample, only when Data transfer is requested
    kI2S_MCKDIV_0,                          //The Master Clock n is divided by (MCKDIV + 1) to obtain the Serial Clock n.
    kI2S_MCKDIV_1,
    kI2S_MCKDIV_2,
    kI2S_MCKDIV_3,
    kI2S_MCKDIV_4,
    kI2S_MCKDIV_5,
    kI2S_MCKDIV_6,
    kI2S_MCKDIV_7,
    kI2S_MCKDIV_8,
    kI2S_MCKDIV_9,
    kI2S_MCKDIV_10,
    kI2S_MCKDIV_11,
    kI2S_MCKDIV_12,
    kI2S_MCKDIV_13,
    kI2S_MCKDIV_14,
    kI2S_MCKDIV_15,
    kI2S_MCKDIV_16,
    kI2S_MCKDIV_17,
    kI2S_MCKDIV_18,
    kI2S_MCKDIV_19,
    kI2S_MCKDIV_20,
    kI2S_MCKDIV_21,
    kI2S_MCKDIV_22,
    kI2S_MCKDIV_23,
    kI2S_MCKDIV_24,
    kI2S_MCKDIV_25,
    kI2S_MCKDIV_26,
    kI2S_MCKDIV_27,
    kI2S_MCKDIV_28,
    kI2S_MCKDIV_29,
    kI2S_MCKDIV_30,
    kI2S_MCKDIV_31,
    kI2S_MCKOUTDIV_0,                //The generic clock selected by MCKSEL is divided by (MCKOUTDIV + 1) to obtain the Master Clock n output.
    kI2S_MCKOUTDIV_1,
    kI2S_MCKOUTDIV_2,
    kI2S_MCKOUTDIV_3,
    kI2S_MCKOUTDIV_4,
    kI2S_MCKOUTDIV_5,
    kI2S_MCKOUTDIV_6,
    kI2S_MCKOUTDIV_7,
    kI2S_MCKOUTDIV_8,
    kI2S_MCKOUTDIV_9,
    kI2S_MCKOUTDIV_10,
    kI2S_MCKOUTDIV_11,
    kI2S_MCKOUTDIV_12,
    kI2S_MCKOUTDIV_13,
    kI2S_MCKOUTDIV_14,
    kI2S_MCKOUTDIV_15,
    kI2S_MCKOUTDIV_16,
    kI2S_MCKOUTDIV_17,
    kI2S_MCKOUTDIV_18,
    kI2S_MCKOUTDIV_19,
    kI2S_MCKOUTDIV_20,
    kI2S_MCKOUTDIV_21,
    kI2S_MCKOUTDIV_22,
    kI2S_MCKOUTDIV_23,
    kI2S_MCKOUTDIV_24,
    kI2S_MCKOUTDIV_25,
    kI2S_MCKOUTDIV_26,
    kI2S_MCKOUTDIV_27,
    kI2S_MCKOUTDIV_28,
    kI2S_MCKOUTDIV_29,
    kI2S_MCKOUTDIV_30,
    kI2S_MCKOUTDIV_31,
};

enum{//SERCTRLn
	kI2S_SERMODE_RX      = 0,             //Receive
	kI2S_SERMODE_TX      = 1,             //Transmit
	kI2S_SERMODE_PDM2    = 2,             //Receive one PDM data on each serial clock edge
    kI2S_TXDEFAULT_ZERO  = 0,             //Output Default Value is 0
    kI2S_TXDEFAULT_ONE   = 1,             //Output Default Value is 1
    kI2S_TXDEFAULT_HIZ   = 3,	          //Output Default Value is high impedance
    kI2S_DATASIZE_32_BIT = 0,
    kI2S_DATASIZE_24_BIT = 1,
    kI2S_DATASIZE_20_BIT = 2,
    kI2S_DATASIZE_18_BIT = 3,
    kI2S_DATASIZE_16_BIT = 4,
    kI2S_DATASIZE_16C_BIT= 5,             //16 bits compact stereo
    kI2S_DATASIZE_8_BIT  = 6,
    kI2S_DATASIZE_8C_BIT = 7,             //8 bits compact stereo
    kI2S_EXTEND_ZERO     = 0,             //Extend with zeros
    kI2S_EXTEND_ONE      = 1,             //Extend with ones
    kI2S_EXTEND_MSBIT    = 2,             //Extend with Most Significant Bit
    kI2S_EXTEND_LSBIT    = 3,             //Extend with Least Significant Bit
};

enum{//DATAm
	kI2S_DATA_TX,                         //This register is used to transfer data to or from Serializer n.
    kI2S_DATA_RX,
};

//MASKS
enum{
     mI2S_CTRLA_SWRST  = 0x01U,
     mI2S_CTRLA_ENABLE = 0x02U,
     mI2S_CTRLA_CKEN0  = 0x04U,
     mI2S_CTRLA_CKEN1  = 0x08U,
     mI2S_CTRLA_SEREN0 = 0x10U,
     mI2S_CTRLA_SEREN1 = 0x20U,
};

enum{
     mI2S_CLKCTRLn0_SLOTSIZE   = 0x00000003U,
     mI2S_CLKCTRLn0_NBSLOT     = 0x0000001CU,
     mI2S_CLKCTRLn0_FSWIDTH    = 0x00000060U,
     mI2S_CLKCTRLn0_BITDELAY   = 0x00000080U,
     mI2S_CLKCTRLn0_FSSEL      = 0x00000100U,
     mI2S_CLKCTRLn0_FSINV      = 0x00000800U,
     mI2S_CLKCTRLn0_SCKEL      = 0x00001000U,
     mI2S_CLKCTRLn0_MCKSEL     = 0x00010000U,
     mI2S_CLKCTRLn0_MCKEN      = 0x00040000U,
     mI2S_CLKCTRLn0_MCKDIV     = 0x00F80000U,
     mI2S_CLKCTRLn0_MCKOUTDIV  = 0x1F000000U,
     mI2S_CLKCTRLn0_FSOUTINV   = 0x20000000U,
     mI2S_CLKCTRLn0_SCKOUTINV  = 0x40000000U,
     mI2S_CLKCTRLn0_MCKOUTINV  = 0x80000000U,
};

enum{
     mI2S_CLKCTRLn1_SLOTSIZE   = 0x00000003U,
     mI2S_CLKCTRLn1_NBSLOT     = 0x0000001CU,
     mI2S_CLKCTRLn1_FSWIDTH    = 0x00000060U,
     mI2S_CLKCTRLn1_BITDELAY   = 0x00000080U,
     mI2S_CLKCTRLn1_FSSEL      = 0x00000100U,
     mI2S_CLKCTRLn1_FSINV      = 0x00000800U,
     mI2S_CLKCTRLn1_SCKEL      = 0x00001000U,
     mI2S_CLKCTRLn1_MCKSEL     = 0x00010000U,
     mI2S_CLKCTRLn1_MCKEN      = 0x00040000U,
     mI2S_CLKCTRLn1_MCKDIV     = 0x00F80000U,
     mI2S_CLKCTRLn1_MCKOUTDIV  = 0x1F000000U,
     mI2S_CLKCTRLn1_FSOUTINV   = 0x20000000U,
     mI2S_CLKCTRLn1_SCKOUTINV  = 0x40000000U,
     mI2S_CLKCTRLn1_MCKOUTINV  = 0x80000000U,
};

enum{
     mI2S_INTENCLR_RXRDY0 = 0x0001U,
     mI2S_INTENCLR_RXRDY1 = 0x0002U,
     mI2S_INTENCLR_RXOR0  = 0x0010U,
     mI2S_INTENCLR_RXOR1  = 0x0020U,
     mI2S_INTENCLR_TXRDY0 = 0x0100U,
     mI2S_INTENCLR_TXRDY1 = 0x0200U,
     mI2S_INTENCLR_TXUR0  = 0x1000U,
     mI2S_INTENCLR_TXUR1  = 0x2000U,
};

enum{
     mI2S_INTENSET_RXRDY0 = 0x0001U,
     mI2S_INTENSET_RXRDY1 = 0x0002U,
     mI2S_INTENSET_RXOR0  = 0x0010U,
     mI2S_INTENSET_RXOR1  = 0x0020U,
     mI2S_INTENSET_TXRDY0 = 0x0100U,
     mI2S_INTENSET_TXRDY1 = 0x0200U,
     mI2S_INTENSET_TXUR0  = 0x1000U,
     mI2S_INTENSET_TXUR1  = 0x2000U,
};

enum{
     mI2S_INTFLAG_RXRDY0 = 0x0001U,
     mI2S_INTFLAG_RXRDY1 = 0x0002U,
     mI2S_INTFLAG_RXOR0  = 0x0010U,
     mI2S_INTFLAG_RXOR1  = 0x0020U,
     mI2S_INTFLAG_TXRDY0 = 0x0100U,
     mI2S_INTFLAG_TXRDY1 = 0x0200U,
     mI2S_INTFLAG_TXUR0  = 0x1000U,
     mI2S_INTFLAG_TXUR1  = 0x2000U,
};

enum{
     mI2S_SYNCBUSY_SWRST   = 0x0001U,
     mI2S_SYNCBUSY_ENABLE  = 0x0002U,
     mI2S_SYNCBUSY_CKEN0   = 0x0004U,
     mI2S_SYNCBUSY_CKEN1   = 0x0008U,
     mI2S_SYNCBUSY_SEREN0  = 0x0010U,
     mI2S_SYNCBUSY_SEREN1  = 0x0020U,
     mI2S_SYNCBUSY_DATA0   = 0x0100U,
     mI2S_SYNCBUSY_DATA1   = 0x0200U,
};

enum{
     mI2S_SERCTRLn0_SERMODE   = 0x00000003U,
     mI2S_SERCTRLn0_TXDEFAULT = 0x0000000CU,
     mI2S_SERCTRLn0_TXSAME    = 0x00000010U,
     mI2S_SERCTRLn0_CLKSEL    = 0x00000020U,
     mI2S_SERCTRLn0_SLOTADJ   = 0x00000080U,
     mI2S_SERCTRLn0_DATASIZE  = 0x00000700U,
     mI2S_SERCTRLn0_WORDADJ   = 0x00001000U,
     mI2S_SERCTRLn0_EXTEND    = 0x00006000U,
     mI2S_SERCTRLn0_BITREV    = 0x00008000U,
     mI2S_SERCTRLn0_SLOTDIS0  = 0x00010000U,
     mI2S_SERCTRLn0_SLOTDIS1  = 0x00020000U,
     mI2S_SERCTRLn0_SLOTDIS2  = 0x00040000U,
     mI2S_SERCTRLn0_SLOTDIS3  = 0x00080000U,
     mI2S_SERCTRLn0_SLOTDIS4  = 0x00100000U,
     mI2S_SERCTRLn0_SLOTDIS5  = 0x00200000U,
     mI2S_SERCTRLn0_SLOTDIS6  = 0x00400000U,
     mI2S_SERCTRLn0_SLOTDIS7  = 0x00800000U,
     mI2S_SERCTRLn0_MONO      = 0x01000000U,
     mI2S_SERCTRLn0_DMA       = 0x02000000U,
     mI2S_SERCTRLn0_RXLOOP    = 0x04000000U,
};

enum{
     mI2S_SERCTRLn1_SERMODE   = 0x00000003U,
     mI2S_SERCTRLn1_TXDEFAULT = 0x0000000CU,
     mI2S_SERCTRLn1_TXSAME    = 0x00000010U,
     mI2S_SERCTRLn1_CLKSEL    = 0x00000020U,
     mI2S_SERCTRLn1_SLOTADJ   = 0x00000080U,
     mI2S_SERCTRLn1_DATASIZE  = 0x00000700U,
     mI2S_SERCTRLn1_WORDADJ   = 0x00001000U,
     mI2S_SERCTRLn1_EXTEND    = 0x00006000U,
     mI2S_SERCTRLn1_BITREV    = 0x00008000U,
     mI2S_SERCTRLn1_SLOTDIS0  = 0x00010000U,
     mI2S_SERCTRLn1_SLOTDIS1  = 0x00020000U,
     mI2S_SERCTRLn1_SLOTDIS2  = 0x00040000U,
     mI2S_SERCTRLn1_SLOTDIS3  = 0x00080000U,
     mI2S_SERCTRLn1_SLOTDIS4  = 0x00100000U,
     mI2S_SERCTRLn1_SLOTDIS5  = 0x00200000U,
     mI2S_SERCTRLn1_SLOTDIS6  = 0x00400000U,
     mI2S_SERCTRLn1_SLOTDIS7  = 0x00800000U,
     mI2S_SERCTRLn1_MONO      = 0x01000000U,
     mI2S_SERCTRLn1_DMA       = 0x02000000U,
     mI2S_SERCTRLn1_RXLOOP    = 0x04000000U,
};

enum{
     mI2S_DATAm0_DATA = 0xFFFFFFFFU,
};

enum{
     mI2S_DATAm1_DATA = 0xFFFFFFFFU,
};

enum{
     mI2S_RXDATA_DATA = 0xFFFFFFFFU,
};

typedef union{//CTRLA
	uint8_t WORD;
	struct{
        uint8_t SWRST  : 1;
        uint8_t ENABLE : 1;
		uint8_t CKEN0  : 1;
		uint8_t CKEN1  : 1;
		uint8_t SEREN0 : 1;
		uint8_t SEREN1 : 1;
		uint8_t        : 2;
	}BITS;
}__i2s_ctrla_t;

typedef union{//CLKCTRLn0
	uint32_t WORD;
	struct{
		uint32_t SLOTSIZE  : 2;
		uint32_t NBSLOTS   : 3;
		uint32_t FSWIDTH   : 2;
		uint32_t BITDELAY  : 1;
		uint32_t FSSEL     : 1;
		uint32_t FSINV     : 1;
        uint32_t SCKSEL    : 1;
        uint32_t MCKSEL    : 1;
        uint32_t MCKEN     : 1;
        uint32_t MCKDIV    : 5;
        uint32_t MCKOUTDIV : 5;
        uint32_t FSOUTINV  : 1;
        uint32_t SCKOUTINV : 1;
        uint32_t MCKOUTINV : 1;
        uint32_t           : 6;
	}BITS;
}__i2s_clkctrln0_t;

typedef union{//CLKCTRLn1
	uint32_t WORD;
	struct{
		uint32_t SLOTSIZE1  : 2;
		uint32_t NBSLOTS1   : 3;
		uint32_t FSWIDTH1   : 2;
		uint32_t BITDELAY1  : 1;
		uint32_t FSSEL1     : 1;
		uint32_t FSINV1     : 1;
        uint32_t SCKSEL1    : 1;
        uint32_t MCKSEL1    : 1;
        uint32_t MCKEN1     : 1;
        uint32_t MCKDIV1    : 5;
        uint32_t MCKOUTDIV1 : 5;
        uint32_t FSOUTINV1  : 1;
        uint32_t SCKOUTINV1 : 1;
        uint32_t MCKOUTINV1 : 1;
        uint32_t            : 6;
	}BITS;
}__i2s_clkctrln1_t;

typedef union{//INTENCLR
	uint16_t WORD;
	struct{
		uint16_t RXRDY0 : 1;
        uint16_t RXRDY1 : 1;
        uint16_t RXOR0  : 1;
        uint16_t RXOR1  : 1;
        uint16_t TXRDY0 : 1;
        uint16_t TXRDY1 : 1;
        uint16_t TXUR0  : 1;
        uint16_t TXUR1  : 1;
        uint16_t        : 8;
	}BITS;
}__i2s_intenclr_t;

typedef union{//INTENSET
	uint16_t WORD;
	struct{
		uint16_t RXRDY0_1 : 1;
        uint16_t RXRDY1_1 : 1;
        uint16_t RXOR0_1  : 1;
        uint16_t RXOR1_1  : 1;
        uint16_t TXRDY0_1 : 1;
        uint16_t TXRDY1_1 : 1;
        uint16_t TXUR0_1  : 1;
        uint16_t TXUR1_1  : 1;
        uint16_t          : 8;
	}BITS;
}__i2s_intenset_t;

typedef union{//INTFLAG
	uint16_t WORD;
	struct{
		uint16_t RXRDY0_2 : 1;
        uint16_t RXRDY1_2 : 1;
        uint16_t RXOR0_2  : 1;
        uint16_t RXOR1_2  : 1;
        uint16_t TXRDY0_2 : 1;
        uint16_t TXRDY1_2 : 1;
        uint16_t TXUR0_2  : 1;
        uint16_t TXUR1_2  : 1;
        uint16_t          : 8;
	}BITS;
}__i2s_intflag_t;

typedef union{//SYNCBUSY
	uint16_t WORD;
	struct{
        uint16_t SWRST_1  : 1;
        uint16_t ENABLE_1 : 1;
		uint16_t CKEN0_1  : 1;
		uint16_t CKEN1_1  : 1;
		uint16_t SEREN0_1 : 1;
		uint16_t SEREN1_1 : 1;
        uint16_t DATA0    : 1;
        uint16_t DATA1    : 1;
        uint16_t          : 8;
	}BITS;
}__i2s_syncbussy_t;

typedef union{//SERCTRLn0
	uint32_t WORD;
	struct{
		uint32_t SERMODE     : 2;
		uint32_t TXDEFAULT   : 2;
		uint32_t TXSAME      : 1;
		uint32_t CLKSEL      : 1;
		uint32_t SLOTADJ     : 1;
		uint32_t DATASIZE    : 3;
        uint32_t WORDADJ     : 1;
        uint32_t EXTEND      : 2;
        uint32_t BITREV      : 1;
        uint32_t SLOTDIS0    : 1;
        uint32_t SLOTDIS1    : 1;
        uint32_t SLOTDIS2    : 1;
        uint32_t SLOTDIS3    : 1;
        uint32_t SLOTDIS4    : 1;
        uint32_t SLOTDIS5    : 1;
        uint32_t SLOTDIS6    : 1;
        uint32_t SLOTDIS7    : 1;
        uint32_t MONO        : 1;
        uint32_t DMA         : 1;
        uint32_t RXLOOP      : 1;
        uint32_t             : 7;
	}BITS;
}__i2s_serctrln0_t;

typedef union{//SERCTRLn1
	uint32_t WORD;
	struct{
		uint32_t SERMODE_A     : 2;
		uint32_t TXDEFAULT_A   : 2;
		uint32_t TXSAME_A      : 1;
		uint32_t CLKSEL_A      : 1;
		uint32_t SLOTADJ_A     : 1;
		uint32_t DATASIZE_A    : 3;
        uint32_t WORDADJ_A     : 1;
        uint32_t EXTEND_A      : 2;
        uint32_t BITREV_A      : 1;
        uint32_t SLOTDIS0_A    : 1;
        uint32_t SLOTDIS1_A    : 1;
        uint32_t SLOTDIS2_A    : 1;
        uint32_t SLOTDIS3_A    : 1;
        uint32_t SLOTDIS4_A    : 1;
        uint32_t SLOTDIS5_A    : 1;
        uint32_t SLOTDIS6_A    : 1;
        uint32_t SLOTDIS7_A    : 1;
        uint32_t MONO_A        : 1;
        uint32_t DMA_A         : 1;
        uint32_t RXLOOP_A      : 1;
        uint32_t               : 7;
	}BITS;
}__i2s_serctrln1_t;

typedef union{//DATAm0
	uint32_t WORD;
	struct{
		uint32_t DATA     : 32;
	}BITS;
}__i2s_datam0_t;

typedef union{//DATAm1
	uint32_t WORD;
	struct{
		uint32_t DATA_1     : 32;
	}BITS;
}__i2s_datam1_t;

typedef union{//RXDATA
	uint32_t WORD;
	struct{
		uint32_t DATA_2     : 32;
	}BITS;
}__i2s_rxdata_t;

/*******************************************************************/
/****************End Bits Fields Type Definition********************/
/*******************************************************************/


/*******************************************************************/
/****************Start Fields Structure Definition******************/
/*******************************************************************/

typedef struct{
	volatile __i2s_ctrla_t CTRLA;
    volatile __i2s_clkctrln0_t CLKCTRLn0;
    volatile __i2s_clkctrln1_t CLKCTRLn1;
    volatile __i2s_intenclr_t INTENCLR;
    volatile __i2s_intenset_t INTENSET;
    volatile __i2s_intflag_t INTFLAG;
    volatile __i2s_syncbussy_t SYNCBUSY;
    volatile __i2s_serctrln0_t SERCTRLn0;
    volatile __i2s_serctrln1_t SERCTRLn1;
    volatile __i2s_datam0_t DATAm0;
    volatile __i2s_datam1_t DATAm1;
    volatile __i2s_rxdata_t RXDATA;
} __i2s_t;

/*******************************************************************/
/****************End Fields Structure Definition********************/
/*******************************************************************/

/*******************************************************************/
/****************Start MACROs per Register**************************/
/*******************************************************************/

#define sI2S (*((__i2s_t *)(0x42005000))) // I2S general structure

//for I2S_CTRLA
#define rI2S_CTRLA sI2S.CTRLA.WORD
#define bI2S_SWRST sI2S.CTRLA.BITS.SWRST
#define bI2S_ENABLE sI2S.CTRLA.BITS.ENABLE
#define bI2S_CKEN0 sI2S.CTRLA.BITS.CKEN0
#define bI2S_CKEN1 sI2S.CTRLA.BITS.CKEN1
#define bI2S_SEREN0 sI2S.CTRLA.BITS.SEREN0
#define bI2S_SEREN1 sI2S.CTRLA.BITS.SEREN1

//for I2S_CLKCTRLn0
#define rI2S_CLKCTRLn0 sI2S.CLKCTRLn0.WORD
#define bI2S_SLOTSIZE sI2S.CLKCTRLn0.BITS.SLOTSIZE
#define bI2S_NBSLOTS sI2S.CLKCTRLn0.BITS.NBSLOTS
#define bI2S_FSWIDTH sI2S.CLKCTRLn0.BITS.FSWIDTH
#define bI2S_BITDELAY sI2S.CLKCTRLn0.BITS.BITDELAY
#define bI2S_FSSEL sI2S.CLKCTRLn0.BITS.FSSEL
#define bI2S_FSINV sI2S.CLKCTRLn0.BITS.FSINV
#define bI2S_SCKSEL sI2S.CLKCTRLn0.BITS.SCKSEL
#define bI2S_MCKSEL sI2S.CLKCTRLn0.BITS.MCKSEL
#define bI2S_MCKEN sI2S.CLKCTRLn0.BITS.MCKEN
#define bI2S_MCKDIV sI2S.CLKCTRLn0.BITS.MCKDIV
#define bI2S_MCKOUTDIV sI2S.CLKCTRLn0.BITS.MCKOUTDIV
#define bI2S_FSOUTINV sI2S.CLKCTRLn0.BITS.FSOUTINV
#define bI2S_SCKOUTINV sI2S.CLKCTRLn0.BITS.SCKOUTINV
#define bI2S_MCKOUTINV sI2S.CLKCTRLn0.BITS.MCKOUTINV

//for I2S_CLKCTRLn1
#define rI2S_CLKCTRLn1 sI2S.CLKCTRLn1.WORD
#define bI2S_SLOTSIZE1 sI2S.CLKCTRLn1.BITS.SLOTSIZE1
#define bI2S_NBSLOTS1 sI2S.CLKCTRLn1.BITS.NBSLOTS1
#define bI2S_FSWIDTH1 sI2S.CLKCTRLn1.BITS.FSWIDTH1
#define bI2S_BITDELAY1 sI2S.CLKCTRLn1.BITS.BITDELAY1
#define bI2S_FSSEL1 sI2S.CLKCTRLn1.BITS.FSSEL1
#define bI2S_FSINV1 sI2S.CLKCTRLn1.BITS.FSINV1
#define bI2S_SCKSEL1 sI2S.CLKCTRLn1.BITS.SCKSEL1
#define bI2S_MCKSEL1 sI2S.CLKCTRLn1.BITS.MCKSEL1
#define bI2S_MCKEN1 sI2S.CLKCTRLn1.BITS.MCKEN1
#define bI2S_MCKDIV1 sI2S.CLKCTRLn1.BITS.MCKDIV1
#define bI2S_MCKOUTDIV1 sI2S.CLKCTRLn1.BITS.MCKOUTDIV1
#define bI2S_FSOUTINV1 sI2S.CLKCTRLn1.BITS.FSOUTINV1
#define bI2S_SCKOUTINV1 sI2S.CLKCTRLn1.BITS.SCKOUTINV1
#define bI2S_MCKOUTINV1 sI2S.CLKCTRLn1.BITS.MCKOUTINV1

//for I2S_INTENCLR
#define rI2S_INTENCLR sI2S.INTENCLR.WORD
#define bI2S_RXRDY0 sI2S.INTENCLR.BITS.RXRDY0
#define bI2S_RXRDY1 sI2S.INTENCLR.BITS.RXRDY1
#define bI2S_RXOR0 sI2S.INTENCLR.BITS.RXOR0
#define bI2S_RXOR1 sI2S.INTENCLR.BITS.RXOR1
#define bI2S_TXRDY0 sI2S.INTENCLR.BITS.TXRDY0
#define bI2S_TXRDY1 sI2S.INTENCLR.BITS.TXRDY1
#define bI2S_TXUR0 sI2S.INTENCLR.BITS.TXUR0
#define bI2S_TXUR1 sI2S.INTENCLR.BITS.TXUR1

//for I2S_INTENSET
#define rI2S_INTENSET sI2S.INTENSET.WORD
#define bI2S_RXRDY0_1 sI2S.INTENSET.BITS.RXRDY0_1
#define bI2S_RXRDY1_1 sI2S.INTENSET.BITS.RXRDY1_1
#define bI2S_RXOR0_1 sI2S.INTENSET.BITS.RXOR0_1
#define bI2S_RXOR1_1 sI2S.INTENSET.BITS.RXOR1_1
#define bI2S_TXRDY0_1 sI2S.INTENSET.BITS.TXRDY0_1
#define bI2S_TXRDY1_1 sI2S.INTENSET.BITS.TXRDY1_1
#define bI2S_TXUR0_1 sI2S.INTENSET.BITS.TXUR0_1
#define bI2S_TXUR1_1 sI2S.INTENSET.BITS.TXUR1_1

//for I2S_INTFLAG
#define rI2S_INTFLAG sI2S.INTFLAG.WORD
#define bI2S_RXRDY0_2 sI2S.INTFLAG.BITS.RXRDY0_2
#define bI2S_RXRDY1_2 sI2S.INTFLAG.BITS.RXRDY1_2
#define bI2S_RXOR0_2 sI2S.INTFLAG.BITS.RXOR0_2
#define bI2S_RXOR1_2 sI2S.INTFLAG.BITS.RXOR1_2
#define bI2S_TXRDY0_2 sI2S.INTFLAG.BITS.TXRDY0_2
#define bI2S_TXRDY1_2 sI2S.INTFLAG.BITS.TXRDY1_2
#define bI2S_TXUR0_2 sI2S.INTFLAG.BITS.TXUR0_2
#define bI2S_TXUR1_2 sI2S.INTFLAG.BITS.TXUR1_2

//for I2S_SYNCBUSY
#define rI2S_SYNCBUSY sI2S.SYNCBUSY.WORD
#define bI2S_SWRST_1 sI2S.SYNCBUSY.BITS.SWRST_1
#define bI2S_ENABLE_1 sI2S.SYNCBUSY.BITS.ENABLE_1
#define bI2S_CKEN0_1 sI2S.SYNCBUSY.BITS.CKEN0_1
#define bI2S_CKEN1_1 sI2S.SYNCBUSY.BITS.CKEN1_1
#define bI2S_SEREN0_1 sI2S.SYNCBUSY.BITS.SEREN0_1
#define bI2S_SEREN1_1 sI2S.SYNCBUSY.BITS.SEREN1_1
#define bI2S_DATA0 sI2S.SYNCBUSY.BITS.DATA0
#define bI2S_DATA1 sI2S.SYNCBUSY.BITS.DATA1

//for I2S_SERCTRLn0
#define rI2S_SERCTRLn0 sI2S.SERCTRLn0.WORD
#define bI2S_SERMODE sI2S.SERCTRLn0.BITS.SERMODE
#define bI2S_TXDEFAULT sI2S.SERCTRLn0.BITS.TXDEFAULT
#define bI2S_TXSAME sI2S.SERCTRLn0.BITS.TXSAME
#define bI2S_CLKSEL sI2S.SERCTRLn0.BITS.CLKSEL
#define bI2S_SLOTADJ sI2S.SERCTRLn0.BITS.SLOTADJ
#define bI2S_DATASIZE sI2S.SERCTRLn0.BITS.DATASIZE
#define bI2S_WORDADJ sI2S.SERCTRLn0.BITS.WORDADJ
#define bI2S_EXTEND sI2S.SERCTRLn0.BITS.EXTEND
#define bI2S_BITREV sI2S.SERCTRLn0.BITS.BITREV
#define bI2S_SLOTDIS0 sI2S.SERCTRLn0.BITS.SLOTDIS0
#define bI2S_SLOTDIS1 sI2S.SERCTRLn0.BITS.SLOTDIS1
#define bI2S_SLOTDIS2 sI2S.SERCTRLn0.BITS.SLOTDIS2
#define bI2S_SLOTDIS3 sI2S.SERCTRLn0.BITS.SLOTDIS3
#define bI2S_SLOTDIS4 sI2S.SERCTRLn0.BITS.SLOTDIS4
#define bI2S_SLOTDIS5 sI2S.SERCTRLn0.BITS.SLOTDIS5
#define bI2S_SLOTDIS6 sI2S.SERCTRLn0.BITS.SLOTDIS6
#define bI2S_SLOTDIS7 sI2S.SERCTRLn0.BITS.SLOTDIS7
#define bI2S_MONO sI2S.SERCTRLn0.BITS.MONO
#define bI2S_DMA sI2S.SERCTRLn0.BITS.DMA
#define bI2S_RXLOOP sI2S.SERCTRLn0.BITS.RXLOOP

//for I2S_SERCTRLn1
#define rI2S_SERCTRLn1 sI2S.SERCTRLn1.WORD
#define bI2S_SERMODE_A sI2S.SERCTRLn1.BITS.SERMODE_A
#define bI2S_TXDEFAULT_A sI2S.SERCTRLn1.BITS.TXDEFAULT_A
#define bI2S_TXSAME_A sI2S.SERCTRLn1.BITS.TXSAME_A
#define bI2S_CLKSEL_A sI2S.SERCTRLn1.BITS.CLKSEL_A
#define bI2S_SLOTADJ_A sI2S.SERCTRLn1.BITS.SLOTADJ_A
#define bI2S_DATASIZE_A sI2S.SERCTRLn1.BITS.DATASIZE_A
#define bI2S_WORDADJ_A sI2S.SERCTRLn1.BITS.WORDADJ_A
#define bI2S_EXTEND_A sI2S.SERCTRLn1.BITS.EXTEND_A
#define bI2S_BITREV_A sI2S.SERCTRLn1.BITS.BITREV_A
#define bI2S_SLOTDIS0_A sI2S.SERCTRLn1.BITS.SLOTDIS0_A
#define bI2S_SLOTDIS1_A sI2S.SERCTRLn1.BITS.SLOTDIS1_A
#define bI2S_SLOTDIS2_A sI2S.SERCTRLn1.BITS.SLOTDIS2_A
#define bI2S_SLOTDIS3_A sI2S.SERCTRLn1.BITS.SLOTDIS3_A
#define bI2S_SLOTDIS4_A sI2S.SERCTRLn1.BITS.SLOTDIS4_A
#define bI2S_SLOTDIS5_A sI2S.SERCTRLn1.BITS.SLOTDIS5_A
#define bI2S_SLOTDIS6_A sI2S.SERCTRLn1.BITS.SLOTDIS6_A
#define bI2S_SLOTDIS7_A sI2S.SERCTRLn1.BITS.SLOTDIS7_A
#define bI2S_MONO_A sI2S.SERCTRLn1.BITS.MONO_A
#define bI2S_DMA_A sI2S.SERCTRLn1.BITS.DMA_A
#define bI2S_RXLOOP_A sI2S.SERCTRLn1.BITS.RXLOOP_A

//for I2S_DATAm0
#define rI2S_DATAm0 sI2S.DATAm0.WORD
#define bI2S_DATA sI2S.DATAm0.BITS.DATA

//for I2S_DATAm1
#define rI2S_DATAm1 sI2S.DATAm1.WORD
#define bI2S_DATA_1 sI2S.DATAm1.BITS.DATA_1

//for I2S_RXDATA
#define rI2S_RXDATA sI2S.RXDATA.WORD
#define bI2S_DATA_2 sI2S.RXDATA.BITS.DATA_2

/*******************************************************************/
/****************End MACROs per Register****************************/
/*******************************************************************/

#endif

/*
 * PMC.h
 *
 *  Created on: 26/09/2017
 *      Author: Ricardo Andrés Velásquez
 */

#ifndef PMC_LIB_H_
#define PMC_LIB_H_


enum{
	kPCM_LVDV_LOW_TRIP_POINT = 0x0000U,						// Low trip point selected (V_LVD = V_LVDL )
	kPCM_LVDV_HIGH_TRIP_POINT = 0x0001U						// High trip point selected (V_LVD = V_LVDH )
}; // __pcm_lvdsc1_lvdv_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t LVDV		: 2;						// Low-Voltage Detect Voltage Select
		uint8_t  			: 2;						// Reserved
		uint8_t LVDRE	 	: 1;						// Low-Voltage Detect Reset Enable
		uint8_t LVDIE      	: 1;						// Low-Voltage Detect Interrupt Enable
		uint8_t LVDACK	  	: 1;						// Low-Voltage Detect Acknowledge
		uint8_t LVDF	  	: 1;						// Low-Voltage Detect Flag
	}BITS;
}__pmc_lvdsc1_t;

enum{
	kPCM_LVWV_LOW_TRIP_POINT = 0x0000U,						// Low trip point selected (VLVW = VLVW1)
	kPCM_LVWV_MID1_TRIP_POINT = 0X0001U,						// Mid 1 trip point selected (VLVW = VLVW2)
	kPCM_LVWV_MID2_TRIP_POINT = 0X0002U,						// Mid 2 trip point selected (VLVW = VLVW3)
	kPCM_LVWV_HIGH_TRIP_POINT = 0x0003U						// High trip point selected (VLVW = VLVW4)
};// __pcm_lvdsc2_lvWv_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t LVWV		: 2;						// Low-Voltage Warning Voltage Select
		uint8_t  			: 3;						// Reserved
		uint8_t LVWIE	 	: 1;						// Low-Voltage Warning Interrupt Enable
		uint8_t LVWACK     	: 1;						// Low-Voltage Warning Acknowledge
		uint8_t LVWF	  	: 1;						// Low-Voltage Warning Flag
	}BITS;
}__pmc_lvdsc2_t;

typedef union{
	unsigned char BYTE;
	struct{
		uint8_t BGBE		: 1;						// Bandgap Buffer Enable
		uint8_t  			: 1;						// Reserved
		uint8_t REGONS		: 1;						// Regulator In Run Regulation Status
		uint8_t ACKISO      : 1;						// Acknowledge Isolation
		uint8_t BGEN		: 1;						// Bandgap Enable In VLPx Operation
		uint8_t 	  		: 1;						// Reserved
		uint8_t	VLPO    	: 1;						// VLPx Option
	}BITS;
}__pmc_regsc_t;

typedef struct{
	volatile __pmc_lvdsc1_t PMC_LVDSC1; 				// 0x00
	volatile __pmc_lvdsc2_t PMC_LVDSC2; 				// 0x01
	volatile __pmc_regsc_t PMC_REGSC; 					// 0x02
} __pmc_t;

#define sPMC (*((__smc_t *)(0x4007E000)))  			// General structure to access the Power Management Controller

// PMC_LVDSC1
#define rPMC_LVDSC1 sPMC.PMC_LVDSC1.BYTE				// Low Voltage Detect Status And Control 1 register (PMC_LVDSC1)
#define bPMC_LVDF sPMC.PMC_LVDSC1.BITS.LVDF			// Low-Voltage Detect Flag
#define bPMC_LVDACK sPMC.PMC_LVDSC1.BITS.LVDACK		// Low-Voltage Detect Acknowledge
#define bPMC_LVDIE sPMC.PMC_LVDSC1.BITS.LVDIE			// Low-Voltage Detect Interrupt Enable
#define bPMC_LVDRE sPMC.PMC_LVDSC1.BITS.LVDRE			// Low-Voltage Detect Reset Enable
#define bPMC_LVDV sPMC.PMC_LVDSC1.BITS.LVDV			// Low-Voltage Detect Voltage Select

// PMC_LVDSC2
#define rPMC_LVDSC2 sPMC.PMC_LVDSC2.BYTE				// Low Voltage Detect Status And Control 2 register (PMC_LVDSC2)
#define bPMC_LVWF sPMC.PMC_LVDSC2.BITS.LVWF			// Low-Voltage Warning Flag
#define bPMC_LVWACK sPMC.PMC_LVDSC2.BITS.LVWACK		// Low-Voltage Warning Acknowledge
#define bPMC_LVWIE sPMC.PMC_LVDSC2.BITS.LVWIE			// Low-Voltage Warning Interrupt Enable
#define bPMC_LVWV sPMC.PMC_LVDSC2.BITS.LVWV			// Low-Voltage Warning Voltage Select

// PMC_REGSC
#define rPMC_REGSC sPMC.PMC_REGSC.BYTE				// Low Voltage Detect Status And Control 2 register (PMC_LVDSC2)
#define bPMC_VLPO sPMC.PMC_REGSC.BITS.VLPO			// VLPx Option
#define bPMC_BGEN sPMC.PMC_REGSC.BITS.BGEN			// Bandgap Enable In VLPx Operation
#define bPMC_ACKISO sPMC.PMC_REGSC.BITS.ACKISO		// Acknowledge Isolation
#define bPMC_REGONS sPMC.PMC_REGSC.BITS.REGONS		// Regulator In Run Regulation Status
#define bPMC_BGBE sPMC.PMC_REGSC.BITS.BGBE			// Bandgap Buffer Enable


#endif /* PMC_LIB_H_ */

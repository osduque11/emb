/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    MKL27Z_ADC_PWM_PIT.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */



#define SetDutyTPM0CH0(x) TPM0->CONTROLS[0].CnV=(uint32_t)(x)

#define CH_1 1

uint16_t ADCVal=0;
volatile bool pitIsrFlag = false;

uint16_t ADC_Read(uint8_t CH){
	uint16_t ADCValue;
    ADC0->SC1[0]=ADC_SC1_ADCH(CH); //select the input channel to start conversion
    while (0U == (ADC_SC1_COCO_MASK & ADC0->SC1[0])){}; //COCO is set upon completion of a conversion
    ADCValue= ADC0->R[0];
    return ADCValue;

}

void ADC_Init(void){

	SIM->SCGC5|= SIM_SCGC5_PORTE_MASK;  //Port E Clock Gate Control: Clock enabled
	PORTE->PCR[16] = PORT_PCR_MUX(0);// PORTE16 (pin 8) is configured as ADC0_SE1

	SIM->SCGC6|=SIM_SCGC6_ADC0_MASK; //ADC0 Clock Gate Control Clock enabled
	ADC0->CFG1 |= ADC_CFG1_ADICLK(0x3U); //Asynchronous clock (ADACK). Default single-ended 8-bit conversion;


}

void PWM_Init(void){

	SIM->SOPT2|=SIM_SOPT2_TPMSRC(3);//TPM Clock Source Select 0b11 MCGIRCLK clock

	SIM->SCGC6|=SIM_SCGC6_TPM0_MASK; //TPM0 Clock Gate Control Clock enabled

	SIM->SCGC5|= SIM_SCGC5_PORTE_MASK;  //Port E Clock Gate Control: Clock enabled
	PORTE->PCR[24] = PORT_PCR_MUX(3);	//PORTE24 (pin 20) is configured as TPM0_CH0, ALT3

	TPM0->SC|=TPM_SC_CMOD(1);	//TPM counter increments on every TPM counter clock
	TPM0->SC|=TPM_SC_PS(0B011); //Prescale Factor Selection Divide by 8

	//Edge-aligned PWM. High-true pulses (clear Output on match, set Output on reload)
	TPM0->CONTROLS[0].CnSC|=TPM_CnSC_MSB_MASK|TPM_CnSC_ELSB_MASK;
	//Edge-aligned PWM. High-true pulses (clear Output on match, set Output on reload)
	TPM0->MOD=255; // Module Register is set to 255
	TPM0->CONTROLS[0].CnV=50; // Channel register is set to 50
}

void PIT_Init(void){
	SIM->SCGC6|=SIM_SCGC6_PIT_MASK; //PIT Clock Gate Control Clock enabled
	//PIT RUNS AT BUS FRECUENCY 24MHz
	PIT->MCR=0x00;// turn on PIT. Clock for standard PIT timers is enabled.
	PIT->CHANNEL[0].LDVAL=1000000;//24MHz to get 1seg Interrupt
	PIT->CHANNEL[0].TCTRL|=PIT_TCTRL_TIE_MASK; //enable Timer 0 interrupts
    /* Enable at the NVIC */
    EnableIRQ(PIT_IRQn);
	PIT->CHANNEL[0].TCTRL|=PIT_TCTRL_TEN_MASK; //start Timer 0
}




void PIT_IRQHandler(void)
{
    /* Clear interrupt flag.*/
	PIT->CHANNEL[0].TFLG=PIT_TFLG_TIF_MASK;
    pitIsrFlag = true;
}



/*
 * @brief   Application entry point.
 */



int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
    ADC_Init();
    PWM_Init();
    SetDutyTPM0CH0(10);
    PIT_Init();


    PRINTF("Hello World\n");

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
        if(pitIsrFlag == true){
        	pitIsrFlag=false;
        	ADCVal=ADC_Read(CH_1);
        	SetDutyTPM0CH0(ADCVal);
        }

    }
    return 0 ;
}


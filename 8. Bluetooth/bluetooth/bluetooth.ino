#include <SoftwareSerial.h>
#define RxD 10
#define TxD 11
SoftwareSerial BTSerial(10, 11); // Recive (RD), Transmit (TxD)

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);
  BTSerial.begin(9600);
  Serial.println("Enter AT commands:");
  delay(100);

}

void loop() {
  // put your main code here, to run repeatedly:
  if (BTSerial.available()) {
    Serial.write(BTSerial.read());
    }
  if (Serial.available()) {
    BTSerial.write(Serial.read());
    }
}

float voltaje;
float intensidad;

void setup() {
  // put your setup code here, to run once:
  pinMode(9,OUTPUT);
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  voltaje = analogRead(A0);
  analogWrite(9,map(voltaje, 0, 1023, 0, 255));
  intensidad = (voltaje*100)/1023;
  voltaje = (voltaje*5)/1023;
  Serial.print(voltaje);
  Serial.print(';');
  Serial.println(intensidad);
  delay(500); 

}

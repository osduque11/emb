################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../source/ADC_test.c \
../source/mtb.c \
../source/semihost_hardfault.c 

OBJS += \
./source/ADC_test.o \
./source/mtb.o \
./source/semihost_hardfault.o 

C_DEPS += \
./source/ADC_test.d \
./source/mtb.d \
./source/semihost_hardfault.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DPRINTF_FLOAT_ENABLE=0 -D__USE_CMSIS -DCR_INTEGER_PRINTF -DSDK_DEBUGCONSOLE=0 -D__MCUXPRESSO -DDEBUG -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DCPU_MKL27Z64VLH4 -DCPU_MKL27Z64VLH4_cm0plus -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\board" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\source" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\drivers" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\utilities" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\startup" -I"C:\Users\Ricardo\Dropbox\code\workspace_mcuXpresso3\ADC_test\CMSIS" -O0 -fno-common -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0plus -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



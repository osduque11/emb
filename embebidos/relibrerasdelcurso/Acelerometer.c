/*
 * Copyright (c) 2017, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Acelerometer.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL27Z644.h"
/* TODO: insert other include files here. */
#include "SIM_LIB.h"
#include "PORT_LIB.h"
#include "I2C_LIB.h"
#include "UART_LIB.h"
#include "LPUART_LIB.h"
#include "FLEXIO_LIB.h"
#include "i2c_data_lib.h"
/* TODO: insert other definitions and declarations here. */
#define MMA8451_ADD_SA0_1 0x3A
#define MMA8451_ADD_SA0_0 0x38

/*
 * @brief   Application entry point.
 */
int main(void) {
	/* Init board hardware. */
	    BOARD_InitBootPins();
	    BOARD_InitBootClocks();
	  	/* Init FSL debug console. */
	    BOARD_InitDebugConsole();

	    bSIM_CG_I2C1 = 1;			// Disable I2C clock gating
	    bSIM_CG_PORTD = 1;			// Disable PORTD clock gating
	    bSIM_CG_PORTC = 1;			// Disable PORTC clock gating

	    bPTD7_MUX = kPTD7_MUX_I2C1_SCL;	// PTD7 as SCL signal for I2C1 module
	    bPTD6_MUX = kPTD6_MUX_I2C1_SDA; // PTD6 as SDA signal for I2C1 module
	    bPTD7_PS = 1;					// Pull up selected for PTD7
	    bPTD6_PS = 1;					// Pull up selected for PTD6
	    bPTD7_PE = 1;					// PTD7 pull enable required by I2C interface
	    bPTD6_PE = 1;					// PTD6 pull enable required by I2C interface
	    bPTD7_DSE = 1;					// PTD7 pull enable required by I2C interface
	    bPTD6_DSE = 1;					// PTD6 pull enable required by I2C interface

	    bPTC3_MUX = kPTC3_MUX_GPIO;		// MMA8451 Interrupt
	    bPTC2_MUX = kPTC2_MUX_GPIO;		// MMA8451 Interrupt
	    bPTC3_IRQC = kPORT_IRQC_INTERRUPT_RISING_EDGE;
	    bPTC2_IRQC = kPORT_IRQC_INTERRUPT_RISING_EDGE;

	    mma8451q_init(MMA8451_ADD_SA0_1);

	    printf("Let's try!!!");
	    uint8_t data = -1;
	    int error = i2c1_single_byte_read(&data, 0x0D);

	    if (!error)
	    	printf("%x",data);

	    //bI2C1
	    /* Force the counter to be placed into memory. */
	    volatile static int i = 0 ;
	    /* Enter an infinite loop, just incrementing a counter. */
	    while(1) {
	        i++ ;
	    }
	    return 0 ;
}
